package org.oucho.picasso;

import static android.content.ContentResolver.SCHEME_ANDROID_RESOURCE;
import static org.oucho.picasso.Picasso.LoadedFrom.DISK;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;

/** @noinspection unused*/
class ResourceRequestHandler extends RequestHandler {
    private final Context context;

    ResourceRequestHandler(Context context) {
        this.context = context;
    }

    private static Bitmap decodeResource(Resources resources, int id, Request data) {
        final BitmapFactory.Options options = createBitmapOptions(data);
        if (requiresInSampleSize(options)) {
            BitmapFactory.decodeResource(resources, id, options);
            calculateInSampleSize(data.targetWidth, data.targetHeight, options, data);
        }
        return BitmapFactory.decodeResource(resources, id, options);
    }

    @Override
    public boolean canHandleRequest(Request data) {
        if (data.resourceId != 0) {
            return true;
        }

        return SCHEME_ANDROID_RESOURCE.equals(data.uri.getScheme());
    }

    @Override
    public Result load(Request request) throws IOException {
        Resources res = PicassoUtils.getResources(context, request);
        int id = PicassoUtils.getResourceId(res, request);
        return new Result(decodeResource(res, id, request), DISK);
    }
}
