package org.oucho.picasso;

import static org.oucho.picasso.Picasso.LoadedFrom.DISK;
import static org.oucho.picasso.Picasso.LoadedFrom.NETWORK;

import java.io.IOException;

import okhttp3.CacheControl;
import okhttp3.Response;
import okhttp3.ResponseBody;

/** @noinspection unused*/
class NetworkRequestHandler extends RequestHandler {
    private static final String SCHEME_HTTP = "http";
    private static final String SCHEME_HTTPS = "https";

    private final Downloader downloader;
    private final Stats stats;

    NetworkRequestHandler(Downloader downloader, Stats stats) {
        this.downloader = downloader;
        this.stats = stats;
    }

    private static okhttp3.Request createRequest(Request request) {
        CacheControl cacheControl;
        CacheControl.Builder builder = new CacheControl.Builder();
        cacheControl = builder.build();

        okhttp3.Request.Builder builder1 = new okhttp3.Request.Builder().url(request.uri.toString());
        builder1.cacheControl(cacheControl);
        return builder1.build();
    }

    @Override
    public boolean canHandleRequest(Request data) {
        String scheme = data.uri.getScheme();
        return (SCHEME_HTTP.equals(scheme) || SCHEME_HTTPS.equals(scheme));
    }

    @Override
    public Result load(Request request) throws IOException {
        okhttp3.Request downloaderRequest = createRequest(request);
        Response response = downloader.load(downloaderRequest);
        ResponseBody body = response.body();

        if (!response.isSuccessful()) {
            assert body != null;
            body.close();
            throw new ResponseException(response.code());
        }

        // Cache response is only null when the response comes fully from the network. Both completely
        // cached and conditionally cached responses will have a non-null cache response.
        Picasso.LoadedFrom loadedFrom = response.cacheResponse() == null ? NETWORK : DISK;

        // Sometimes response content length is zero when requests are being replayed. Haven't found
        // root cause to this but retrying the request seems safe to do so.
        if (loadedFrom == DISK) {
            assert body != null;
            if (body.contentLength() == 0) {
                body.close();
                throw new ContentLengthException();
            }
        }
        if (loadedFrom == NETWORK) {
            assert body != null;
            if (body.contentLength() > 0) {
                stats.dispatchDownloadFinished(body.contentLength());
            }
        }
        return new Result(body.source(), loadedFrom);
    }

    @Override
    int getRetryCount() {
        return 2;
    }

    static class ContentLengthException extends IOException {
        ContentLengthException() {
            super("Received response with 0 content-length header.");
        }
    }

    static final class ResponseException extends IOException {
        final int code;

        ResponseException(int code) {
            super("HTTP " + code);
            this.code = code;
        }
    }
}
