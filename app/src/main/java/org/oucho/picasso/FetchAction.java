package org.oucho.picasso;

import android.graphics.Bitmap;

/** @noinspection unused, unused , unused , unused */
class FetchAction extends Action<Object> {

    private final Object target;
    private Callback callback;

    FetchAction(Picasso picasso, Request data, Object tag,
                String key, Callback callback) {
        super(picasso, null, data, 0, null, key, tag);
        this.target = new Object();
        this.callback = callback;
    }

    @Override
    void complete(Bitmap result, Picasso.LoadedFrom from) {
        if (callback != null) {
            callback.onSuccess();
        }
    }

    @Override
    void error(Exception e) {
        if (callback != null) {
            callback.onError(e);
        }
    }

    @Override
    void cancel() {
        super.cancel();
        callback = null;
    }

    @Override
    Object getTarget() {
        return target;
    }
}
