package org.oucho.picasso;

import static org.oucho.picasso.Picasso.LoadedFrom;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public interface Target {

    void onBitmapLoaded(Bitmap bitmap, LoadedFrom from);

    void onBitmapFailed(Exception e, Drawable errorDrawable);

    void onPrepareLoad(Drawable placeHolderDrawable);
}
