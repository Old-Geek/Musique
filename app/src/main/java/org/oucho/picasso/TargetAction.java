package org.oucho.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import androidx.appcompat.content.res.AppCompatResources;

final class TargetAction extends Action<Target> {

    TargetAction(Picasso picasso, Target target, Request data,
                 Drawable errorDrawable, String key, Object tag, int errorResId) {
        super(picasso, target, data, errorResId, errorDrawable, key, tag);
    }

    @Override
    void complete(Bitmap result, Picasso.LoadedFrom from) {
        if (result == null) {
            throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
        }
        Target target = getTarget();
        if (target != null) {
            target.onBitmapLoaded(result, from);
            if (result.isRecycled()) {
                throw new IllegalStateException("Target callback must not recycle bitmap!");
            }
        }
    }

    @Override
    void error(Exception e) {
        Target target = getTarget();
        if (target != null) {
            if (errorResId != 0) {
                Drawable drawable = AppCompatResources.getDrawable(picasso.context, errorResId);
                target.onBitmapFailed(e, drawable);
            } else {
                target.onBitmapFailed(e, errorDrawable);
            }
        }
    }
}
