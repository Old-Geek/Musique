package org.oucho.picasso;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static org.oucho.picasso.Action.RequestWeakReference;
import static org.oucho.picasso.Dispatcher.HUNTER_BATCH_COMPLETE;
import static org.oucho.picasso.Dispatcher.REQUEST_BATCH_RESUME;
import static org.oucho.picasso.Dispatcher.REQUEST_GCED;
import static org.oucho.picasso.Picasso.LoadedFrom.MEMORY;
import static org.oucho.picasso.PicassoUtils.THREAD_LEAK_CLEANING_MS;
import static org.oucho.picasso.PicassoUtils.THREAD_PREFIX;
import static org.oucho.picasso.PicassoUtils.checkMain;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

/** @noinspection rawtypes, unused , unused */
public class Picasso {

    @SuppressLint("StaticFieldLeak")
    static volatile Picasso singleton = null;
    final Context context;
    final Dispatcher dispatcher;
    final Cache cache;
    final Stats stats;
    final Map<Object, Action> targetToAction;
    final Map<ImageView, DeferredRequestCreator> targetToDeferredRequestCreator;
    final ReferenceQueue<Object> referenceQueue;
    final Bitmap.Config defaultBitmapConfig;
    private final Listener listener;
    private final RequestTransformer requestTransformer;
    private final List<RequestHandler> requestHandlers;

    static final String TAG = "Picasso";

    public static Picasso get() {
        if (singleton == null) {
            synchronized (Picasso.class) {
                if (singleton == null) {
                    if (PicassoProvider.context == null)
                        throw new IllegalStateException("context == null");

                    singleton = new Builder(PicassoProvider.context).build();
                }
            }
        }
        return singleton;
    }



    Picasso(Context context, Dispatcher dispatcher, Cache cache, Listener listener,
            RequestTransformer requestTransformer, List<RequestHandler> extraRequestHandlers, Stats stats,
            Bitmap.Config defaultBitmapConfig) {
        this.context = context;
        this.dispatcher = dispatcher;
        this.cache = cache;
        this.listener = listener;
        this.requestTransformer = requestTransformer;
        this.defaultBitmapConfig = defaultBitmapConfig;

        int builtInHandlers = 7; // Adjust this as internal handlers are added or removed.
        int extraCount = (extraRequestHandlers != null ? extraRequestHandlers.size() : 0);
        List<RequestHandler> allRequestHandlers = new ArrayList<>(builtInHandlers + extraCount);

        // ResourceRequestHandler needs to be the first in the list to avoid
        // forcing other RequestHandlers to perform null checks on request.uri
        // to cover the (request.resourceId != 0) case.
        allRequestHandlers.add(new ResourceRequestHandler(context));
        if (extraRequestHandlers != null)
            allRequestHandlers.addAll(extraRequestHandlers);

        allRequestHandlers.add(new MediaStoreRequestHandler(context));
        allRequestHandlers.add(new ContentStreamRequestHandler(context));
        allRequestHandlers.add(new AssetRequestHandler(context));
        allRequestHandlers.add(new FileRequestHandler(context));
        allRequestHandlers.add(new NetworkRequestHandler(dispatcher.downloader, stats));
        requestHandlers = Collections.unmodifiableList(allRequestHandlers);

        this.stats = stats;
        this.targetToAction = new WeakHashMap<>();
        this.targetToDeferredRequestCreator = new WeakHashMap<>();
        this.referenceQueue = new ReferenceQueue<>();
        CleanupThread cleanupThread = new CleanupThread(referenceQueue);
        cleanupThread.start();
    }

    @SuppressWarnings("UnusedDeclaration") // Public API.
    public static class Builder {
        private final Context context;
        private Downloader downloader;
        private ExecutorService service;
        private Cache cache;
        private Listener listener;
        private RequestTransformer transformer;
        private List<RequestHandler> requestHandlers;
        private Bitmap.Config defaultBitmapConfig;

        public Builder(@NonNull Context context) {
            this.context = context.getApplicationContext();
        }

        public Builder defaultBitmapConfig(@NonNull Bitmap.Config bitmapConfig) {
            this.defaultBitmapConfig = bitmapConfig;
            return this;
        }

        public Builder downloader(@NonNull Downloader downloader) {
            if (this.downloader != null)
                throw new IllegalStateException("Downloader already set.");

            this.downloader = downloader;
            return this;
        }

        public Builder executor(@NonNull ExecutorService executorService) {
            if (this.service != null)
                throw new IllegalStateException("Executor service already set.");

            this.service = executorService;
            return this;
        }

        public Builder memoryCache(@NonNull Cache memoryCache) {
            if (this.cache != null)
                throw new IllegalStateException("Memory cache already set.");

            this.cache = memoryCache;
            return this;
        }

        public Builder listener(@NonNull Listener listener) {
            if (this.listener != null)
                throw new IllegalStateException("Listener already set.");

            this.listener = listener;
            return this;
        }

        public Builder requestTransformer(@NonNull RequestTransformer transformer) {
            if (this.transformer != null)
                throw new IllegalStateException("Transformer already set.");

            this.transformer = transformer;
            return this;
        }

        public Builder addRequestHandler(@NonNull RequestHandler requestHandler) {
            if (requestHandlers == null)
                requestHandlers = new ArrayList<>();

            if (requestHandlers.contains(requestHandler))
                throw new IllegalStateException("RequestHandler already registered.");

            requestHandlers.add(requestHandler);
            return this;
        }

        public Picasso build() {
            Context context = this.context;

            if (downloader == null)
                downloader = new OkHttp3Downloader(context);

            if (cache == null)
                cache = new LruCache(context);

            if (service == null)
                service = new PicassoExecutorService();

            if (transformer == null)
                transformer = RequestTransformer.IDENTITY;

            Stats stats = new Stats(cache);

            Dispatcher dispatcher = new Dispatcher(context, service, downloader, cache, stats);

            return new Picasso(context, dispatcher, cache, listener, transformer, requestHandlers, stats,
                    defaultBitmapConfig);
        }
    }

    static final Handler HANDLER = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HUNTER_BATCH_COMPLETE: {
                    @SuppressWarnings("unchecked") List<BitmapHunter> batch = (List<BitmapHunter>) msg.obj;
                    for (int i = 0, n = batch.size(); i < n; i++) {
                        BitmapHunter hunter = batch.get(i);
                        hunter.picasso.complete(hunter);
                    }
                    break;
                }
                case REQUEST_GCED: {
                    Action action = (Action) msg.obj;
                    action.picasso.cancelExistingRequest(action.getTarget());
                    break;
                }
                case REQUEST_BATCH_RESUME:
                    @SuppressWarnings("unchecked") List<Action> batch = (List<Action>) msg.obj;
                    for (int i = 0, n = batch.size(); i < n; i++) {
                        Action action = batch.get(i);
                        action.picasso.resumeAction(action);
                    }
                    break;
                default:
                    throw new AssertionError("Unknown handler message received: " + msg.what);
            }
        }
    };


    public static void setSingletonInstance(@NonNull Picasso picasso) {
        synchronized (Picasso.class) {
            if (singleton != null)
                throw new IllegalStateException("Singleton instance already exists.");

            singleton = picasso;
        }
    }


    public void cancelRequest(@NonNull ImageView view) {
        // checkMain() is called from cancelExistingRequest()
        cancelExistingRequest(view);
    }


    public void cancelRequest(@NonNull Target target) {
        // checkMain() is called from cancelExistingRequest()
        cancelExistingRequest(target);
    }


    /**
     * Invalidate all memory cached images for the specified {@code uri}.
     *
     * @see #invalidate(String)
     * @see #invalidate(File)
     */
    public void invalidate(@Nullable Uri uri) {
        if (uri != null) {
            cache.clearKeyUri(uri.toString());
        }
    }

    /**
     * Invalidate all memory cached images for the specified {@code path}. You can also pass a
     *
     * @see #invalidate(Uri)
     * @see #invalidate(File)
     */
    public void invalidate(@Nullable String path) {
        if (path != null) {
            invalidate(Uri.parse(path));
        }
    }

    /**
     * Invalidate all memory cached images for the specified {@code file}.
     *
     * @see #invalidate(Uri)
     * @see #invalidate(String)
     */
    public void invalidate(@NonNull File file) {
        invalidate(Uri.fromFile(file));
    }

    public RequestCreator load(@Nullable Uri uri) {
        return new RequestCreator(this, uri, 0);
    }

    public RequestCreator load(@Nullable String path) {
        if (path == null)
            return new RequestCreator(this, null, 0);

        if (path.trim().isEmpty())
            throw new IllegalArgumentException("Path must not be empty.");

        return load(Uri.parse(path));
    }

    public RequestCreator load(@NonNull File file) {
        return load(Uri.fromFile(file));
    }

    public RequestCreator load(@DrawableRes int resourceId) {
        if (resourceId == 0)
            throw new IllegalArgumentException("Resource ID must not be zero.");

        return new RequestCreator(this, null, resourceId);
    }

    List<RequestHandler> getRequestHandlers() {
        return requestHandlers;
    }

    Request transformRequest(Request request) {
        Request transformed = requestTransformer.transformRequest(request);
        if (transformed == null) {
            throw new IllegalStateException("Request transformer "
                    + requestTransformer.getClass().getCanonicalName()
                    + " returned null for "
                    + request);
        }
        return transformed;
    }

    void defer(ImageView view, DeferredRequestCreator request) {
        // If there is already a deferred request, cancel it.
        if (targetToDeferredRequestCreator.containsKey(view))
            cancelExistingRequest(view);

        targetToDeferredRequestCreator.put(view, request);
    }

    void enqueueAndSubmit(Action action) {
        Object target = action.getTarget();
        if (target != null && targetToAction.get(target) != action) {
            // This will also check we are on the main thread.
            cancelExistingRequest(target);
            targetToAction.put(target, action);
        }
        submit(action);
    }

    void submit(Action action) {
        dispatcher.dispatchSubmit(action);
    }

    Bitmap quickMemoryCacheCheck(String key) {
        Bitmap cached = cache.get(key);
        if (cached != null)
            stats.dispatchCacheHit();
        else
            stats.dispatchCacheMiss();

        return cached;
    }

    void complete(BitmapHunter hunter) {
        Action single = hunter.getAction();
        List<Action> joined = hunter.getActions();

        boolean hasMultiple = joined != null && !joined.isEmpty();
        boolean shouldDeliver = single != null || hasMultiple;

        if (!shouldDeliver)
            return;

        Uri uri = hunter.getData().uri;
        Exception exception = hunter.getException();
        Bitmap result = hunter.getResult();
        LoadedFrom from = hunter.getLoadedFrom();

        if (single != null)
            deliverAction(result, from, single, exception);

        if (hasMultiple) {
            for (int i = 0, n = joined.size(); i < n; i++) {
                Action join = joined.get(i);
                deliverAction(result, from, join, exception);
            }
        }

        if (listener != null && exception != null)
            listener.onImageLoadFailed(this, uri, exception);
    }

    void resumeAction(Action action) {
        Bitmap bitmap;

        bitmap = quickMemoryCacheCheck(action.getKey());

        if (bitmap != null) {
            // Resumed action is cached, complete immediately.
            deliverAction(bitmap, MEMORY, action, null);
        } else {
            // Re-submit the action to the executor.
            enqueueAndSubmit(action);
        }
    }

    private void deliverAction(Bitmap result, LoadedFrom from, Action action, Exception e) {
        if (action.isCancelled())
            return;

        if (!action.willReplay())
            targetToAction.remove(action.getTarget());

        if (result != null) {
            if (from == null)
                throw new AssertionError("LoadedFrom cannot be null.");

            action.complete(result, from);
        } else {
            action.error(e);
        }
    }

    void cancelExistingRequest(Object target) {
        checkMain();
        Action action = targetToAction.remove(target);
        if (action != null) {
            action.cancel();
            dispatcher.dispatchCancel(action);
        }
        if (target instanceof ImageView targetImageView) {
            DeferredRequestCreator deferredRequestCreator = targetToDeferredRequestCreator.remove(targetImageView);
            if (deferredRequestCreator != null)
                deferredRequestCreator.cancel();
        }
    }

    public enum Priority {
        LOW,
        NORMAL,
        HIGH
    }

    public enum LoadedFrom {
        MEMORY(Color.GREEN),
        DISK(Color.BLUE),
        NETWORK(Color.RED);

        final int debugColor;

        LoadedFrom(int debugColor) {
            this.debugColor = debugColor;
        }
    }

    public interface Listener {
        /**
         * Invoked when an image has failed to load. This is useful for reporting image failures to a
         * remote analytics service, for example.
         */
        void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception);
    }

    public interface RequestTransformer {
        /**
         * A {@link RequestTransformer} which returns the original request.
         */
        RequestTransformer IDENTITY = request -> request;

        /**
         * Transform a request before it is submitted to be processed.
         *
         * @return The original request or a new request to replace it. Must not be null.
         */
        Request transformRequest(Request request);
    }

    private static class CleanupThread extends Thread {
        private final ReferenceQueue<Object> referenceQueue;
        private final Handler handler;

        CleanupThread(ReferenceQueue<Object> referenceQueue) {
            this.referenceQueue = referenceQueue;
            this.handler = Picasso.HANDLER;
            setDaemon(true);
            setName(THREAD_PREFIX + "refQueue");
        }

        @Override
        public void run() {
            Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND);
            while (true) {
                try {
                    // Prior to Android 5.0, even when there is no local variable, the result from
                    // remove() & obtainMessage() is kept as a stack local variable.
                    // We're forcing this reference to be cleared and replaced by looping every second
                    // when there is nothing to do.
                    // This behavior has been tested and reproduced with heap dumps.
                    RequestWeakReference<?> remove = (RequestWeakReference<?>) referenceQueue.remove(THREAD_LEAK_CLEANING_MS);
                    Message message = handler.obtainMessage();
                    if (remove != null) {
                        message.what = REQUEST_GCED;
                        message.obj = remove.action;
                        handler.sendMessage(message);
                    } else {
                        message.recycle();
                    }
                } catch (InterruptedException e) {
                    break;
                } catch (final Exception e) {
                    handler.post(() -> {
                        throw new RuntimeException(e);
                    });
                    break;
                }
            }
        }

    }

}
