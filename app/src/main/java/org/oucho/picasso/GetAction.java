package org.oucho.picasso;

import android.graphics.Bitmap;

/** @noinspection unused */
class GetAction extends Action<Void> {
    GetAction(Picasso picasso, Request data, Object tag,
              String key) {
        super(picasso, null, data, 0, null, key, tag);
    }

    @Override
    void complete(Bitmap result, Picasso.LoadedFrom from) {
    }

    @Override
    public void error(Exception e) {
    }
}
