package org.oucho.picasso;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/** @noinspection unused*/
public final class OkHttp3Downloader implements Downloader {
    @VisibleForTesting
    final Call.Factory client;

    public OkHttp3Downloader(final Context context) {
        this(PicassoUtils.createDefaultCacheDir(context));
    }

    public OkHttp3Downloader(final File cacheDir) {
        this(cacheDir, PicassoUtils.calculateDiskCacheSize(cacheDir));
    }

    public OkHttp3Downloader(final File cacheDir, final long maxSize) {
        this(new OkHttpClient.Builder().cache(new Cache(cacheDir, maxSize)).build());
    }

    public OkHttp3Downloader(OkHttpClient client) {
        this.client = client;
    }

    @NonNull
    @Override
    public Response load(@NonNull Request request) throws IOException {
        return client.newCall(request).execute();
    }

}
