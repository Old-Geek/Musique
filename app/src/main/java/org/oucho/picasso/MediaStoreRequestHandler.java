package org.oucho.picasso;

import static android.content.ContentResolver.SCHEME_CONTENT;
import static org.oucho.picasso.Picasso.LoadedFrom.DISK;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;
import java.io.InputStream;

import okio.Okio;
import okio.Source;

class MediaStoreRequestHandler extends ContentStreamRequestHandler {

    /** @noinspection unused*/
    MediaStoreRequestHandler(Context context) {
        super(context);
    }

    @Override
    public boolean canHandleRequest(Request data) {
        final Uri uri = data.uri;
        return (SCHEME_CONTENT.equals(uri.getScheme())
                && MediaStore.AUTHORITY.equals(uri.getAuthority()));
    }

    public Bitmap getBitmapFromUri(ContentResolver contentResolver, Uri uri) throws IOException {
        try (InputStream inputStream = contentResolver.openInputStream(uri)) {
            if (inputStream == null) {
                return null;
            }
            return BitmapFactory.decodeStream(inputStream);
        }
    }

    // Usage in your load method
    @Override
    public Result load(Request request) throws IOException {
        ContentResolver contentResolver = context.getContentResolver();

        Bitmap bitmap = getBitmapFromUri(contentResolver, request.uri);
        if (bitmap != null) {
            return new Result(bitmap, null, DISK);
        }

        Source source = Okio.source(getInputStream(request));
        return new Result(null, source, DISK);
    }
}
