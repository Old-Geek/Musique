package org.oucho.picasso;

/** @noinspection EmptyMethod, unused */
public interface Callback {
    void onSuccess();

    void onError(Exception e);

    class EmptyCallback implements Callback {

        @Override
        public void onSuccess() {
        }

        @Override
        public void onError(Exception e) {
        }
    }
}
