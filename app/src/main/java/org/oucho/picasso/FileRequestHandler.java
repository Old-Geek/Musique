package org.oucho.picasso;

import static android.content.ContentResolver.SCHEME_FILE;
import static org.oucho.picasso.Picasso.LoadedFrom.DISK;

import android.content.Context;

import java.io.IOException;

import okio.Okio;
import okio.Source;

class FileRequestHandler extends ContentStreamRequestHandler {

    /** @noinspection unused*/
    FileRequestHandler(Context context) {
        super(context);
    }

    @Override
    public boolean canHandleRequest(Request data) {
        return SCHEME_FILE.equals(data.uri.getScheme());
    }

    @Override
    public Result load(Request request) throws IOException {
        Source source = Okio.source(getInputStream(request));
        return new Result(null, source, DISK);
    }
}
