package org.oucho.picasso;

import static org.oucho.picasso.Picasso.LoadedFrom.MEMORY;
import static org.oucho.picasso.Picasso.Priority;
import static org.oucho.picasso.Picasso.Priority.LOW;
import static org.oucho.picasso.PicassoUtils.THREAD_IDLE_NAME;
import static org.oucho.picasso.PicassoUtils.THREAD_PREFIX;

import android.graphics.Bitmap;
import android.net.NetworkCapabilities;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import okio.BufferedSource;
import okio.Okio;
import okio.Source;

/** @noinspection rawtypes*/
class BitmapHunter implements Runnable {

    private static final Object DECODE_LOCK = new Object();
    private static final ThreadLocal<StringBuilder> NAME_BUILDER = ThreadLocal.withInitial(() -> new StringBuilder(THREAD_PREFIX));
    private static final AtomicInteger SEQUENCE_GENERATOR = new AtomicInteger();

    private static final RequestHandler ERRORING_HANDLER = new RequestHandler() {
        @Override
        public boolean canHandleRequest(Request data) {
            return true;
        }

        @Override
        public Result load(Request request) {
            throw new IllegalStateException("Unrecognized type of request: " + request);
        }
    };

    final int sequence;
    final Picasso picasso;
    final Dispatcher dispatcher;
    final Cache cache;
    final Stats stats;
    final String key;
    final Request data;
    final RequestHandler requestHandler;
    Action action;
    List<Action> actions;
    Bitmap result;
    Future<?> future;
    Picasso.LoadedFrom loadedFrom;
    Exception exception;
    int retryCount;
    Priority priority;

    BitmapHunter(Picasso picasso, Dispatcher dispatcher, Cache cache, Stats stats, Action action,
                 RequestHandler requestHandler) {
        this.sequence = SEQUENCE_GENERATOR.incrementAndGet();
        this.picasso = picasso;
        this.dispatcher = dispatcher;
        this.cache = cache;
        this.stats = stats;
        this.action = action;
        this.key = action.getKey();
        this.data = action.getRequest();
        this.priority = action.getPriority();
        this.requestHandler = requestHandler;
        this.retryCount = requestHandler.getRetryCount();
    }

    static Bitmap decodeStream(Source source) throws IOException {
        BufferedSource bufferedSource = Okio.buffer(source);
        byte[] bytes = bufferedSource.readByteArray();

        // Use OpenCV to decode the image
        Mat mat = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_COLOR);
        if (mat.empty())
            throw new IOException("Failed to decode stream with OpenCV.");

        // Convert BGR to RGB
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGB);

        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);
        return bitmap;
    }

    static void updateThreadName(Request data) {
        String name = data.getName();
        StringBuilder builder = NAME_BUILDER.get();
        assert builder != null;
        builder.ensureCapacity(THREAD_PREFIX.length() + name.length());
        builder.replace(THREAD_PREFIX.length(), builder.length(), name);
        Thread.currentThread().setName(builder.toString());
    }

    static BitmapHunter forRequest(Picasso picasso, Dispatcher dispatcher, Cache cache, Stats stats,
                                   Action action) {
        Request request = action.getRequest();
        List<RequestHandler> requestHandlers = picasso.getRequestHandlers();
        for (int i = 0, count = requestHandlers.size(); i < count; i++) {
            RequestHandler requestHandler = requestHandlers.get(i);
            if (requestHandler.canHandleRequest(request))
                return new BitmapHunter(picasso, dispatcher, cache, stats, action, requestHandler);
        }
        return new BitmapHunter(picasso, dispatcher, cache, stats, action, ERRORING_HANDLER);
    }

    static Bitmap applyCustomTransformations(List<Transformation> transformations, Bitmap result) {
        for (int i = 0, count = transformations.size(); i < count; i++) {
            final Transformation transformation = transformations.get(i);
            Bitmap newResult;
            try {
                newResult = transformation.transform(result);
            } catch (final RuntimeException e) {
                Picasso.HANDLER.post(() -> {
                    throw new RuntimeException("Transformation " + transformation.key() + " crashed with exception.", e);
                });
                return null;
            }

            if (newResult == null) {
                final StringBuilder builder = new StringBuilder()
                        .append("Transformation ")
                        .append(transformation.key())
                        .append(" returned null after ")
                        .append(i)
                        .append(" previous transformation(s).\n\nTransformation list:\n");

                for (Transformation t : transformations)
                    builder.append(t.key()).append('\n');

                Picasso.HANDLER.post(() -> {
                    throw new NullPointerException(builder.toString());
                });
                return null;
            }

            if (newResult == result && result.isRecycled()) {
                Picasso.HANDLER.post(() -> {
                    throw new IllegalStateException("Transformation " + transformation.key() + " returned input Bitmap but recycled it.");
                });
                return null;
            }

            if (newResult != result && !result.isRecycled()) {
                Picasso.HANDLER.post(() -> {
                    throw new IllegalStateException("Transformation " + transformation.key() + " mutated input Bitmap but failed to recycle the original.");
                });
                return null;
            }

            result = newResult;
        }
        return result;
    }

    static Bitmap transformResult(Request data, Bitmap result) {
        Mat src = new Mat();
        Utils.bitmapToMat(result, src);

        int inWidth = src.cols();
        int inHeight = src.rows();
        boolean onlyScaleDown = data.onlyScaleDown;

        int drawWidth;
        int drawHeight;

        Mat dst = new Mat();

        if (data.needsMatrixTransform()) {
            int targetWidth = data.targetWidth;
            int targetHeight = data.targetHeight;

            boolean isUpscaling = (targetWidth > inWidth) || (targetHeight > inHeight);

            if (data.centerCrop) {
                float widthRatio = targetWidth != 0 ? targetWidth / (float) inWidth : targetHeight / (float) inHeight;
                float heightRatio = targetHeight != 0 ? targetHeight / (float) inHeight : targetWidth / (float) inWidth;
                float scaleX, scaleY;
                if (widthRatio > heightRatio) {

                    drawHeight = (int) Math.ceil(inHeight * (heightRatio / widthRatio));
                    scaleX = widthRatio;
                    scaleY = targetHeight / (float) drawHeight;
                } else if (widthRatio < heightRatio) {

                    drawWidth = (int) Math.ceil(inWidth * (widthRatio / heightRatio));
                    scaleX = targetWidth / (float) drawWidth;
                    scaleY = heightRatio;
                } else {
                    scaleX = scaleY = heightRatio;
                }
                if (shouldResize(onlyScaleDown, inWidth, inHeight, targetWidth, targetHeight)) {

                    if (isUpscaling)
                        Imgproc.resize(src, dst, new Size(targetWidth, targetHeight), 0, 0, Imgproc.INTER_CUBIC);
                    else
                        Imgproc.resize(src, dst, new Size(), scaleX, scaleY, Imgproc.INTER_LINEAR);

                    src = dst;
                }
            } else if (data.centerInside) {
                float widthRatio = targetWidth != 0 ? targetWidth / (float) inWidth : targetHeight / (float) inHeight;
                float heightRatio = targetHeight != 0 ? targetHeight / (float) inHeight : targetWidth / (float) inWidth;
                float scale = Math.min(widthRatio, heightRatio);
                if (shouldResize(onlyScaleDown, inWidth, inHeight, targetWidth, targetHeight)) {

                    if (isUpscaling)
                        Imgproc.resize(src, dst, new Size(targetWidth, targetHeight), 0, 0, Imgproc.INTER_CUBIC);
                    else
                        Imgproc.resize(src, dst, new Size(), scale, scale, Imgproc.INTER_LINEAR);

                    src = dst;
                }
            } else if ((targetWidth != 0 || targetHeight != 0) && (targetWidth != inWidth || targetHeight != inHeight)) {
                float sx = targetWidth != 0 ? targetWidth / (float) inWidth : targetHeight / (float) inHeight;
                float sy = targetHeight != 0 ? targetHeight / (float) inHeight : targetWidth / (float) inWidth;
                if (shouldResize(onlyScaleDown, inWidth, inHeight, targetWidth, targetHeight)) {

                    if (isUpscaling)
                        Imgproc.resize(src, dst, new Size(targetWidth, targetHeight), 0, 0, Imgproc.INTER_CUBIC);
                    else
                        Imgproc.resize(src, dst, new Size(), sx, sy, Imgproc.INTER_LINEAR);

                    src = dst;
                }
            }

            // Apply anti-aliasing for small images
            if (targetWidth <= 50 || targetHeight <= 50) {
                // Apply a mild blur
                Imgproc.GaussianBlur(src, dst, new Size(1, 1), 0);
                src = dst;

                // Apply sharpening to counteract the blur
                Mat sharpened = new Mat();
                Core.addWeighted(src, 1.5, dst, -0.5, 0, sharpened);
                src = sharpened;
            }
        }

        Bitmap newResult = Bitmap.createBitmap(src.cols(), src.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(src, newResult);
        if (newResult != result) {
            result.recycle();
            result = newResult;
        }

        return result;
    }

    private static boolean shouldResize(boolean onlyScaleDown, int inWidth, int inHeight, int targetWidth, int targetHeight) {
        return !onlyScaleDown || (targetWidth != 0 && inWidth > targetWidth) || (targetHeight != 0 && inHeight > targetHeight);
    }

    @Override
    public void run() {
        try {
            updateThreadName(data);
            result = hunt();

            if (result == null)
                dispatcher.dispatchFailed(this);
            else
                dispatcher.dispatchComplete(this);

        } catch (NetworkRequestHandler.ResponseException e) {
            if (e.code != 504)
                exception = e;

            dispatcher.dispatchFailed(this);
        } catch (IOException e) {
            exception = e;
            dispatcher.dispatchRetry(this);
        } catch (OutOfMemoryError e) {
            StringWriter writer = new StringWriter();
            stats.createSnapshot().dump(new PrintWriter(writer));
            exception = new RuntimeException(writer.toString(), e);
            dispatcher.dispatchFailed(this);
        } catch (Exception e) {
            exception = e;
            dispatcher.dispatchFailed(this);
        } finally {
            Thread.currentThread().setName(THREAD_IDLE_NAME);
        }
    }

    Bitmap hunt() throws IOException {
        Bitmap bitmap;

        bitmap = cache.get(key);
        if (bitmap != null) {
            stats.dispatchCacheHit();
            loadedFrom = MEMORY;
            return bitmap;
        }


        RequestHandler.Result result = requestHandler.load(data);
        if (result != null) {
            loadedFrom = result.getLoadedFrom();
            bitmap = result.getBitmap();

            if (bitmap == null) {
                try (Source source = result.getSource()) {
                    bitmap = decodeStream(source);
                }
            }
        }

        if (bitmap != null) {
            stats.dispatchBitmapDecoded(bitmap);
            if (data.needsTransformation()) {
                synchronized (DECODE_LOCK) {
                    if (data.needsMatrixTransform()) {
                        bitmap = transformResult(data, bitmap);
                    }
                    if (data.hasCustomTransformations()) {
                        bitmap = applyCustomTransformations(data.transformations, bitmap);
                    }
                }

                if (bitmap != null)
                    stats.dispatchBitmapTransformed(bitmap);
            }
        }

        return bitmap;
    }

    void attach(Action action) {

        if (this.action == null) {
            this.action = action;
            return;
        }

        if (actions == null) {
            actions = new ArrayList<>(3);
        }

        actions.add(action);

        Priority actionPriority = action.getPriority();
        if (actionPriority.ordinal() > priority.ordinal())
            priority = actionPriority;

    }

    void detach(Action action) {
        boolean detached = false;
        if (this.action == action) {
            this.action = null;
            detached = true;
        } else if (actions != null) {
            detached = actions.remove(action);
        }

        if (detached && action.getPriority() == priority)
            priority = computeNewPriority();
    }

    private Priority computeNewPriority() {
        Priority newPriority = LOW;

        boolean hasMultiple = actions != null && !actions.isEmpty();
        boolean hasAny = action != null || hasMultiple;

        if (!hasAny)
            return newPriority;

        if (action != null)
            newPriority = action.getPriority();

        if (hasMultiple) {
            for (int i = 0, n = actions.size(); i < n; i++) {
                Priority actionPriority = actions.get(i).getPriority();

                if (actionPriority.ordinal() > newPriority.ordinal())
                    newPriority = actionPriority;
            }
        }

        return newPriority;
    }

    boolean cancel() {
        return action == null
                && (actions == null || actions.isEmpty())
                && future != null
                && future.cancel(false);
    }

    boolean isCancelled() {
        return future != null && future.isCancelled();
    }

    boolean shouldRetry(NetworkCapabilities networkCapabilities) {
        boolean hasRetries = retryCount > 0;
        if (!hasRetries)
            return false;

        retryCount--;
        return requestHandler.shouldRetry(networkCapabilities);
    }

    Bitmap getResult() {
        return result;
    }

    String getKey() {
        return key;
    }

    Request getData() {
        return data;
    }

    Action getAction() {
        return action;
    }

    List<Action> getActions() {
        return actions;
    }

    Exception getException() {
        return exception;
    }

    Picasso.LoadedFrom getLoadedFrom() {
        return loadedFrom;
    }

    Priority getPriority() {
        return priority;
    }
}
