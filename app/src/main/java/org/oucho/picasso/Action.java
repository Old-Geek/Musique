package org.oucho.picasso;

import static org.oucho.picasso.Picasso.Priority;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/** @noinspection rawtypes*/
abstract class Action<T> {
    final Picasso picasso;
    final Request request;
    final WeakReference<T> target;
    final int errorResId;
    final Drawable errorDrawable;
    final String key;
    final Object tag;
    boolean willReplay;
    boolean cancelled;

    Action(Picasso picasso, T target, Request request,
           int errorResId, Drawable errorDrawable, String key, Object tag) {
        this.picasso = picasso;
        this.request = request;
        this.target = target == null ? null : new RequestWeakReference<>(this, target, picasso.referenceQueue);
        this.errorResId = errorResId;
        this.errorDrawable = errorDrawable;
        this.key = key;
        this.tag = (tag != null ? tag : this);
    }

    abstract void complete(Bitmap result, Picasso.LoadedFrom from);

    abstract void error(Exception e);

    void cancel() {
        cancelled = true;
    }

    Request getRequest() {
        return request;
    }

    T getTarget() {
        return target == null ? null : target.get();
    }

    String getKey() {
        return key;
    }

    boolean isCancelled() {
        return cancelled;
    }

    boolean willReplay() {
        return willReplay;
    }

    Picasso getPicasso() {
        return picasso;
    }

    Priority getPriority() {
        return request.priority;
    }

    Object getTag() {
        return tag;
    }

    static class RequestWeakReference<M> extends WeakReference<M> {
        final Action action;

        RequestWeakReference(Action action, M referent, ReferenceQueue<? super M> q) {
            super(referent, q);
            this.action = action;
        }
    }
}
