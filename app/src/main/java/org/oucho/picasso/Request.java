package org.oucho.picasso;

import static java.util.Collections.unmodifiableList;

import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;

import org.oucho.picasso.Picasso.Priority;

import java.util.ArrayList;
import java.util.List;

/**
 * Immutable data about an image and the transformations that will be applied to it.
 * @noinspection unused
 */
public final class Request {

    public final Uri uri;

    public final int resourceId;

    public final String stableKey;

    public final List<Transformation> transformations;

    public final int targetWidth;

    public final int targetHeight;

    public final boolean centerCrop;

    public final int centerCropGravity;

    public final boolean centerInside;
    public final boolean onlyScaleDown;

    public final boolean purgeable;

    public final Bitmap.Config config;

    public final Priority priority;

    int id;

    long started;

    private Request(Uri uri, int resourceId, String stableKey, List<Transformation> transformations,
                    int targetWidth, int targetHeight, boolean centerCrop, boolean centerInside,
                    int centerCropGravity, boolean onlyScaleDown,
                    boolean purgeable, Bitmap.Config config, Priority priority) {
        this.uri = uri;
        this.resourceId = resourceId;
        this.stableKey = stableKey;
        if (transformations == null) {
            this.transformations = null;
        } else {
            this.transformations = unmodifiableList(transformations);
        }
        this.targetWidth = targetWidth;
        this.targetHeight = targetHeight;
        this.centerCrop = centerCrop;
        this.centerInside = centerInside;
        this.centerCropGravity = centerCropGravity;
        this.onlyScaleDown = onlyScaleDown;
        this.purgeable = purgeable;
        this.config = config;
        this.priority = priority;
    }

    @NonNull
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("Request{");
        if (resourceId > 0) {
            builder.append(resourceId);
        } else {
            builder.append(uri);
        }
        if (transformations != null && !transformations.isEmpty()) {
            for (Transformation transformation : transformations) {
                builder.append(' ').append(transformation.key());
            }
        }
        if (stableKey != null) {
            builder.append(" stableKey(").append(stableKey).append(')');
        }
        if (targetWidth > 0) {
            builder.append(" resize(").append(targetWidth).append(',').append(targetHeight).append(')');
        }
        if (centerCrop) {
            builder.append(" centerCrop");
        }
        if (centerInside) {
            builder.append(" centerInside");
        }
        if (purgeable) {
            builder.append(" purgeable");
        }
        if (config != null) {
            builder.append(' ').append(config);
        }
        builder.append('}');

        return builder.toString();
    }

    String getName() {
        if (uri != null) {
            return String.valueOf(uri.getPath());
        }
        return Integer.toHexString(resourceId);
    }

    public boolean hasSize() {
        return targetWidth != 0 || targetHeight != 0;
    }

    boolean needsTransformation() {
        return needsMatrixTransform() || hasCustomTransformations();
    }

    boolean needsMatrixTransform() {
        return hasSize();
    }

    boolean hasCustomTransformations() {
        return transformations != null;
    }

    public static final class Builder {
        private Uri uri;
        private int resourceId;
        private String stableKey;
        private int targetWidth;
        private int targetHeight;
        private boolean centerCrop;
        private int centerCropGravity;
        private boolean centerInside;
        private boolean onlyScaleDown;
        private boolean purgeable;
        private List<Transformation> transformations;
        private Bitmap.Config config;
        private Priority priority;

        public Builder(@NonNull Uri uri) {
            setUri(uri);
        }

        public Builder(@DrawableRes int resourceId) {
            setResourceId(resourceId);
        }

        Builder(Uri uri, int resourceId, Bitmap.Config bitmapConfig) {
            this.uri = uri;
            this.resourceId = resourceId;
            this.config = bitmapConfig;
        }

        boolean hasImage() {
            return uri != null || resourceId != 0;
        }

        boolean hasSize() {
            return targetWidth != 0 || targetHeight != 0;
        }

        boolean hasPriority() {
            return priority != null;
        }

        public Builder setUri(@NonNull Uri uri) {
            this.uri = uri;
            this.resourceId = 0;
            return this;
        }

        public Builder setResourceId(@DrawableRes int resourceId) {
            if (resourceId == 0) {
                throw new IllegalArgumentException("Image resource ID may not be 0.");
            }
            this.resourceId = resourceId;
            this.uri = null;
            return this;
        }

        public Builder stableKey(@Nullable String stableKey) {
            this.stableKey = stableKey;
            return this;
        }

        public Builder resize(@Px int targetWidth, @Px int targetHeight) {
            if (targetWidth < 0) {
                throw new IllegalArgumentException("Width must be positive number or 0.");
            }
            if (targetHeight < 0) {
                throw new IllegalArgumentException("Height must be positive number or 0.");
            }
            if (targetHeight == 0 && targetWidth == 0) {
                throw new IllegalArgumentException("At least one dimension has to be positive number.");
            }
            this.targetWidth = targetWidth;
            this.targetHeight = targetHeight;
            return this;
        }

        public Builder centerCrop(int alignGravity) {
            if (centerInside) {
                throw new IllegalStateException("Center crop can not be used after calling centerInside");
            }
            centerCrop = true;
            centerCropGravity = alignGravity;
            return this;
        }

        public Builder centerInside() {
            if (centerCrop) {
                throw new IllegalStateException("Center inside can not be used after calling centerCrop");
            }
            centerInside = true;
            return this;
        }

        public Builder onlyScaleDown() {
            if (targetHeight == 0 && targetWidth == 0) {
                throw new IllegalStateException("onlyScaleDown can not be applied without resize");
            }
            onlyScaleDown = true;
            return this;
        }

        public Builder purgeable() {
            purgeable = true;
            return this;
        }

        public Builder config(@NonNull Bitmap.Config config) {
            this.config = config;
            return this;
        }

        public Builder priority(@NonNull Priority priority) {
            if (this.priority != null) {
                throw new IllegalStateException("Priority already set.");
            }
            this.priority = priority;
            return this;
        }

        public Builder transform(@NonNull Transformation transformation) {
            if (transformation.key() == null) {
                throw new IllegalArgumentException("Transformation key must not be null.");
            }
            if (transformations == null) {
                transformations = new ArrayList<>(2);
            }
            transformations.add(transformation);
            return this;
        }

        public Builder transform(@NonNull List<? extends Transformation> transformations) {
            for (int i = 0, size = transformations.size(); i < size; i++) {
                transform(transformations.get(i));
            }
            return this;
        }

        public Request build() {
            if (centerInside && centerCrop) {
                throw new IllegalStateException("Center crop and center inside can not be used together.");
            }
            if (centerCrop && (targetWidth == 0 && targetHeight == 0)) {
                throw new IllegalStateException(
                        "Center crop requires calling resize with positive width and height.");
            }
            if (centerInside && (targetWidth == 0 && targetHeight == 0)) {
                throw new IllegalStateException(
                        "Center inside requires calling resize with positive width and height.");
            }
            if (priority == null) {
                priority = Priority.NORMAL;
            }
            return new Request(uri, resourceId, stableKey, transformations, targetWidth, targetHeight,
                    centerCrop, centerInside, centerCropGravity, onlyScaleDown, purgeable, config, priority);
        }
    }
}
