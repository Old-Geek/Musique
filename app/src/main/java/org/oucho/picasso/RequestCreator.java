package org.oucho.picasso;

import static org.oucho.picasso.BitmapHunter.forRequest;
import static org.oucho.picasso.Picasso.LoadedFrom.MEMORY;
import static org.oucho.picasso.Picasso.Priority;
import static org.oucho.picasso.PicassoDrawable.setBitmap;
import static org.oucho.picasso.PicassoDrawable.setPlaceholder;
import static org.oucho.picasso.PicassoUtils.checkMain;
import static org.oucho.picasso.PicassoUtils.checkNotMain;
import static org.oucho.picasso.PicassoUtils.createKey;
import static org.oucho.picasso.RemoteViewsAction.AppWidgetAction;
import static org.oucho.picasso.RemoteViewsAction.NotificationAction;

import android.app.Notification;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.RemoteViews;

import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/** @noinspection rawtypes, unused */
// Public API.
public class RequestCreator {
    private static final AtomicInteger nextId = new AtomicInteger();

    private final Picasso picasso;
    private final Request.Builder data;

    private boolean deferred;
    private final boolean setPlaceholder = true;
    private int placeholderResId;
    private int errorResId;
    private Drawable placeholderDrawable;
    private Drawable errorDrawable;
    private Object tag;

    RequestCreator(Picasso picasso, Uri uri, int resourceId) {
        this.picasso = picasso;
        this.data = new Request.Builder(uri, resourceId, picasso.defaultBitmapConfig);
    }

    public RequestCreator placeholder(@DrawableRes int placeholderResId) {
        if (!setPlaceholder) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        }
        if (placeholderResId == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        }
        if (placeholderDrawable != null) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        this.placeholderResId = placeholderResId;
        return this;
    }

    public RequestCreator placeholder(@NonNull Drawable placeholderDrawable) {
        if (!setPlaceholder) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        }
        if (placeholderResId != 0) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        this.placeholderDrawable = placeholderDrawable;
        return this;
    }

    public RequestCreator error(@DrawableRes int errorResId) {
        if (errorResId == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        }
        if (errorDrawable != null) {
            throw new IllegalStateException("Error image already set.");
        }
        this.errorResId = errorResId;
        return this;
    }

    public RequestCreator error(@NonNull Drawable errorDrawable) {
        if (errorResId != 0) {
            throw new IllegalStateException("Error image already set.");
        }
        this.errorDrawable = errorDrawable;
        return this;
    }

    public RequestCreator tag(@NonNull Object tag) {
        if (this.tag != null) {
            throw new IllegalStateException("Tag already set.");
        }
        this.tag = tag;
        return this;
    }

    public RequestCreator fit() {
        deferred = true;
        return this;
    }

    RequestCreator unfit() {
        deferred = false;
        return this;
    }

    RequestCreator clearTag() {
        this.tag = null;
        return this;
    }

    Object getTag() {
        return tag;
    }

    public RequestCreator resize(int targetWidth, int targetHeight) {
        data.resize(targetWidth, targetHeight);
        return this;
    }

    public RequestCreator centerCrop() {
        data.centerCrop(Gravity.CENTER);
        return this;
    }

    public RequestCreator centerCrop(int alignGravity) {
        data.centerCrop(alignGravity);
        return this;
    }

    public RequestCreator centerInside() {
        data.centerInside();
        return this;
    }

    public Bitmap get() throws IOException {
        long started = System.nanoTime();
        checkNotMain();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with get.");
        }
        if (!data.hasImage()) {
            return null;
        }

        Request finalData = createRequest(started);
        String key = createKey(finalData, new StringBuilder());

        Action action = new GetAction(picasso, finalData, tag, key);
        return forRequest(picasso, picasso.dispatcher, picasso.cache, picasso.stats, action).hunt();
    }

    public void fetch() {
        fetch(null);
    }

    public void fetch(@Nullable Callback callback) {
        long started = System.nanoTime();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with fetch.");
        }
        if (data.hasImage()) {
            // Fetch requests have lower priority by default.
            if (!data.hasPriority()) {
                data.priority(Priority.LOW);
            }

            Request request = createRequest(started);
            String key = createKey(request, new StringBuilder());

            Bitmap bitmap = picasso.quickMemoryCacheCheck(key);
            if (bitmap != null) {
                if (callback != null) {
                    callback.onSuccess();
                }
                return;
            }

            Action action = new FetchAction(picasso, request, tag, key, callback);
            picasso.submit(action);
        }
    }

    public void into(@NonNull Target target) {
        long started = System.nanoTime();
        checkMain();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        }

        if (!data.hasImage()) {
            picasso.cancelRequest(target);
            target.onPrepareLoad(setPlaceholder ? getPlaceholderDrawable() : null);
            return;
        }

        Request request = createRequest(started);
        String requestKey = createKey(request);

        Bitmap bitmap = picasso.quickMemoryCacheCheck(requestKey);
        if (bitmap != null) {
            picasso.cancelRequest(target);
            target.onBitmapLoaded(bitmap, MEMORY);
            return;
        }

        target.onPrepareLoad(setPlaceholder ? getPlaceholderDrawable() : null);

        Action action = new TargetAction(picasso, target, request, errorDrawable, requestKey, tag, errorResId);
        picasso.enqueueAndSubmit(action);
    }

    public void into(@NonNull RemoteViews remoteViews, @IdRes int viewId, int notificationId,
                     @NonNull Notification notification) {
        into(remoteViews, viewId, notificationId, notification, null);
    }

    public void into(@NonNull RemoteViews remoteViews, @IdRes int viewId, int notificationId,
                     @NonNull Notification notification, @Nullable String notificationTag) {
        into(remoteViews, viewId, notificationId, notification, notificationTag, null);
    }

    public void into(@NonNull RemoteViews remoteViews, @IdRes int viewId, int notificationId,
                     @NonNull Notification notification, @Nullable String notificationTag, Callback callback) {
        long started = System.nanoTime();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with RemoteViews.");
        }
        if (placeholderDrawable != null || placeholderResId != 0 || errorDrawable != null) {
            throw new IllegalArgumentException(
                    "Cannot use placeholder or error drawables with remote views.");
        }

        Request request = createRequest(started);
        String key = createKey(request, new StringBuilder()); // Non-main thread needs own builder.

        RemoteViewsAction action =
                new NotificationAction(picasso, request, remoteViews, viewId, notificationId, notification,
                        notificationTag, key, tag, errorResId, callback);

        performRemoteViewInto(action);
    }

    public void into(@NonNull RemoteViews remoteViews, @IdRes int viewId,
                     @NonNull int[] appWidgetIds) {
        into(remoteViews, viewId, appWidgetIds, null);
    }

    public void into(@NonNull RemoteViews remoteViews, @IdRes int viewId, @NonNull int[] appWidgetIds,
                     Callback callback) {
        long started = System.nanoTime();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with remote views.");
        }
        if (placeholderDrawable != null || placeholderResId != 0 || errorDrawable != null) {
            throw new IllegalArgumentException(
                    "Cannot use placeholder or error drawables with remote views.");
        }

        Request request = createRequest(started);
        String key = createKey(request, new StringBuilder()); // Non-main thread needs own builder.

        RemoteViewsAction action =
                new AppWidgetAction(picasso, request, remoteViews, viewId, appWidgetIds,
                        key, tag, errorResId, callback);

        performRemoteViewInto(action);
    }

    public void into(ImageView target) {
        into(target, null);
    }

    public void into(ImageView target, Callback callback) {
        long started = System.nanoTime();
        checkMain();

        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        }

        if (!data.hasImage()) {
            picasso.cancelRequest(target);
            if (setPlaceholder) {
                setPlaceholder(target, getPlaceholderDrawable());
            }
            return;
        }

        if (deferred) {
            if (data.hasSize()) {
                throw new IllegalStateException("Fit cannot be used with resize.");
            }
            int width = target.getWidth();
            int height = target.getHeight();
            if (width == 0 || height == 0) {
                if (setPlaceholder) {
                    setPlaceholder(target, getPlaceholderDrawable());
                }
                picasso.defer(target, new DeferredRequestCreator(this, target, callback));
                return;
            }
            data.resize(width, height);
        }

        Request request = createRequest(started);
        String requestKey = createKey(request);

        Bitmap bitmap = picasso.quickMemoryCacheCheck(requestKey);
        if (bitmap != null) {
            picasso.cancelRequest(target);
            setBitmap(target, picasso.context, bitmap, MEMORY);
            if (callback != null) {
                callback.onSuccess();
            }
            return;
        }


        if (setPlaceholder) {
            setPlaceholder(target, getPlaceholderDrawable());
        }

        Action action = new ImageViewAction(picasso, target, request, errorResId, errorDrawable, requestKey, tag, callback);

        picasso.enqueueAndSubmit(action);
    }

    private Drawable getPlaceholderDrawable() {
        if (placeholderResId != 0) {
            return AppCompatResources.getDrawable(picasso.context, placeholderResId);
        } else {
            return placeholderDrawable; // This may be null which is expected and desired behavior.
        }
    }

    private Request createRequest(long started) {
        int id = nextId.getAndIncrement();

        Request request = data.build();
        request.id = id;
        request.started = started;

        Request transformed = picasso.transformRequest(request);
        if (transformed != request) {
            // If the request was changed, copy over the id and timestamp from the original.
            transformed.id = id;
            transformed.started = started;
        }

        return transformed;
    }

    private void performRemoteViewInto(RemoteViewsAction action) {

        Bitmap bitmap = picasso.quickMemoryCacheCheck(action.getKey());
        if (bitmap != null) {
            action.complete(bitmap, MEMORY);
            return;
        }

        if (placeholderResId != 0) {
            action.setImageResource(placeholderResId);
        }

        picasso.enqueueAndSubmit(action);
    }
}
