package org.oucho.picasso;

import static org.oucho.picasso.Picasso.TAG;

import android.util.Log;

import androidx.annotation.NonNull;

import java.io.PrintWriter;
import java.io.StringWriter;

/** @noinspection unused*/

public record StatsSnapshot(int maxSize, int size, long cacheHits, long cacheMisses,
                            long totalDownloadSize, long totalOriginalBitmapSize,
                            long totalTransformedBitmapSize, long averageDownloadSize,
                            long averageOriginalBitmapSize, long averageTransformedBitmapSize,
                            int downloadCount, int originalBitmapCount, int transformedBitmapCount,
                            long timeStamp) {

    @SuppressWarnings("UnusedDeclaration")
    public void dump() {
        StringWriter logWriter = new StringWriter();
        dump(new PrintWriter(logWriter));
        Log.i(TAG, logWriter.toString());
    }

    public void dump(PrintWriter writer) {
        writer.println("===============BEGIN PICASSO STATS ===============");
        writer.println("Memory Cache Stats");
        writer.print("  Max Cache Size: ");
        writer.println(maxSize);
        writer.print("  Cache Size: ");
        writer.println(size);
        writer.print("  Cache % Full: ");
        writer.println((int) Math.ceil((float) size / maxSize * 100));
        writer.print("  Cache Hits: ");
        writer.println(cacheHits);
        writer.print("  Cache Misses: ");
        writer.println(cacheMisses);
        writer.println("Network Stats");
        writer.print("  Download Count: ");
        writer.println(downloadCount);
        writer.print("  Total Download Size: ");
        writer.println(totalDownloadSize);
        writer.print("  Average Download Size: ");
        writer.println(averageDownloadSize);
        writer.println("Bitmap Stats");
        writer.print("  Total Bitmaps Decoded: ");
        writer.println(originalBitmapCount);
        writer.print("  Total Bitmap Size: ");
        writer.println(totalOriginalBitmapSize);
        writer.print("  Total Transformed Bitmaps: ");
        writer.println(transformedBitmapCount);
        writer.print("  Total Transformed Bitmap Size: ");
        writer.println(totalTransformedBitmapSize);
        writer.print("  Average Bitmap Size: ");
        writer.println(averageOriginalBitmapSize);
        writer.print("  Average Transformed Bitmap Size: ");
        writer.println(averageTransformedBitmapSize);
        writer.println("===============END PICASSO STATS ===============");
        writer.flush();
    }

    @NonNull
    @Override
    public String toString() {
        return "StatsSnapshot{"
                + "maxSize="
                + maxSize
                + ", size="
                + size
                + ", cacheHits="
                + cacheHits
                + ", cacheMisses="
                + cacheMisses
                + ", downloadCount="
                + downloadCount
                + ", totalDownloadSize="
                + totalDownloadSize
                + ", averageDownloadSize="
                + averageDownloadSize
                + ", totalOriginalBitmapSize="
                + totalOriginalBitmapSize
                + ", totalTransformedBitmapSize="
                + totalTransformedBitmapSize
                + ", averageOriginalBitmapSize="
                + averageOriginalBitmapSize
                + ", averageTransformedBitmapSize="
                + averageTransformedBitmapSize
                + ", originalBitmapCount="
                + originalBitmapCount
                + ", transformedBitmapCount="
                + transformedBitmapCount
                + ", timeStamp="
                + timeStamp
                + '}';
    }
}
