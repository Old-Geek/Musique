package org.oucho.picasso;

import android.graphics.Bitmap;


/** @noinspection unused*/
public interface Cache {

    Cache NONE = new Cache() {
        @Override
        public Bitmap get(String key) {
            return null;
        }

        @Override
        public void set(String key, Bitmap bitmap) {
            // Ignore.
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public int maxSize() {
            return 0;
        }

        @Override
        public void clear() {
        }

        @Override
        public void clearKeyUri(String keyPrefix) {
        }

    };

    Bitmap get(String key);

    void set(String key, Bitmap bitmap);

    int size();

    int maxSize();

    void clear();

    /** Remove items whose key is prefixed with {@code keyPrefix}. */
    void clearKeyUri(String keyPrefix);

}
