package org.oucho.picasso;

import android.graphics.Bitmap;

public interface Transformation {

    Bitmap transform(Bitmap source);

    String key();
}
