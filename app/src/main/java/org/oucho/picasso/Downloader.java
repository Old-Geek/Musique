package org.oucho.picasso;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Response;

/**
 * A mechanism to load images from external resources such as a disk cache and/or the internet.
 */
public interface Downloader {

    @NonNull
    Response load(@NonNull okhttp3.Request request) throws IOException;

}
