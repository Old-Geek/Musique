package org.oucho.musicplayer;


import android.net.Uri;

public interface Keys {

    long WAKELOCK_DURATION = 2 * 60 * 60 * 1000;

    String INTENT_PLAYER_STATE = "musicplayer.STATE";
    String INTENT_LAYOUT_VIEW = "LAYOUTVIEW";

    String PLAYER_NEXT_TRACK = "musicplayer.ACTION_NEXT";
    String PLAYER_TOGGLE_PLAY = "musicplayer.ACTION_TOGGLE";
    String PLAYER_PREVIOUS_TRACK = "musicplayer.ACTION_PREVIOUS";
    String PLAYER_SET_TRACK_POSITION = "musicplayer.ACTION_SET_POSITION";
    String PLAYER_SERVICE_SEEK_TO = "musicplayer.SEEK_TO";
    String PLAYER_SERVICE_SET_VOLUME = "musicplayer.SET_VOLUME";
    String PLAYER_SERVICE_STOP = "musicplayer.PLAYER_STOP";

    String ITEM_ADDED_TO_QUEUE = "musicplayer.ITEM_ADDED";
    String METADATA_CHANGED = "musicplayer.META_CHANGED";
    String PLAY_STATE_CHANGED = "musicplayer.PLAYSTATE_CHANGED";

    String PLAYBAR_CONTROL = "playBar_control";
    String HALT_AND_EXIT = "exit";
    String SET_TITLE = "set_title";
    String BACK_VIEWPAGER = "back_viewPager";
    String SWIPE_ENABLED = "swipe_enabled";
    String TOOLBAR_SHADOW = "toolbar_shadow";

    String ACTION_RADIO_PLAY = "radio_play";
    String ACTION_RADIO_STOP = "radio_stop";
    String ACTION_RADIO_PAUSE = "radio_pause";
    String ACTION_RADIO_RESTART = "radio_restart";

    String RADIO_PLAYER_STATE = "radio.INTENT_STATE";
    String RADIO_ERROR_STATE = "radio.INTENT_ERROR";
    String RADIO_SEARCH_STATE = "radio.INTENT_SEARCH";
    String RADIO_FOCUS_STATE = "radio.INTENT_FOCUS";
    String RADIO_HOME_STATE = "radio.INTENT_HOME";
    String RADIO_LOAD_TUNEIN_STATE = "radio.INTENT_TUNEIN";
    String RADIO_TUNEIN_CLOSE_STATE = "radio.INTENT_TUNEIN_CLOSE";

    String SONG_SORT_CRITERIA = "song_sort_order";
    String ALBUM_SORT_CRITERIA = "album_sort_order";

    Uri ALBUM_ARTWORK_URI = Uri.parse("content://media/external/audio/albumart");

    static String getRadioAppUserAgent() {
        return "Radio/" + BuildConfig.VERSION_NAME + " (Android 15+)";
    }

    // valeurs "Shake"
    float SHAKE_SENSITIVITY = 3.00f; // Diminuez la valeur pour une détection plus sensible.
    int SHAKE_DELAY_MS = 500; // Temps d'attente entre deux shakes
}
