package org.oucho.musicplayer.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.ImageTools;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AudioWidgetProvider extends AppWidgetProvider implements Keys {

    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateWidgetIfNeeded(context, appWidgetManager, appWidgetId);
            Intent serviceIntent = new Intent(context, PlayerService.class);
            context.startService(serviceIntent);
        }
    }

    private void updateWidgetIfNeeded(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        boolean isPlayingNow = PlayerService.isPlaying() || RadioState.isPlaying();
        boolean wasPlaying = RadioState.isPlaying() || RadioState.isPaused();

        if (isPlayingNow != wasPlaying) {
            refreshWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public static void refreshWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        Intent mainActivityIntent = new Intent(context, MainActivity.class);
        PendingIntent openAppIntent = PendingIntent.getActivity(context, 0, mainActivityIntent, PendingIntent.FLAG_IMMUTABLE);

        RemoteViews views;

        if (PlayerService.isPlaying() || PlayerService.isPaused()) {
            views = new RemoteViews(context.getPackageName(), R.layout.music_widget);
            views.setOnClickPendingIntent(R.id.widget_cover, openAppIntent);
            configureMusicWidget(context, views, appWidgetManager, appWidgetId);
        } else if (RadioState.isPlaying() || RadioState.isPaused() || RadioState.isStopped()) {
            views = new RemoteViews(context.getPackageName(), R.layout.radio_widget);
            views.setOnClickPendingIntent(R.id.widget_radio_logo, openAppIntent);
            configureRadioWidget(context, views, appWidgetManager, appWidgetId);
        }
    }

    private static void configureRadioWidget(Context context, RemoteViews views, AppWidgetManager appWidgetManager, int appWidgetId) {
        PendingIntent togglePlayIntent = PendingIntent.getService(context, 0,
                new Intent(context, RadioService.class).setAction(ACTION_RADIO_PLAY), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.widget_radio_play_pause, togglePlayIntent);

        PendingIntent nextIntent = PendingIntent.getService(context, 0,
                new Intent(context, RadioService.class).setAction(ACTION_RADIO_STOP), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.widget_radio_stop, nextIntent);

        if (RadioState.isPlaying()) {
            views.setImageViewResource(R.id.widget_radio_play_pause, R.drawable.pause_circle_40px);
        } else {
            views.setImageViewResource(R.id.widget_radio_play_pause, R.drawable.play_circle_40px);
        }

        loadRadioArt(views, appWidgetManager, appWidgetId);

        String title = RadioService.getName();
        views.setTextViewText(R.id.widget_radio_name, title);

        String state = RadioState.getStateString(context);
        views.setTextViewText(R.id.widget_radio_state, state);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private static void loadRadioArt(RemoteViews views, AppWidgetManager appWidgetManager, int appWidgetId) {
        executorService.execute(() -> {
            Mat mat = new ImageTools().loadMatFromJPEG(RadioService.getUrl());
            if (mat != null) {
                // Convert Mat to Bitmap to set it in RemoteViews
                android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(mat.cols(), mat.rows(), android.graphics.Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(mat, bitmap);
                views.setImageViewBitmap(R.id.widget_radio_logo, bitmap);
                appWidgetManager.updateAppWidget(appWidgetId, views);
                mat.release();
            }
        });
    }

    private static void configureMusicWidget(Context context, RemoteViews views, AppWidgetManager appWidgetManager, int appWidgetId) {
        PendingIntent togglePlayIntent = PendingIntent.getService(context, 0,
                new Intent(context, PlayerService.class).setAction(PlayerService.PLAYER_TOGGLE_PLAY), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.widget_play_pause_button, togglePlayIntent);

        PendingIntent nextIntent = PendingIntent.getService(context, 0,
                new Intent(context, PlayerService.class).setAction(PlayerService.PLAYER_NEXT_TRACK), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.widget_next_button, nextIntent);

        PendingIntent previousIntent = PendingIntent.getService(context, 0,
                new Intent(context, PlayerService.class).setAction(PlayerService.PLAYER_PREVIOUS_TRACK), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.widget_prev_button, previousIntent);

        if (PlayerService.isPlaying()) {
            views.setImageViewResource(R.id.widget_play_pause_button, R.drawable.pause_circle_40px);
        } else {
            views.setImageViewResource(R.id.widget_play_pause_button, R.drawable.play_circle_40px);
        }

        Song song = PlayerService.getCurrentSong();
        if (song == null) return;

        loadAlbumArt(context, song, views, appWidgetManager, appWidgetId);

        String title = song.getSongTitle();
        String artist = song.getArtistName();

        views.setTextViewText(R.id.widget_song_title, title);
        views.setTextViewText(R.id.widget_artist_name, artist);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private static void loadAlbumArt(Context context, Song song, RemoteViews views, AppWidgetManager appWidgetManager, int appWidgetId) {
        executorService.execute(() -> {
            Mat mat = null;
            try {
                Uri uri = ContentUris.withAppendedId(ALBUM_ARTWORK_URI, song.getAlbumIdentifier());
                ContentResolver resolver = context.getContentResolver();
                mat = new ImageTools().decode(Objects.requireNonNull(resolver.openInputStream(uri)));
            } catch (Exception e) {
                // Handle exception
            }
            if (mat != null) {
                // Convert Mat to Bitmap to set it in RemoteViews
                android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(mat.cols(), mat.rows(), android.graphics.Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(mat, bitmap);
                views.setImageViewBitmap(R.id.widget_cover, bitmap);
                appWidgetManager.updateAppWidget(appWidgetId, views);
                mat.release();
            }
        });
    }
}
