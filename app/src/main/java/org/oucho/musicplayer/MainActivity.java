package org.oucho.musicplayer;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.PermissionHelper;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.basefrag.LibraryFragment;
import org.oucho.musicplayer.ui.helpers.OnStartDragListener;
import org.oucho.musicplayer.ui.playbar.PlayBarManager;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.radio.tunein.RadioPlayer;
import org.oucho.musicplayer.ui.song.SongLoader;
import org.oucho.musicplayer.ui.timer.TimerHelper;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements Keys, OnStartDragListener {

    public PlayerService playerService;
    private boolean isPlayerServiceBound = false;

    private static int currentViewID;
    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    private PlayBarManager playBarManager;
    private TimerHelper timerHelper;

    private QueueManager queueManager;

    public static int getCurrentViewID() {
        return currentViewID;
    }

    public static void setCurrentViewID(int id) {
        currentViewID = id;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PermissionHelper.requestPermissions(this);

        // Demander la permission MANAGE_EXTERNAL_STORAGE
        ActivityResultLauncher<Intent> manageAllFilesLauncher = PermissionHelper.registerManageAllFilesLauncher(this, getApplicationContext());
        PermissionHelper.requestManageExternalStoragePermission(this, manageAllFilesLauncher);

        setContentView(R.layout.activity_main);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        queueManager = new QueueManager(this, findViewById(R.id.activ_main));
        playBarManager = new PlayBarManager(this);

        IntentFilter filter = createIntentFilter();
        registerReceiver(serviceListener, filter, Context.RECEIVER_EXPORTED);

        initializeTimer();

        if (savedInstanceState == null) {
            displayLibrary();
        }
    }

    public QueueManager getQueueManager() {
        return queueManager;
    }

    private void initializeTimer() {
        TextView timeAfficheur_a = findViewById(R.id.zZz);

        timerHelper = new TimerHelper(timeAfficheur_a, this, getLayoutInflater());
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        queueManager.onStartDrag(viewHolder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return handleMenuItemSelection(item, id);
    }

    private boolean handleMenuItemSelection(MenuItem item, int id) {
        if (id == android.R.id.home) {
            handleHomeButtonClick();
            return true;
        } else if (id == R.id.action_view_queue) {
            queueManager.refreshQueue();
            queueManager.toggleQueueVisibility();
            return true;
        } else if (id == R.id.set_timer) {
            handleTimerMenuItem();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handleHomeButtonClick() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            displayLibrary();
        }
    }

    private void handleTimerMenuItem() {
        if (!timerHelper.isTimerRunning()) {
            timerHelper.showTimePicker();
        } else {
            timerHelper.showTimerInfo();
        }
    }

    private void displayLibrary() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, LibraryFragment.newInstance())
                .commit();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isPlayerServiceBound) {
            unbindPlayerService();
        }

        deleteCache(this);

        mainHandler.removeCallbacks(playBarManager.mUpdateProgressBar);
    }

    private void unbindPlayerService() {
        playerService = null;
        unbindService(playerServiceConnection);
        isPlayerServiceBound = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        String title = Preferences.getRadioName();
        ((TextView) findViewById(R.id.radio_name)).setText(title);

        String state = RadioState.getStateString(this);
        ((TextView) findViewById(R.id.radio_state)).setText(state);

        if (!isPlayerServiceBound) {
            bindPlayerService();
        } else {
            updateAllComponents();
        }
    }

    private void bindPlayerService() {
        Intent mServiceIntent = new Intent(this, PlayerService.class);
        bindService(mServiceIntent, playerServiceConnection, BIND_AUTO_CREATE);
        startService(mServiceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainHandler.removeCallbacksAndMessages(null);
        playBarManager.clearReferences();

        unregisterReceiver(serviceListener);

        if (isPlayerServiceBound) {
            unbindService(playerServiceConnection);
            isPlayerServiceBound = false;
        }

        if (timerHelper.isTimerRunning()) {
            timerHelper.cancelTimer();
        }

        RadioPlayer.shutdownExecutor();
    }

    private void deleteCache(Context context) {
        try {
            File cacheDir = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
            if (cacheDir.exists()) {
                deleteRecursive(cacheDir);
            }
        } catch (Exception e) {
            Log.e("MainActivity", "deleteCache: ", e);
        }
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : Objects.requireNonNull(fileOrDirectory.listFiles())) {
                deleteRecursive(child);
            }
        }

        //noinspection ResultOfMethodCallIgnored
        fileOrDirectory.delete();
    }

    private IntentFilter createIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(PlayerService.METADATA_CHANGED);
        filter.addAction(PlayerService.PLAY_STATE_CHANGED);
        filter.addAction(PlayerService.ITEM_ADDED_TO_QUEUE);
        filter.addAction(HALT_AND_EXIT);
        filter.addAction(INTENT_LAYOUT_VIEW);
        filter.addAction(RADIO_PLAYER_STATE);
        return filter;
    }

    private void refresh() {
        int fragmentNB = getSupportFragmentManager().getFragments().size();
        Fragment fragment;

        for (int pos = 0; pos < fragmentNB; pos++) {
            fragment = getSupportFragmentManager().getFragments().get(pos);
            if (fragment != null) {
                ((BaseFragment) fragment).load();
            }
        }
    }

    // Gère les clicks pour AlbumContent,
    // SongList et Playlist
    public void onSongSelected(List<Song> songList, int position) {

        if (RadioState.isPlaying() || RadioState.isPaused()) {
            Intent radio = new Intent(getApplicationContext(), RadioService.class);
            radio.setAction(ACTION_RADIO_STOP);
            getApplicationContext().startService(radio);
            mainHandler.postDelayed(() ->
                    ((TextView) findViewById(R.id.radio_state)).setText(R.string.radio_stop), 150
            );
        }

        playerService.setPlayList(songList, position);
        mainHandler.postDelayed(() -> playBarManager.setButtonDrawable(), 150);
    }

    private final BroadcastReceiver serviceListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action == null) return;

            switch (action) {
                case INTENT_LAYOUT_VIEW:
                    handleLayoutViewIntent(intent);
                    break;
                case PlayerService.PLAY_STATE_CHANGED:
                    playBarManager.handlePlayStateChanged();
                    break;
                case PlayerService.METADATA_CHANGED:
                    updateTrackInfo();
                    break;
                case HALT_AND_EXIT:
                    handleExitIntent(intent);
                    break;
                case RADIO_PLAYER_STATE:
                    playBarManager.setButtonDrawable();
                    updateTrackInfo();
                    break;
            }
        }
    };


    private void handleLayoutViewIntent(Intent intent) {
        final float tailleBarre = getResources().getDimension(R.dimen.playbar_height);
        String action = intent.getStringExtra(PLAYBAR_CONTROL);

        switch (Objects.requireNonNull(action)) {
            case "showradio":
                playBarManager.showPlayBarRadio(tailleBarre);
                break;
            case "hideradio":
                playBarManager.hidePlayBarRadio(tailleBarre);
                break;
        }

        refresh();
        updateTrackInfo();
    }

    private void handleExitIntent(Intent intent) {
        if (HALT_AND_EXIT.equals(intent.getAction())) {
            exitApp();
        }
    }

    public void addToQueue(Song song) {
        if (playerService != null) {
            playerService.addToQueue(song);
        }
    }

    private final ServiceConnection playerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayerService.PlaybackBinder binder = (PlayerService.PlaybackBinder) service;
            playerService = binder.getService();
            isPlayerServiceBound = true;
            updateAllComponents();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isPlayerServiceBound = false;
        }
    };

    private void updateAllComponents() {
        if (playerService != null) {
            queueManager.refreshQueue();
            updateTrackInfo();
            playBarManager.setButtonDrawable();

            if (PlayerService.isPlaying()) {
                mainHandler.post(playBarManager.mUpdateProgressBar);
            }
        }
    }

    public void updateTrackInfo() {
        Song song = PlayerService.getCurrentSong();
        if (song == null) {
            song = SongLoader.fetchFirstSong(getContentResolver());
            PlayerService.setCurrentSong(song);
            return;
        }

        updateTrackInfoForSong(song);
    }

    private void updateTrackInfoForSong(Song song) {
        String title;
        String artist;

        if (RadioState.isPlaying() || RadioState.isPaused()) {
            title = RadioService.getName();
            String stat = RadioState.getStateString(getApplicationContext());
            artist = stat;
            ((TextView) findViewById(R.id.radio_state)).setText(stat);
        } else {
            title = song.getSongTitle();
            artist = song.getArtistName();
        }

        updateTitleAndArtist(title, artist, song);

        if (RadioState.isPlaying()) {
            playBarManager.progressBar.setMaxValue(0);
            playBarManager.progressBar.setCurrentProgress(0);
        } else {
            int duration = song.getSongDuration();
            if (duration != -1) {
                playBarManager.progressBar.setMaxValue(duration);
                playBarManager.progressBar.setCurrentProgress(PlayerService.getPlayerPosition());
            }
        }
    }

    private void updateTitleAndArtist(String title, String artist, Song song) {
        if (title != null) {
            ((TextView) findViewById(R.id.song_title)).setText(title);
        }

        String radioTitle = Preferences.getRadioName();
        radioTitle = radioTitle == null ? "" : radioTitle;
        if (!radioTitle.isEmpty()) {
            ((TextView) findViewById(R.id.radio_name)).setText(radioTitle);
        }

        if (artist != null && !RadioState.isPlaying()) {
            String sartist = artist + ", " + song.getAlbumTitle();
            ((TextView) findViewById(R.id.song_artist)).setText(sartist);
        } else {
            ((TextView) findViewById(R.id.song_artist)).setText(artist);
        }
    }

    private void exitApp() {
        if (timerHelper.isTimerRunning()) {
            timerHelper.cancelTimer();
        }

        if (PlayerService.isPlaying()) {
            playerService.toggle();
        }

        SendBroadcast.sending(getApplicationContext(), PLAYER_SERVICE_SET_VOLUME, "volume", 1.0f);
        finish();
    }

    public TimerHelper getTimerHelper() {
        return timerHelper;
    }
}
