package org.oucho.musicplayer.tools;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConverter {
    public static void main(String[] args) {
        String inputDate = "2025-02-15";
        String outputDate = convertDateFormat(inputDate);
        System.out.println(outputDate); // Affiche : 15 février 2025
    }

    public static String convertDateFormat(String inputDate) {
        // Définir le format d'entrée
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);

        // Définir le format de sortie
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);

        try {
            // Convertir la chaîne en objet Date
            Date date = inputFormat.parse(inputDate);

            // Formater la date dans le nouveau format
            assert date != null;
            return outputFormat.format(date);
        } catch (ParseException e) {
            Log.e("DateConverter", "Erreur de conversion de date : " + e.getMessage());
            return null;
        }
    }
}
