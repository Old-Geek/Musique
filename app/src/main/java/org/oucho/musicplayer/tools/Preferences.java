package org.oucho.musicplayer.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MusicPlayerApp;

import java.util.concurrent.Semaphore;

public class Preferences implements Keys {

    static final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MusicPlayerApp.getInstance());
    static final SharedPreferences.Editor editor = prefs.edit();

    static final SharedPreferences prefsRadio = MusicPlayerApp.getInstance().getSharedPreferences("radio_preferences", Context.MODE_PRIVATE);
    static final SharedPreferences.Editor editorRadio = prefsRadio.edit();

    /// Evite plusieurs accès simultané, chacun son tour
    private static final Semaphore semaphore = new Semaphore(1); // Semaphore avec 1 permis

    public static void edit(String key, String value) {
        try{
            semaphore.acquire();
            editor.putString(key, value);
            editor.apply();
        } catch (InterruptedException e) {
            Log.w("Preferences", "edit String ", e);
        } finally {
            semaphore.release();
        }
    }

    public static void edit(String key, int value) {
        try {
            semaphore.acquire();
            editor.putInt(key, value);
            editor.apply();
        } catch (InterruptedException e) {
            Log.w("Preferences", "edit int ", e);
        } finally {
            semaphore.release();
        }
    }

    public static void edit(String key, boolean value) {
        try {
            semaphore.acquire();
            editor.putBoolean(key, value);
            editor.apply();
        } catch (InterruptedException e) {
            Log.w("Preferences", "edit boolean ", e);
        } finally {
            semaphore.release();
        }
    }

    public static String getAlbumSort() {
        return prefs.getString(ALBUM_SORT_CRITERIA, "");
    }
    public static String getSongSort() {
        return prefs.getString(SONG_SORT_CRITERIA, "");
    }
    public static int getQueuePosition(){
        return prefs.getInt("currentPosition", 0);
    }

    public static int getViewPager2(){
        return prefs.getInt("page_position", 1);
    }

    public static boolean getStateSaved(){
        return prefs.getBoolean("stateSaved", false);
    }

    public static void editRadio(String url, String name) {
        editorRadio.putString("url", url);
        editorRadio.putString("name", name);
        editorRadio.apply();
    }

    public static String getRadioUrl() {
        return prefsRadio.getString("url", null);
    }

    public static String getRadioName() {
        return prefsRadio.getString("name", null);
    }
}
