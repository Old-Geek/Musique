package org.oucho.musicplayer.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import org.jaudiotagger.audio.flac.metadatablock.MetadataBlockDataPicture;
import org.jaudiotagger.tag.images.Artwork;
import org.jaudiotagger.tag.images.ArtworkFactory;
import org.jaudiotagger.tag.reference.PictureTypes;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.ui.radio.MD5Helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ImageTools {

    public Mat enhanceAndResizeImage(Mat mat) {
        // Redimensionner l'image pour 2.5 fois sa taille
        double newWidth = mat.cols() * 2.5;
        double newHeight = mat.rows() * 2.5;
        Mat resizedMat = new Mat();
        Imgproc.resize(mat, resizedMat, new Size(newWidth, newHeight), 0, 0, Imgproc.INTER_LANCZOS4);

        // Appliquer un filtre pour améliorer la netteté
        Mat blurred = new Mat();
        Imgproc.GaussianBlur(resizedMat, blurred, new Size(9, 9), 10.0);
        Mat sharpened = new Mat();
        Core.addWeighted(resizedMat, 1.5, blurred, -0.5, 0, sharpened);

        // Libérer les ressources
        resizedMat.release();

        return sharpened;
    }

    public Mat loadMatFromJPEG(String key) {
        try {
            String fileName = MD5Helper.hash(key) + ".jpg";
            File file = new File(MusicPlayerApp.getInstance().getFilesDir(), fileName);

            if (file.exists()) {
                Mat mat = Imgcodecs.imread(file.getAbsolutePath(), Imgcodecs.IMREAD_COLOR);
                if (mat.empty()) {
                    Log.e("ImageTools", "Failed to read image from file: " + file.getAbsolutePath());
                    return null;
                }
                Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGB);
                return mat;
            }

        } catch (Exception e) {
            Log.e("ImageTools", "Erreur lors du chargement JPEG", e);
        }
        return null;
    }

    public Mat decode(InputStream in) throws IOException {
        byte[] bytes = in.readAllBytes();
        Mat mat = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_COLOR);
        if (mat.empty()) {
            Log.e("ImageTools", "Failed to decode image from stream");
            return null;
        }
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGB);
        return mat;
    }

    public static Bitmap resizeImageToBitmap(byte[] imageData, int width, int height) {
        // Convertir les données binaires en Mat
        Mat originalImage = Imgcodecs.imdecode(new MatOfByte(imageData), Imgcodecs.IMREAD_UNCHANGED);

        // Créer une nouvelle Mat pour l'image redimensionnée
        Mat resizedImage = new Mat();

        // Redimensionner l'image
        Imgproc.resize(originalImage, resizedImage, new Size(width, height));

        // Convertir la Mat redimensionnée en tableau de bytes
        MatOfByte byteArray = new MatOfByte();
        Imgcodecs.imencode(".jpg", resizedImage, byteArray);

        // Convertir le tableau de bytes en Bitmap
        byte[] resizedImageData = byteArray.toArray();
        return BitmapFactory.decodeByteArray(resizedImageData, 0, resizedImageData.length);
    }

    public static Artwork createArtworkForJAudioTagger(Context context, Uri imageUri, int size) {
        InputStream inputStream = null;

        try {
            inputStream = context.getContentResolver().openInputStream(imageUri);
            if (inputStream == null) {
                Log.e("Artwork", "Failed to open input stream from URI: " + imageUri);
                return null;
            }

            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            if (bitmap == null) {
                Log.e("Artwork", "Failed to decode bitmap from URI: " + imageUri);
                return null;
            }

            Mat srcMat = new Mat();
            Utils.bitmapToMat(bitmap, srcMat);

            int originalWidth = srcMat.width();
            int originalHeight = srcMat.height();
            double aspectRatio = (double) originalWidth / originalHeight;

            int newWidth, newHeight;
            if (originalWidth > originalHeight) {
                newWidth = size;
                newHeight = (int) (size / aspectRatio);
            } else {
                newHeight = size;
                newWidth = (int) (size * aspectRatio);
            }

            Mat resizedMat = new Mat();
            Imgproc.resize(srcMat, resizedMat, new Size(newWidth, newHeight), 0, 0, Imgproc.INTER_LANCZOS4);

            Bitmap resizedBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(resizedMat, resizedBitmap);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 95, byteArrayOutputStream);
            byte[] coverArtData = byteArrayOutputStream.toByteArray();

            MetadataBlockDataPicture picture = new MetadataBlockDataPicture(
                    coverArtData,
                    PictureTypes.DEFAULT_ID,
                    "image/jpeg",
                    "Cover Art",
                    newWidth,
                    newHeight,
                    24,
                    0
            );

            return ArtworkFactory.createArtworkFromMetadataBlockDataPicture(picture);
        } catch (IOException e) {
            Log.e("Artwork", "IOException while converting image: " + e.getMessage(), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e("Artwork", "Failed to close input stream: " + e.getMessage(), e);
                }
            }
        }
        return null;
    }

}
