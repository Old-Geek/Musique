package org.oucho.musicplayer.tools;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;

import org.oucho.musicplayer.MainActivity;

public class PermissionHelper {

    static final int REQUEST_CODE = 1;

    static final String[] permissions = {
            Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.POST_NOTIFICATIONS,
            Manifest.permission.CHANGE_NETWORK_STATE,
            Manifest.permission.WRITE_SETTINGS
    };

    public static void requestPermissions(Activity activity) {
        ActivityCompat.requestPermissions(activity, permissions, REQUEST_CODE);
    }

    public static void requestManageExternalStoragePermission(Activity activity, ActivityResultLauncher<Intent> launcher) {
        if (!Environment.isExternalStorageManager()) {
            // Demander la permission MANAGE_EXTERNAL_STORAGE
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            launcher.launch(intent);
        }
    }

    public static ActivityResultLauncher<Intent> registerManageAllFilesLauncher(Activity activity, Context appContext) {
        return  ((MainActivity) activity).registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (Environment.isExternalStorageManager()) {
                        Log.d("Permission", "Permission MANAGE_EXTERNAL_STORAGE accordée");
                    } else {
                        Log.d("Permission", "Permission MANAGE_EXTERNAL_STORAGE refusée");
                        Toast.makeText(appContext, "Permission MANAGE_EXTERNAL_STORAGE refusée", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
