package org.oucho.musicplayer.tools;

import android.content.Context;
import android.content.Intent;


public class SendBroadcast {

    private static final Intent intent = new Intent();

    public static void sending(Context context, String action) {
        intent.setAction(action);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, boolean extraValue) {
        intent.setAction(action);
        intent.putExtra("boolean", extraValue);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, String extra, int extraValue) {
        intent.setAction(action);
        intent.putExtra(extra, extraValue);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, String extra, float extraValue) {
        intent.setAction(action);
        intent.putExtra(extra, extraValue);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, String extra, String extraValue) {
        intent.setAction(action);
        intent.putExtra(extra, extraValue);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, String extra, boolean extraValue) {
        intent.setAction(action);
        intent.putExtra(extra, extraValue);
        context.sendBroadcast(intent);
    }

    public static void sending(Context context, String action, String state, String url, String name) {
        intent.setAction(action);
        intent.putExtra("state", state);
        intent.putExtra("url", url);
        intent.putExtra("name", name);
        context.sendBroadcast(intent);
    }

}
