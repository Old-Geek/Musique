package org.oucho.musicplayer.tools;

import java.nio.charset.StandardCharsets;

public class RustineUTF8 {

    // Méthode utilitaire pour corriger l'encodage de manière sélective
    public static String fixEncodingIfNeeded(String input) {
        if (input == null) {
            return null;
        }
        // Vérifiez si la chaîne contient des caractères mal encodés
        if (input.contains("Ã©") || input.contains("Ã¨") || input.contains("Ãª") ||
                input.contains("Ã«") || input.contains("Ã ") || input.contains("Ã¢") ||
                input.contains("Ã§") || input.contains("Ã®") || input.contains("Ã¯") ||
                input.contains("Ã´") || input.contains("Ã¹") || input.contains("Ã»") ||
                input.contains("Ã¼") || input.contains("Ã¿") || input.contains("Å“") ||
                input.contains("Å’")) {
            byte[] bytes = input.getBytes(StandardCharsets.ISO_8859_1);
            return new String(bytes, StandardCharsets.UTF_8);
        }
        return input;
    }
}
