package org.oucho.musicplayer.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.tools.SendBroadcast;


public class RadioState implements Keys {

   public static final String STATE_STOP         = "Stop";
   private static final String STATE_ERROR       = "Error";
   public static final String STATE_PAUSE        = "Pause";
   public static final String STATE_PLAY         = "Play";
   public static final String STATE_BUFFER       = "Loading...";
   private static final String STATE_COMPLETED   = "Completed";
   public static final String STATE_DUCK         = "\\_o< coin";
   public static final String STATE_DISCONNECTED = "Disconnected";

   private static String current_state = STATE_STOP;
   private static boolean isNetworkUrl = false;

    public static void setState(Context context, String s, boolean isNetworkUrl) {

      if ( s == null )
         return;

      current_state = s;
      RadioState.isNetworkUrl = isNetworkUrl;

      SendBroadcast.sending(context,
              RADIO_PLAYER_STATE,
              current_state,
              RadioService.getUrl(),
              RadioService.getName());
   }

   public static void broadcastState(Context context) {
        setState(context, current_state, isNetworkUrl);
   }

   public static boolean is(String state) {
       return current_state.equals(state);
   }

   public static boolean isPlaying() {
       return is(STATE_PLAY) || is(STATE_BUFFER) || is(STATE_DUCK);
   }

   public static boolean isStopped() {
       return RadioState.is(STATE_STOP) || RadioState.is(STATE_ERROR) || RadioState.is(STATE_COMPLETED);
   }

   public static boolean isPaused() {
        return RadioState.is(STATE_PAUSE);
    }

   public static boolean isWantPlaying() {
       return isPlaying() || is(STATE_ERROR);
   }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null)
                return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    public static String getStateString(Context context) {
        return switch (current_state) {
            case "Play" -> context.getResources().getString(R.string.radio_play);
            case "Loading..." -> context.getResources().getString(R.string.loading);
            case "Disconnected" -> context.getResources().getString(R.string.radio_disconnected);
            case "Completed" -> context.getResources().getString(R.string.radio_completed);
            case "Pause"  -> context.getResources().getString(R.string.radio_pause);
            case "Stop" -> context.getResources().getString(R.string.radio_stop);
            default -> current_state;
        };
    }
}


