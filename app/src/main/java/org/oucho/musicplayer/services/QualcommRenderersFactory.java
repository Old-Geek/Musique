package org.oucho.musicplayer.services;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.OptIn;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.DefaultRenderersFactory;
import androidx.media3.exoplayer.mediacodec.MediaCodecInfo;
import androidx.media3.exoplayer.mediacodec.MediaCodecSelector;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.media3.exoplayer.mediacodec.MediaCodecUtil.DecoderQueryException;

import java.util.ArrayList;
import java.util.List;

@OptIn(markerClass = UnstableApi.class)
public class QualcommRenderersFactory extends DefaultRenderersFactory {
    public QualcommRenderersFactory(Context context) {
        super(context);
        setMediaCodecSelector(new CustomMediaCodecSelector());
    }

    private static class CustomMediaCodecSelector implements MediaCodecSelector {
        @NonNull
        @Override
        public List<MediaCodecInfo> getDecoderInfos(@NonNull String mimeType, boolean requiresSecureDecoder, boolean tunneling)
                throws DecoderQueryException {
            List<MediaCodecInfo> codecInfos = MediaCodecUtil.getDecoderInfos(mimeType, requiresSecureDecoder, tunneling);
            List<MediaCodecInfo> filteredInfos = new ArrayList<>();
            for (MediaCodecInfo info : codecInfos) {
                if ((mimeType.equals("audio/flac") && info.name.equals("c2.qti.flac.sw.decoder")) ||
                        (mimeType.equals("audio/mp4a-latm") && info.name.equals("c2.qti.aac.sw.decoder"))) {
                    filteredInfos.add(info);
                }
            }
            return filteredInfos.isEmpty() ? codecInfos : filteredInfos;
        }
    }
}
