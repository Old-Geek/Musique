package org.oucho.musicplayer.services;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.OptIn;
import androidx.media3.common.AudioAttributes;
import androidx.media3.common.C;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.TrackSelectionParameters;
import androidx.media3.common.TrackSelectionParameters.AudioOffloadPreferences;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.ExoPlayer;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.db.QueueDbHelper;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.widget.AudioWidgetProvider;

import java.util.ArrayList;
import java.util.List;

public class PlayerService extends Service implements Keys {

    public static final int PLAYBACK_STATE_PLAYING = 0;
    public static final int PLAYBACK_STATE_PAUSED = 1;
    public static final int PLAYBACK_STATE_STOPPED = 2;
    private final String QUEUE_CHANGED = "queue_changed";
    private static final int IDLE_DELAY = 60000;

    private static ExoPlayer exoPlayer;
    private static int statePlayback = PLAYBACK_STATE_STOPPED;

    private boolean firstPlay = true;
    private boolean shouldStartPlaying = false;
    private int startId;
    private int currentPosition;
    private Boolean start = false;
    private static Song currentSong;
    private Context context;
    private PowerManager.WakeLock wakeLock;

    private static final List<Song> queuePlayList = new ArrayList<>();
    private final Handler delayedStopHandler = new Handler(Looper.getMainLooper());
    private final PlaybackBinder mBinder = new PlaybackBinder(this);

    private final Handler stopAfterPauseHandler = new Handler(Looper.getMainLooper());
    private Runnable stopAfterPauseRunnable;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        initializePowerManager();
        initializeExoPlayer();
        registerReceiver();
        restoreState();
    }

    private void initializePowerManager() {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "org.oucho.musicplayer:PlayerServiceWakeLock");
    }

    private void registerReceiver() {
        IntentFilter filter = createIntentFilter();
        registerReceiver(playerServiceListener, filter, Context.RECEIVER_EXPORTED);
    }

    private IntentFilter createIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        filter.addAction(PLAYER_SERVICE_SEEK_TO);
        filter.addAction(PLAYER_SERVICE_SET_VOLUME);
        filter.addAction(PLAYER_SERVICE_STOP);
        filter.addAction(PLAYER_TOGGLE_PLAY);
        filter.addAction(PLAYER_NEXT_TRACK);
        filter.addAction(PLAYER_PREVIOUS_TRACK);
        return filter;
    }

    @OptIn(markerClass = UnstableApi.class)
    private void initializeExoPlayer() {
        AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.AUDIO_CONTENT_TYPE_MUSIC)
                .build();

        /* Active la lecture audio déchargée permet de décharger le traitement
         du flux audio du processeur principal (CPU) vers un processeur dédié */
        AudioOffloadPreferences audioOffloadPreferences = new AudioOffloadPreferences.Builder()
                .setAudioOffloadMode(AudioOffloadPreferences.AUDIO_OFFLOAD_MODE_ENABLED)
                .setIsGaplessSupportRequired(true)
                .build();

        TrackSelectionParameters trackSelectionParameters = new TrackSelectionParameters.Builder(context)
                .setAudioOffloadPreferences(audioOffloadPreferences)
                .build();

        // utiliser les codec Qualcomm si disponible
        QualcommRenderersFactory qualcommRenderersFactory = new QualcommRenderersFactory(context);
        exoPlayer = new ExoPlayer.Builder(context, qualcommRenderersFactory).build();
        exoPlayer.setAudioAttributes(playbackAttributes, true);
        exoPlayer.setTrackSelectionParameters(trackSelectionParameters);

        exoPlayer.addListener(new Player.Listener() {
            @Override
            public void onPlaybackStateChanged(int state) {
                switch (state) {
                    case Player.STATE_ENDED:
                        handlePlaybackEnded();
                        break;
                    case Player.STATE_READY:
                        handlePlaybackReady();
                        break;
                    case Player.STATE_IDLE:
                        handlePlaybackStateChanged(state);
                        break;
                    case Player.STATE_BUFFERING:
                        break;
                }
            }

            @Override
            public void onPlayerError(@NonNull PlaybackException error) {
                handlePlayerError();
            }
        });
    }

    private void handlePlayerError() {
        Log.e("ExoPlayerError", "Erreur de lecture");
        Toast.makeText(getApplicationContext(), "Une erreur s'est produite lors de la lecture du morceau.", Toast.LENGTH_LONG).show();
    }

    private void handlePlaybackStateChanged(int playbackState) {
        if (playbackState == Player.STATE_READY) {
            handlePlaybackReady();
        } else if (playbackState == Player.STATE_ENDED) {
            handlePlaybackEnded();
        }
        notifyChange(PLAY_STATE_CHANGED);
    }

    private void handlePlaybackReady() {
        if (!start) {
            start = true;
        } else {
            notifyChange(METADATA_CHANGED);
        }

        if (!firstPlay && shouldStartPlaying) {
            play();
        }

        firstPlay = false;
    }

    private void handlePlaybackEnded() {
        if (currentPosition + 1 == queuePlayList.size()) {
            statePlayback = PLAYBACK_STATE_PAUSED;
            stop();
            notifyChange(PLAY_STATE_CHANGED);
        } else if (!queuePlayList.isEmpty()) {
            delayedStopHandler.postDelayed(() -> {
                int position = getNextPosition();
                if (position >= 0) {
                    currentPosition = position;
                    setCurrentSong(queuePlayList.get(position));
                    notifyChange(METADATA_CHANGED);
                    open();
                }
            }, 500);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        this.startId = startId;

        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                handleIntentAction(action, intent);
            }
        }
        return START_STICKY;
    }

    private void handleIntentAction(String action, Intent intent) {
        if (queuePlayList.isEmpty()) {
            startMainActivity();
        } else {
            switch (action) {
                case PLAYER_TOGGLE_PLAY:
                    toggle();
                    break;
                case PLAYER_NEXT_TRACK:
                    playNext();
                    break;
                case PLAYER_PREVIOUS_TRACK:
                    playPrev();
                    break;
                case PLAYER_SET_TRACK_POSITION:
                    setPosition(intent.getIntExtra("position", 0), intent.getBooleanExtra("play", false));
                    break;
            }
        }
    }

    private void startMainActivity() {
        Intent dialogIntent = new Intent(this, MainActivity.class);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

    public void setPlayList(List<Song> songList, int position) {
        setPlayListInternal(songList);
        setPosition(position, true);
        notifyChange(QUEUE_CHANGED);
    }

    private void setPlayListInternal(List<Song> songList) {
        if (songList == null || songList.isEmpty()) return;

        queuePlayList.clear();
        queuePlayList.addAll(songList);
    }

    public void addToQueue(Song song) {
        queuePlayList.add(song);
        notifyChange(ITEM_ADDED_TO_QUEUE);
    }

    public void notifyChange(String what) {
        boolean saveQueue = QUEUE_CHANGED.equals(what) || ITEM_ADDED_TO_QUEUE.equals(what);

        if (!queuePlayList.isEmpty()) {
            Preferences.edit("stateSaved", true);
            if (saveQueue) {
                saveQueueToDatabase();
            }
            Preferences.edit("currentPosition", currentPosition);
        }

        if (PLAY_STATE_CHANGED.equals(what) || METADATA_CHANGED.equals(what)) {
            updateWidget();
        }

        SendBroadcast.sending(this, what);
    }

    private void saveQueueToDatabase() {
        QueueDbHelper dbHelper = new QueueDbHelper(this);
        dbHelper.removeAll();
        dbHelper.add();
        dbHelper.close();
    }

    private void updateWidget() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        ComponentName widgetComponent = new ComponentName(this, AudioWidgetProvider.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(widgetComponent);
        for (int appWidgetId : appWidgetIds) {
            AudioWidgetProvider.refreshWidget(this, appWidgetManager, appWidgetId);
        }
    }

    private void play() {
        if (!queuePlayList.isEmpty()) {
            acquireWakeLock();
            exoPlayer.play();
            statePlayback = PLAYBACK_STATE_PLAYING;
            notifyChange(PLAY_STATE_CHANGED);
            updateWidget();
        }
    }

    private void acquireWakeLock() {
        if (!wakeLock.isHeld()) {
            wakeLock.acquire(WAKELOCK_DURATION);
        }
    }

    private void pause() {
        exoPlayer.pause();
        statePlayback = PLAYBACK_STATE_PAUSED;
        notifyChange(PLAY_STATE_CHANGED);

        if (wakeLock.isHeld()) {
            wakeLock.release();
        }

        // Start the 1-minute countdown to stop the player
        stopAfterPauseHandler.removeCallbacksAndMessages(null);
        stopAfterPauseRunnable = this::stop;
        stopAfterPauseHandler.postDelayed(stopAfterPauseRunnable, 60000); // 1 minute
    }

    public void toggle() {
        if (exoPlayer == null) {
            initializeExoPlayer();

            if (currentPosition >= 0 && currentPosition < queuePlayList.size()) {
                setCurrentSong(queuePlayList.get(currentPosition));
                open();
            }
        }

        if (exoPlayer.isPlaying()) {
            pause();
        } else {
            // Cancel the stop countdown if the player is resumed
            stopAfterPauseHandler.removeCallbacks(stopAfterPauseRunnable);
            shouldStartPlaying = true;
            play();
        }
    }

    public void playNext() {
        int position = getNextPosition();
        if (position >= 0 && position < queuePlayList.size()) {
            currentPosition = position;
            setCurrentSong(queuePlayList.get(position));
            open();
            play();
            SendBroadcast.sending(context, INTENT_PLAYER_STATE, "state", "next");
        }
    }

    public void playPrev() {
        int position = getPreviousPosition();
        if (position >= 0 && position < queuePlayList.size()) {
            if (exoPlayer.isPlaying()) {
                exoPlayer.stop();
            }

            currentPosition = position;
            setCurrentSong(queuePlayList.get(position));
            open();
        }
    }

    private int getPreviousPosition() {
        updateCurrentPosition();
        int position = currentPosition;
        if (isPlaying() && getPlayerPosition() >= 1500) {
            return position;
        }

        if (position - 1 < 0) {
            return -1;
        }

        return position - 1;
    }

    private int getNextPosition() {
        updateCurrentPosition();
        int position = currentPosition;
        int nextPosition = position + 1;
        if (nextPosition >= queuePlayList.size()) {
            return 0;
        }

        return nextPosition;
    }

    public void setPosition(int position, boolean play) {
        if (position >= queuePlayList.size()) return;

        if (exoPlayer == null) {
            initializeExoPlayer();
        }

        currentPosition = position;
        try {
            Song song = queuePlayList.get(position);
            setCurrentSong(song);
            open();
            if (play) {
                play();
            }
        } catch (Exception ignore) {
        }
    }

    private void updateCurrentPosition() {
        int pos = queuePlayList.indexOf(currentSong);
        if (pos != -1) {
            currentPosition = pos;
        }
    }

    private void open() {
        MediaItem mediaItem = new MediaItem.Builder()
                .setUri(Uri.parse(currentSong.getFilePath()))
                .build();
        exoPlayer.setMediaItem(mediaItem);
        exoPlayer.prepare();

        if (shouldStartPlaying) {
            exoPlayer.play();
        }
    }

    public void stop() {
        // Cancel the stop countdown if the player is manually stopped
        stopAfterPauseHandler.removeCallbacks(stopAfterPauseRunnable);

        if (exoPlayer != null) {
            exoPlayer.stop(); // Arrête la lecture
            releaseMediaPlayer();
        }

        statePlayback = PLAYBACK_STATE_STOPPED;
        shouldStartPlaying = false;
        releaseWakeLock();
    }

    public static void setCurrentSong(Song song) {
        currentSong = song;
    }

    public static Song getCurrentSong() {
        return currentSong;
    }

    public static boolean isPlaying() {
        return statePlayback == PLAYBACK_STATE_PLAYING;
    }

    public static boolean isPaused() {
        return statePlayback == PLAYBACK_STATE_PAUSED;
    }

    public static int getPlayerPosition() {
        if (exoPlayer == null) {
            return 0;
        }
        return (int) exoPlayer.getCurrentPosition();
    }

    public static int getPositionWithinQueuePlayList() {
        return queuePlayList.indexOf(currentSong);
    }

    public static List<Song> getQueuePlayList() {
        return queuePlayList;
    }

    private void restoreState() {
        if (Preferences.getStateSaved()) {
            QueueDbHelper dbHelper = new QueueDbHelper(this);
            List<Song> playList = dbHelper.readAll();
            dbHelper.close();
            setPlayListInternal(playList);
            setPosition(Preferences.getQueuePosition(), false);
            updateWidget();
        }
    }

    @Override
    public void onDestroy() {
        // Désenregistrez les écouteurs ou annulez les références ici
        unregisterReceiver(playerServiceListener);
        releaseMediaPlayer();
        releaseWakeLock();
        delayedStopHandler.removeCallbacksAndMessages(null);
        stopAfterPauseHandler.removeCallbacksAndMessages(null);
        mBinder.setService(null);
        super.onDestroy();
    }

    private void releaseMediaPlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private void releaseWakeLock() {
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    private final BroadcastReceiver playerServiceListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) return;

            switch (action) {
                case PLAYER_SERVICE_SEEK_TO:
                    exoPlayer.seekTo(intent.getIntExtra("msec", 0));
                    break;
                case PLAYER_SERVICE_SET_VOLUME:
                    exoPlayer.setVolume((int) intent.getFloatExtra("volume", 1.0f));
                    break;
                case PLAYER_SERVICE_STOP:
                    stop();
                    break;
                case PLAYER_TOGGLE_PLAY:
                    toggle();
                    break;
                case PLAYER_NEXT_TRACK:
                    playNext();
                    break;
                case PLAYER_PREVIOUS_TRACK:
                    playPrev();
                    break;
                case Intent.ACTION_HEADSET_PLUG:
                    handleHeadsetPlug(intent);
                    break;
            }
        }
    };

    private void handleHeadsetPlug(Intent intent) {
        boolean plugged = intent.getIntExtra("state", 0) == 1;
        if (isPlaying() && !plugged) {
            pause();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (isPlaying()) return true;

        if (!queuePlayList.isEmpty()) {
            Message msg = delayedStopHandler.obtainMessage();
            delayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
            return true;
        }

        stopSelf(startId);
        return true;
    }

    public static class PlaybackBinder extends Binder {
        private PlayerService service;

        public PlaybackBinder(PlayerService service) {
            this.service = service;
        }

        public PlayerService getService() {
            return service;
        }

        public void setService(PlayerService service) {
            this.service = service;
        }
    }
}
