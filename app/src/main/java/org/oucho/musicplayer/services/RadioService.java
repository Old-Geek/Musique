package org.oucho.musicplayer.services;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.annotation.OptIn;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.TrackSelectionParameters;
import androidx.media3.common.TrackSelectionParameters.AudioOffloadPreferences;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.ExoPlayer;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.radio.RadioPlaylist;
import org.oucho.musicplayer.ui.radio.network.Connectivity;
import org.oucho.musicplayer.widget.AudioWidgetProvider;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RadioService extends Service implements Keys, Player.Listener, AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = "RadioService";
    private Context context;
    private static String url;
    private static String name;
    private int failureTtl = 5;
    private float currentVol = 1.0f;
    private Connectivity connectivity;
    private ExoPlayer exoPlayer;
    private AudioManager audioManager;
    private AudioFocusRequest focusRequest;
    private PowerManager.WakeLock wakeLock;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Future<?> playlistFuture;
    private Runnable pauseRunnable;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initializePowerManager();
        initializeAudioManager();
        initializeConnectivity();
        createExoPlayer();
        registerReceiver();
        setUrl(Preferences.getRadioUrl());
        setName(Preferences.getRadioName());
    }

    private void initializePowerManager() {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "org.oucho.musicplayer:RadioServiceWakeLock");
    }

    private void initializeAudioManager() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(playbackAttributes)
                .setOnAudioFocusChangeListener(this)
                .build();
    }

    private void initializeConnectivity() {
        connectivity = new Connectivity(context, this);
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(playingControlReceiver, filter, Context.RECEIVER_EXPORTED);
    }

    private final BroadcastReceiver playingControlReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleIntentAction(intent.getAction(), intent);
        }
    };

    private void handleIntentAction(String action, Intent intent) {
        if (Intent.ACTION_HEADSET_PLUG.equals(action) && RadioState.isPlaying()) {
            handleHeadsetPlugged(intent.getIntExtra("state", 0) == 1);
        }
        updateWidget();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }
        handleStartCommandAction(intent);
        return START_STICKY;
    }

    private void handleStartCommandAction(Intent intent) {
        String action = intent.getAction();
        float voldown = intent.getFloatExtra("voldown", 0.0f);
        if (ACTION_RADIO_PLAY.equals(action)) {
            handleRadioPlayAction(intent);
        } else if (ACTION_RADIO_PAUSE.equals(action)) {
            pause();
        } else if (ACTION_RADIO_RESTART.equals(action)) {
            restart();
        } else if (ACTION_RADIO_STOP.equals(action)) {
            stopPlayback();
        } else if (voldown != 0.0f && voldown != currentVol) {
            setVolume(voldown);
            currentVol = voldown;
        }
        updateWidget();
    }

    private void handleRadioPlayAction(Intent intent) {
        if (RadioState.isPlaying()) {
            pause();
        } else if (RadioState.isPaused()) {
            restart();
        } else if (RadioState.isStopped()) {
            if (intent.getStringExtra("name") == null || intent.getStringExtra("url") == null) {
                if (Preferences.getRadioUrl() == null) return;

                Intent defaultIntent = new Intent(context, RadioService.class);
                defaultIntent.putExtra("url", Preferences.getRadioUrl());
                defaultIntent.putExtra("name", Preferences.getRadioName());
                intentPlay(defaultIntent);
            } else {
                intentPlay(intent);
            }

            startPlayback(getUrl());
        }
    }

    private void handleHeadsetPlugged(boolean plugged) {
        if (!plugged) {
            pause();
        }
    }

    private void updateWidget() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        ComponentName widgetComponent = new ComponentName(this, AudioWidgetProvider.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(widgetComponent);
        for (int appWidgetId : appWidgetIds) {
            AudioWidgetProvider.refreshWidget(this, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopPlayback();
        releaseExoPlayer();
        if (connectivity != null) {
            connectivity.destroy();
            connectivity = null;
        }
        executorService.shutdown();
        unregisterReceiver(playingControlReceiver);
    }

    private void startPlayback(String url) {
        stopPlayback(false);
        if (!URLUtil.isValidUrl(url)) {
            stopPlayback();
            return;
        }
        if (isNetworkUrl(url) && !Connectivity.isConnected(context)) {
            connectivity.droppedConnection();
            return;
        }
        int focus = audioManager.requestAudioFocus(focusRequest);
        if (focus != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            stopPlayback();
            return;
        }
        if (isNetworkUrl(url)) {
            connectivity.lockNetwork();
        }
        if (pauseRunnable != null) {
            handler.removeCallbacks(pauseRunnable);
        }

        if (exoPlayer == null)
            createExoPlayer();
        new RadioPlaylist(this, url).start();
        done(RadioState.STATE_BUFFER);
    }

    public void playLaunch(String surl) {
        setUrl(surl);
        if (!URLUtil.isValidUrl(surl)) {
            releaseWakeLock();
            stopPlayback();
            return;
        }
        try {
            exoPlayer.setVolume(1.0f);
            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.pause();
                exoPlayer.stop();
                audioManager.abandonAudioFocusRequest(focusRequest);
                connectivity.unlockNetwork();
                releaseWakeLock();
            }
            acquireWakeLock();
            if (surl != null) {
                connectivity.lockNetwork();
                initializeExoPlayer();
                exoPlayer.play();
                SendBroadcast.sending(context, PLAYER_SERVICE_STOP);
            }
        } catch (Exception e) {
            stopPlayback();
        }
        done(RadioState.STATE_BUFFER);
    }

    private void releaseWakeLock() {
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    private void acquireWakeLock() {
        if (!wakeLock.isHeld()) {
            wakeLock.acquire(WAKELOCK_DURATION);
        }
    }

    private void stopPlayback() {
        stopPlayback(true);
    }

    private void stopPlayback(boolean updateState) {
        audioManager.abandonAudioFocusRequest(focusRequest);
        connectivity.unlockNetwork();
        releaseWakeLock();

        if (exoPlayer != null) {
            exoPlayer.stop(); // Stop the player only if it's not null
            releaseExoPlayer();
        }

        if (playlistFuture != null) {
            playlistFuture.cancel(true);
            playlistFuture = null;
        }

        if (updateState) {
            done(RadioState.STATE_STOP);
            updateWidget();
        }
    }

    private void pause() {
        if (pauseRunnable != null) {
            handler.removeCallbacks(pauseRunnable);
        }

        pauseRunnable = this::stopPlayback;
        handler.postDelayed(pauseRunnable, 60000);
        exoPlayer.pause();
        done(RadioState.STATE_PAUSE);
    }

    private void restart() {
        if (exoPlayer == null) {
            createExoPlayer(); // Initialize ExoPlayer if it's null
        }
        if (RadioState.isStopped()) {
            startPlayback(getUrl());
        }
        if (exoPlayer != null) {
            exoPlayer.setVolume(1.0f);
        }
        if (RadioState.is(RadioState.STATE_PLAY) || RadioState.is(RadioState.STATE_BUFFER)) {
            return;
        }
        if (RadioState.is(RadioState.STATE_DUCK)) {
            done(RadioState.STATE_PLAY);
        }
        int focus = audioManager.requestAudioFocus(focusRequest);
        if (focus != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            return;
        }
        if (playlistFuture != null) {
            playlistFuture.cancel(true);
        }
        acquireWakeLock();
        if (pauseRunnable != null) {
            handler.removeCallbacks(pauseRunnable);
        }
        if (exoPlayer != null) {
            exoPlayer.play();
        }
        SendBroadcast.sending(context, PLAYER_SERVICE_STOP);
        done(RadioState.STATE_PLAY);
    }

    private void intentPlay(Intent intent) {
        setUrl(intent.getStringExtra("url"));
        setName(intent.getStringExtra("name"));
        Preferences.editRadio(getUrl(), getName());
        failureTtl = 5;
    }

    public boolean isNetworkUrl() {
        return isNetworkUrl(url);
    }

    private boolean isNetworkUrl(String checkUrl) {
        return (checkUrl != null && URLUtil.isNetworkUrl(checkUrl));
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (exoPlayer != null) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_GAIN:
                    restart();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                case AudioManager.AUDIOFOCUS_LOSS:
                    pause();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    duck();
                    break;
            }
        }
    }

    private void duck() {
        if (RadioState.is(RadioState.STATE_DUCK) || !RadioState.isPlaying()) {
            return;
        }
        exoPlayer.setVolume(0.1f);
        done(RadioState.STATE_DUCK);
    }

    private void setVolume(float vol) {
        if (exoPlayer != null) {
            exoPlayer.setVolume(vol);
        }
    }

    @Override
    public void onPlaybackStateChanged(int playbackState) {
        switch (playbackState) {
            case Player.STATE_BUFFERING:
                RadioState.setState(context, RadioState.STATE_BUFFER, isNetworkUrl());
                updateWidget();
                break;
            case Player.STATE_READY:
                failureTtl = 5;
                RadioState.setState(context, RadioState.STATE_PLAY, isNetworkUrl());
                updateWidget();
                break;
            case Player.STATE_ENDED:
            case Player.STATE_IDLE:
                updateWidget();
                // Handle end of playback or idle state if necessary
                break;
        }
    }

    @Override
    public void onPlayerError(@NonNull PlaybackException error) {
        Log.e(TAG, "An error occurred: " + error.getMessage());
        tryRecover();
    }

    private void tryRecover() {
        stopPlaybackSoon();
        if (isNetworkUrl() && failureTtl > 0) {
            failureTtl--;
            if (Connectivity.isConnected(context)) {
                startPlayback(getUrl());
            } else {
                connectivity.droppedConnection();
            }
        }
    }

    private void stopPlaybackSoon() {
        releaseWakeLock();
        handler.postDelayed(this::stopPlayback, 150);
    }

    @Override
    public void onIsLoadingChanged(boolean isLoading) {
        Log.i(TAG, "Media source loading state changed: " + isLoading);
    }

    @OptIn(markerClass = UnstableApi.class)
    private void createExoPlayer() {
        if (exoPlayer != null) {
            releaseExoPlayer();
        }

        /* Active la lecture audio déchargée permet de décharger le traitement
         du flux audio du processeur principal (CPU) vers un processeur dédié */
        AudioOffloadPreferences audioOffloadPreferences = new AudioOffloadPreferences.Builder()
                .setAudioOffloadMode(AudioOffloadPreferences.AUDIO_OFFLOAD_MODE_ENABLED)
                .setIsGaplessSupportRequired(true)
                .build();

        TrackSelectionParameters trackSelectionParameters = new TrackSelectionParameters.Builder(context)
                .setAudioOffloadPreferences(audioOffloadPreferences)
                .build();

        // utiliser les codec Qualcomm si disponible
        QualcommRenderersFactory qualcommRenderersFactory = new QualcommRenderersFactory(context);
        exoPlayer = new ExoPlayer.Builder(context, qualcommRenderersFactory).build();
        exoPlayer.setTrackSelectionParameters(trackSelectionParameters);
    }

    private void prepareExoPlayer(String uriString) {
        MediaItem item = MediaItem.fromUri(Uri.parse(uriString));
        exoPlayer.setMediaItem(item);
        exoPlayer.prepare();
    }

    private void releaseExoPlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private void initializeExoPlayer() {
        executorService.execute(() -> handler.post(() -> {
            prepareExoPlayer(getUrl());
            exoPlayer.addListener(RadioService.this);
        }));
    }

    private void done(String state) {
        if (state != null) {
            RadioState.setState(context, state, isNetworkUrl());
            SendBroadcast.sending(context, RADIO_PLAYER_STATE);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static String getName() {
        return name;
    }

    private static void setName(String value) {
        name = value;
    }

    public static String getUrl() {
        return url;
    }

    private static void setUrl(String value) {
        url = value;
    }
}
