package org.oucho.musicplayer;

import android.app.Application;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.oucho.musicplayer.ui.tags.PriorityThreadFactory;
import org.oucho.picasso.LruCache;
import org.oucho.picasso.Picasso;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class MusicPlayerApp extends Application {

    private static MusicPlayerApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        setInstance(this);

        if (!OpenCVLoader.initLocal())
            Log.d("OpenCV", "OpenCV not loaded");

        int corePoolSize = Runtime.getRuntime().availableProcessors() - 1;
        int maximumPoolSize = corePoolSize * 2;
        ThreadFactory threadFactory = new PriorityThreadFactory(Thread.MAX_PRIORITY);
        // Laisse toujours 2 CPUs de libre pour le reste du programme
        // Le nombre de threads est égal au double de CPU disponibles
        Picasso picasso = new Picasso.Builder(this)
                .memoryCache(new LruCache(1073741824)) // 1Go
                .executor(new ThreadPoolExecutor(corePoolSize, maximumPoolSize * 2, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(),threadFactory))
                .build();

        Picasso.setSingletonInstance(picasso);
    }

    public static synchronized MusicPlayerApp getInstance() {
        return sInstance;
    }

    private static void setInstance(MusicPlayerApp value) {
        sInstance = value;
    }

 }
