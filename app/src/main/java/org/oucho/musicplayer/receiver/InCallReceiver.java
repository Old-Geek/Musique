package org.oucho.musicplayer.receiver;

import android.content.Intent;
import android.telecom.Call;
import android.telecom.InCallService;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.SendBroadcast;

public class InCallReceiver extends InCallService implements Keys {

    boolean playerstate = false;
    boolean radiostate = false;

    @Override
    public void onCallAdded(Call call) {
        super.onCallAdded(call);
        if (call.getDetails().getState() == Call.STATE_RINGING)
            pauseMusic();
    }

    @Override
    public void onCallRemoved(Call call) {
        super.onCallRemoved(call);
        resumeMusic();
    }

    private void pauseMusic() {

        if (PlayerService.isPlaying()) {
            playerstate = true;
            SendBroadcast.sending(MusicPlayerApp.getInstance(), PLAYER_TOGGLE_PLAY);
        } else if (RadioState.isPlaying()) {
            radiostate = true;
            Intent radioService = new Intent(MusicPlayerApp.getInstance().getApplicationContext(), RadioService.class);
            radioService.setAction(ACTION_RADIO_PAUSE);
            MusicPlayerApp.getInstance().getApplicationContext().startService(radioService);
        }
    }

    private void resumeMusic() {
        if (playerstate) {
            playerstate = false;
            SendBroadcast.sending(MusicPlayerApp.getInstance(), PLAYER_TOGGLE_PLAY);
        } else if (radiostate) {
            radiostate = false;
            Intent radioService = new Intent(MusicPlayerApp.getInstance().getApplicationContext(), RadioService.class);
            radioService.setAction(ACTION_RADIO_PLAY);
            MusicPlayerApp.getInstance().getApplicationContext().startService(radioService);
        }
    }
}
