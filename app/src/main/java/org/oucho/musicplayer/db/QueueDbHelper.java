package org.oucho.musicplayer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;

import java.util.ArrayList;
import java.util.List;


public class QueueDbHelper extends SQLiteOpenHelper implements Keys {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Queue.db";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + QueueContract.TABLE_NAME + " (" +
                    QueueContract._ID + " INTEGER PRIMARY KEY," +
                    QueueContract.COLUMN_SONG_IDENTIFIER + " INTEGER UNIQUE" + COMMA_SEP +
                    QueueContract.COLUMN_SONG_TITLE + " TEXT" + COMMA_SEP +
                    QueueContract.COLUMN_ARTIST_NAME + " TEXT" + COMMA_SEP +
                    QueueContract.COLUMN_ALBUM_TITLE + " TEXT" + COMMA_SEP +
                    QueueContract.COLUMN_TRACK_POSITION + " INTEGER" + COMMA_SEP +
                    QueueContract.COLUMN_TRACK_DURATION + " INTEGER" + COMMA_SEP +
                    QueueContract.COLUMN_ALBUM_IDENTIFIER + " INTEGER" + COMMA_SEP +
                    QueueContract.COLUMN_FILE_PATH +  " TEXT" +
                    " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + QueueContract.TABLE_NAME;

    private static final String[] sProjection = new String[] {
                    QueueContract._ID, //0
                    QueueContract.COLUMN_SONG_IDENTIFIER,
                    QueueContract.COLUMN_SONG_TITLE,
                    QueueContract.COLUMN_ARTIST_NAME,
                    QueueContract.COLUMN_ALBUM_TITLE,
                    QueueContract.COLUMN_TRACK_POSITION,
                    QueueContract.COLUMN_TRACK_DURATION,
                    QueueContract.COLUMN_ALBUM_IDENTIFIER,
                    QueueContract.COLUMN_FILE_PATH,
            };

    public QueueDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);

    }

    private void addInternal(SQLiteDatabase db, Song song) {

        ContentValues values = new ContentValues();
        values.put(QueueContract.COLUMN_SONG_IDENTIFIER, song.getSongId());
        values.put(QueueContract.COLUMN_SONG_TITLE, song.getSongTitle());
        values.put(QueueContract.COLUMN_ARTIST_NAME, song.getArtistName());
        values.put(QueueContract.COLUMN_ALBUM_TITLE, song.getAlbumTitle());
        values.put(QueueContract.COLUMN_TRACK_POSITION, song.getTrackPosition());
        values.put(QueueContract.COLUMN_TRACK_DURATION, song.getSongDuration());
        values.put(QueueContract.COLUMN_ALBUM_IDENTIFIER, song.getAlbumIdentifier());
        values.put(QueueContract.COLUMN_FILE_PATH, song.getFilePath());

        db.insert(QueueContract.TABLE_NAME, null, values);
    }

    public void removeAll() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(QueueContract.TABLE_NAME, null, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close(); // Fermer la base de données dans le bloc finally
        }
    }

    public void add() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        List<Song> queuePlaylist = PlayerService.getQueuePlayList();

        try {
            for(Song song: queuePlaylist) {
                addInternal(db, song);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        db.close();
    }


    public List<Song> readAll() {
        List<Song> songs = null;
        try {
            songs = read();
        } catch (SQLException e) {
            Log.e("QueueDbHelper", "Erreur lors de la lecture de la file d'attente", e);
        }
        return songs;
    }

    private List<Song> read() {
        SQLiteDatabase db = getReadableDatabase();
        List<Song> songs = new ArrayList<>();


        try (Cursor cursor = db.query(QueueContract.TABLE_NAME, sProjection, null, null, null, null, null)) {
            if (cursor.moveToFirst()) {

                int idCol = cursor.getColumnIndex(QueueContract.COLUMN_SONG_IDENTIFIER);

                int titleCol = cursor.getColumnIndex(QueueContract.COLUMN_SONG_TITLE);
                int artistCol = cursor.getColumnIndex(QueueContract.COLUMN_ARTIST_NAME);
                int albumCol = cursor.getColumnIndex(QueueContract.COLUMN_ALBUM_TITLE);
                int albumIdCol = cursor.getColumnIndex(QueueContract.COLUMN_ALBUM_IDENTIFIER);
                int trackCol = cursor.getColumnIndex(QueueContract.COLUMN_TRACK_POSITION);
                int trackDur = cursor.getColumnIndex(QueueContract.COLUMN_TRACK_DURATION);
                int pathCol = cursor.getColumnIndex(QueueContract.COLUMN_FILE_PATH);


                do {
                    long id = cursor.getLong(idCol);
                    String title = cursor.getString(titleCol);
                    String artist = cursor.getString(artistCol);
                    String album = cursor.getString(albumCol);
                    long albumId = cursor.getLong(albumIdCol);
                    int track = cursor.getInt(trackCol);
                    int duration = cursor.getInt(trackDur);
                    int year = 0;
                    String path = cursor.getString(pathCol);

                    songs.add(new Song(id, title, artist, album, albumId, track, duration, year, path));
                } while (cursor.moveToNext());
            }

        } finally {
                db.close();
        }

            return songs;
    }

    private static class QueueContract implements SongListColumns {
        static final String TABLE_NAME = "queue";
    }

}