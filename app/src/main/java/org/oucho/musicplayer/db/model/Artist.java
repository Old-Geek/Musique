package org.oucho.musicplayer.db.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Artist implements Parcelable {

    public static final Parcelable.Creator<Artist> CREATOR = new Parcelable.Creator<>() {
        @Override
        public Artist createFromParcel(Parcel source) {
            return new Artist(source);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    private final long id;
    private final String name;
    private final int numberOfAlbums;

    private Artist(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.numberOfAlbums = in.readInt();
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.numberOfAlbums);
    }
}
