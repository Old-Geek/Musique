package org.oucho.musicplayer.db.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.oucho.musicplayer.tools.RustineUTF8;

public class Song implements Parcelable {

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<>() {
        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    private final long songId;
    private final String songTitle;
    private final String artistName;
    private final String albumTitle;
    private final int trackPosition;
    private final long albumIdentifier;
    private final int songDuration;
    private final int releaseYear;
    private final String filePath;

    public Song(long songId, String songTitle, String artistName, String albumTitle, long albumIdentifier, int trackPosition, int songDuration, int releaseYear, String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("File path cannot be null");
        }
        this.songId = songId;
        this.songTitle = RustineUTF8.fixEncodingIfNeeded(songTitle);
        this.artistName = RustineUTF8.fixEncodingIfNeeded(artistName);
        this.albumTitle = RustineUTF8.fixEncodingIfNeeded(albumTitle);
        this.albumIdentifier = albumIdentifier;
        this.trackPosition = trackPosition;
        this.songDuration = songDuration;
        this.releaseYear = releaseYear;
        this.filePath = filePath;
    }

    private Song(Parcel in) {
        this.songId = in.readLong();
        this.songTitle = in.readString();
        this.artistName = in.readString();
        this.albumTitle = in.readString();
        this.trackPosition = in.readInt();
        this.albumIdentifier = in.readLong();
        this.songDuration = in.readInt();
        this.releaseYear = in.readInt();
        this.filePath = in.readString();
    }

    public long getSongId() {
        return songId;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getArtistName() {
        return artistName;
    }

    public long getAlbumIdentifier() {
        return albumIdentifier;
    }

    public int getTrackPosition() {
        return trackPosition;
    }

    public int getSongDuration() {
        return songDuration;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public String getFilePath() {
        return filePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.songId);
        dest.writeString(this.songTitle);
        dest.writeString(this.artistName);
        dest.writeString(this.albumTitle);
        dest.writeInt(this.trackPosition);
        dest.writeLong(this.albumIdentifier);
        dest.writeInt(this.songDuration);
        dest.writeInt(this.releaseYear);
        dest.writeString(this.filePath);
    }
}
