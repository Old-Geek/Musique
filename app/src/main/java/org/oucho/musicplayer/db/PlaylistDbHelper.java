package org.oucho.musicplayer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.MediaStore;
import android.widget.Toast;

import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class PlaylistDbHelper extends SQLiteOpenHelper implements AutoCloseable {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Playlists.db";

    private static final String COMMA_SEP =",";

    // Table des playlists
    private static final String SQL_CREATE_PLAYLISTS_TABLE = "CREATE TABLE " + PlaylistContract.TABLE_NAME + " (" +
    PlaylistContract._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    PlaylistContract.COLUMN_NAME_NAME + " TEXT);";

    private static final String SQL_DELETE_PLAYLISTS = "DROP TABLE IF EXISTS " + PlaylistContract.TABLE_NAME;

    // Table des chansons dans les playlists
    private static final String SQL_CREATE_PLAYLIST_SONGS =
            "CREATE TABLE " + PlaylistSongContract.TABLE_NAME + " (" +
                    PlaylistSongContract._ID + " INTEGER PRIMARY KEY," +
                    PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID + " INTEGER" + COMMA_SEP +
                    PlaylistSongContract.COLUMN_NAME_SONG_ID + " INTEGER" + COMMA_SEP +
                    "FOREIGN KEY (" + PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID + ") REFERENCES " +
                    PlaylistContract.TABLE_NAME + "(" + PlaylistContract._ID + ")" +
                    " )";

    private static final String SQL_DELETE_PLAYLIST_SONGS = "DROP TABLE IF EXISTS " + PlaylistSongContract.TABLE_NAME;

    private final Semaphore playlistSemaphore = new Semaphore(1);

    public PlaylistDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PLAYLISTS_TABLE);
        db.execSQL(SQL_CREATE_PLAYLIST_SONGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PLAYLIST_SONGS);
        db.execSQL(SQL_DELETE_PLAYLISTS);
        onCreate(db);
    }

    // Méthodes pour gérer les playlists
    public void addPlaylist(String playlistName) {
        try {
            playlistSemaphore.acquire();
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(PlaylistContract.COLUMN_NAME_NAME, playlistName);
            db.insert(PlaylistContract.TABLE_NAME, null, values);
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }

    public void removePlaylist(String playlistName) {
        try {
            playlistSemaphore.acquire();
            SQLiteDatabase db = getWritableDatabase();
            db.delete(PlaylistContract.TABLE_NAME, PlaylistContract.COLUMN_NAME_NAME + " = ?", new String[]{playlistName});
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }

    public void renamePlaylist(String oldPlaylistName, String newPlaylistName) {
        try {
            playlistSemaphore.acquire();
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(PlaylistContract.COLUMN_NAME_NAME, newPlaylistName);
            db.update(PlaylistContract.TABLE_NAME, values, PlaylistContract.COLUMN_NAME_NAME + " = ?", new String[]{oldPlaylistName});
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }

    public List<String> getAllPlaylists() {
        SQLiteDatabase db = getReadableDatabase();
        List<String> playlists = new ArrayList<>();
        try (Cursor cursor = db.query(PlaylistContract.TABLE_NAME, new String[]{PlaylistContract.COLUMN_NAME_NAME},
                null, null, null, null, null)) {
            if (cursor.moveToFirst()) {
                do {
                    playlists.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        }
        db.close();
        return playlists;
    }

    // Méthodes pour gérer les chansons dans les playlists
    public void addSongToPlaylist(String playlistName, Song song) {
        try {
            playlistSemaphore.acquire();
            try (SQLiteDatabase db = getWritableDatabase()) {
                long playlistId = getPlaylistId(db, playlistName);
                if (playlistId != -1) {
                    Context context = MusicPlayerApp.getInstance();
                    ContentValues values = new ContentValues();
                    values.put(PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID, playlistId);
                    values.put(PlaylistSongContract.COLUMN_NAME_SONG_ID, song.getSongId());
                    db.insert(PlaylistSongContract.TABLE_NAME, null, values);

                    String songTitle = song.getSongTitle();
                    String text = context.getResources().getString(R.string.song_added_to_playlist, songTitle, playlistName);
                    Toast.makeText(context,text, Toast.LENGTH_SHORT).show();
                }
            }
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }

    public void updatePlaylistOrder(String playlistName, List<Song> songs) {
        try {
            playlistSemaphore.acquire();
            SQLiteDatabase db = this.getWritableDatabase();
            try {
                db.beginTransaction();
                // Supprimer l'ancienne playlist
                db.delete(PlaylistSongContract.TABLE_NAME,
                        PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID + " = (SELECT " + PlaylistContract._ID + " FROM " + PlaylistContract.TABLE_NAME + " WHERE " + PlaylistContract.COLUMN_NAME_NAME + " = ?)",
                        new String[]{playlistName});

                // Recréer la playlist avec le nouvel ordre
                long playlistId = getPlaylistId(db, playlistName);
                for (Song song : songs) {
                    ContentValues values = new ContentValues();
                    values.put(PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID, playlistId);
                    values.put(PlaylistSongContract.COLUMN_NAME_SONG_ID, song.getSongId());
                    db.insert(PlaylistSongContract.TABLE_NAME, null, values);
                }
                db.setTransactionSuccessful();
            } finally {
                if (db != null) {
                    db.endTransaction();
                }
            }
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }


    public void removeSongFromPlaylist(String playlistName, long songId) {
        try {
            playlistSemaphore.acquire();
            try (SQLiteDatabase db = getWritableDatabase()) {
                long playlistId = getPlaylistId(db, playlistName);
                if (playlistId != -1) {
                    db.delete(PlaylistSongContract.TABLE_NAME,
                            PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID + " = ? AND " +
                                    PlaylistSongContract.COLUMN_NAME_SONG_ID + " = ?",
                            new String[]{String.valueOf(playlistId), String.valueOf(songId)});
                }
            }
        } catch (InterruptedException e) {
            // Gérer l'interruption
        } finally {
            playlistSemaphore.release();
        }
    }

    public List<Song> getSongsFromPlaylist(Context context, String playlistName) {
        try (SQLiteDatabase db = getReadableDatabase()) {
            List<Song> songs = new ArrayList<>();
            long playlistId = getPlaylistId(db, playlistName);
            if (playlistId != -1) {
                try (Cursor cursor = db.rawQuery("SELECT " + PlaylistSongContract.COLUMN_NAME_SONG_ID +
                                " FROM " + PlaylistSongContract.TABLE_NAME + " WHERE " +
                                PlaylistSongContract.COLUMN_NAME_PLAYLIST_ID + " = ?",
                        new String[]{String.valueOf(playlistId)})) {
                    if (cursor.moveToFirst()) {
                        int idCol = cursor.getColumnIndex(PlaylistSongContract.COLUMN_NAME_SONG_ID);
                        do {
                            long songId = cursor.getLong(idCol);
                            Song song = getSongDetails(context, songId); // Obtenir les détails de la chanson
                            if (song != null) {
                                songs.add(song);
                            }
                        } while (cursor.moveToNext());
                    }
                }
            }
            return songs;
        }
    }

    private Song getSongDetails(Context context, long songId) {
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.DATA
        };
        String selection = MediaStore.Audio.Media._ID + " = ?";
        String[] selectionArgs = { String.valueOf(songId)};
        try (Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                int titleCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
                int artistCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
                int albumCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM);
                int albumIdCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID);
                int trackCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TRACK);
                int durationCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
                int yearCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.YEAR);
                int dataCol = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

                String title = cursor.getString(titleCol);
                String artist = cursor.getString(artistCol);
                String album = cursor.getString(albumCol);
                long albumId = cursor.getLong(albumIdCol);
                int track = cursor.getInt(trackCol);
                int duration = cursor.getInt(durationCol);
                int year = cursor.getInt(yearCol);String path = cursor.getString(dataCol);

                return new Song(songId, title, artist, album, albumId, track, duration, year, path);
            }
        }
        return null;
    }

    private long getPlaylistId(SQLiteDatabase db, String playlistName) {
        long playlistId = -1;
        try (Cursor cursor = db.query(PlaylistContract.TABLE_NAME, new String[]{PlaylistContract._ID},
                PlaylistContract.COLUMN_NAME_NAME + " = ?", new String[]{playlistName}, null, null, null)) {
            if (cursor.moveToFirst()) {
                playlistId = cursor.getLong(0);
            }
        }
        return playlistId;
    }

    // Contrats pour les tables
    private static class PlaylistContract {
        static final String TABLE_NAME = "playlists";
        static final String _ID = "_id";
        static final String COLUMN_NAME_NAME = "name";
    }

    private static class PlaylistSongContract {
        static final String TABLE_NAME = "playlist_songs";
        static final String _ID = "_id";
        static final String COLUMN_NAME_PLAYLIST_ID = "playlist_id";
        static final String COLUMN_NAME_SONG_ID = "song_id";
    }
}
