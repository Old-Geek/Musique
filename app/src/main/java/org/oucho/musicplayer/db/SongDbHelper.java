package org.oucho.musicplayer.db;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.db.model.Song;

public class SongDbHelper {

    public static void addSongsToPlaylist(Context context, Album album, String playlistName) {
        long albumId = album.getAlbumId();
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(context)) {
            String[] projection = {
                    MediaStore.Audio.Media._ID,
                    MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.ALBUM,
                    MediaStore.Audio.Media.TRACK,
                    MediaStore.Audio.Media.DURATION,
                    MediaStore.Audio.Media.YEAR,
                    MediaStore.Audio.Media.DATA
            };
            String selection = MediaStore.Audio.Media.ALBUM_ID + " = ?";
            String[] selectionArgs = {String.valueOf(albumId)};
            try (Cursor cursor = context.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    selection,
                    selectionArgs,
                    null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    int idCol = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
                    int titleCol = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                    int artistCol = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
                    int albumCol = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
                    int trackCol = cursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
                    int durationCol = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
                    int yearCol = cursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
                    int dataCol = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

                    do {
                        long songId = cursor.getLong(idCol);
                        String songTitle = cursor.getString(titleCol);
                        String artistName = cursor.getString(artistCol);
                        String albumTitle = cursor.getString(albumCol);
                        int trackPosition = cursor.getInt(trackCol);
                        int songDuration = cursor.getInt(durationCol);
                        int releaseYear = cursor.getInt(yearCol);
                        String filePath = cursor.getString(dataCol);

                        Song song = new Song(songId, songTitle, artistName, albumTitle, albumId, trackPosition, songDuration, releaseYear, filePath);
                        dbHelper.addSongToPlaylist(playlistName, song);
                    } while (cursor.moveToNext());
                }
            }
        }
    }
}
