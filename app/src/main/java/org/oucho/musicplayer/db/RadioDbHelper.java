package org.oucho.musicplayer.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Radio;

import java.util.ArrayList;


public class RadioDbHelper extends SQLiteOpenHelper {

	private static final String RADIO_DB_NAME = "WebRadio.db";
	private static final String RADIO_TABLE_NAME = "webradio";
	private static final int DB_VERSION = 1;


	public RadioDbHelper(Context context) {
		super(context, RADIO_DB_NAME, null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE" + " " + RADIO_TABLE_NAME + " " +
				"(url TEXT PRIMARY KEY, name TEXT, image TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public static ArrayList<Radio> getRadios(Context context) {
		RadioDbHelper radiosDatabase = new RadioDbHelper(context);
		SQLiteDatabase db = radiosDatabase.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT url, name, image FROM " + RADIO_TABLE_NAME + " ORDER BY NAME COLLATE NOCASE", null);
		ArrayList<Radio> radios = new ArrayList<>();
		while (cursor.moveToNext()) {
			Radio radio = new Radio(cursor.getString(0), cursor.getString(1), cursor.getString(2));
			radios.add(radio);
		}
		db.close();
		cursor.close();
		return radios;
	}


	public static void addNewRadio(Context context, Radio radio) {

		ContentValues values = new ContentValues();
		values.put("url", radio.streamUrl());
		values.put("name", radio.stationName());
		values.put("image", radio.logoImage());

		try (RadioDbHelper radiosDatabase = new RadioDbHelper(context);
			 SQLiteDatabase db = radiosDatabase.getWritableDatabase()) {
			db.insertOrThrow(RADIO_TABLE_NAME, null, values);

			new Handler(Looper.getMainLooper()).post(() -> {
				String text = context.getResources().getString(R.string.addRadio_fromApp, radio.stationName());
				Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			});


		} catch (Exception e) {

			new Handler(Looper.getMainLooper()).post(() -> {
				String text = context.getResources().getString(R.string.addRadio_error);
				Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			});
			Log.e("Radio", "Error: " + e);
		}
	}

	public static void deleteRadio(Context context, Radio radio) {
		RadioDbHelper radiosDatabase = new RadioDbHelper(context);
		SQLiteDatabase db = radiosDatabase.getWritableDatabase();
		db.delete(RADIO_TABLE_NAME, "url = '" + radio.streamUrl() + "'", null);
		db.close();
	}

}
