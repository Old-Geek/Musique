package org.oucho.musicplayer.db;

import android.provider.BaseColumns;


interface SongListColumns extends BaseColumns {

    String COLUMN_ALBUM_TITLE = "album";
    String COLUMN_SONG_TITLE = "title";
    String COLUMN_ARTIST_NAME = "artist";
    String COLUMN_SONG_IDENTIFIER = "song_id";
    String COLUMN_ALBUM_IDENTIFIER = "album_id";
    String COLUMN_TRACK_POSITION = "number";
    String COLUMN_TRACK_DURATION = "duration";
    String COLUMN_FILE_PATH = "data";
}
