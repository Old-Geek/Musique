package org.oucho.musicplayer.db.model;

public record Radio(String streamUrl, String stationName, String logoImage) {
    public Radio {
        if (streamUrl == null) {
            throw new IllegalArgumentException("Stream URL cannot be null");
        }
    }
}
