package org.oucho.musicplayer.db.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

import org.oucho.musicplayer.tools.RustineUTF8;

public class Album implements Parcelable {

    public static final Parcelable.Creator<Album> CREATOR = new Parcelable.Creator<>() {
        @Override
        public Album createFromParcel(Parcel source) {
            return new Album(source);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    private final long albumId;
    private final String title;
    private final String artist;
    private final int releaseYear;

    public Album(long albumId, String title, String artist, int releaseYear) {
        this.albumId = albumId;
        this.title = (title == null) ? MediaStore.UNKNOWN_STRING : RustineUTF8.fixEncodingIfNeeded(title);
        this.artist = (artist == null) ? MediaStore.UNKNOWN_STRING : RustineUTF8.fixEncodingIfNeeded(artist);
        this.releaseYear = releaseYear;
    }

    private Album(Parcel in) {
        this.albumId = in.readLong();
        this.title = in.readString();
        this.artist = in.readString();
        this.releaseYear = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.albumId);
        dest.writeString(this.title);
        dest.writeString(this.artist);
        dest.writeInt(this.releaseYear);
    }

    public long getAlbumId() {
        return albumId;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public int getReleaseYear() {
        return releaseYear;
    }
}
