package org.oucho.musicplayer.ui.playbar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.queue.QueueManager;

import java.util.List;

public class PlayBarManager implements Keys {

    private final PlayBarHelper playBarHelper;
    private MainActivity activity;
    private final QueueManager queueManager;
    private ImageButton previousButton;
    private ImageButton forwardButton;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    final RelativeLayout playBarLayout;
    final LinearLayout playbarLayoutRadio;
    public final ProgressBar progressBar;
    final Runnable updateProgressBar;

    public PlayBarManager(MainActivity activity) {
        this.activity = activity;
        this.queueManager = activity.getQueueManager();
        this.progressBar = activity.findViewById(R.id.progress_bar);
        this.playBarLayout = activity.findViewById(R.id.playbar_layout_global);
        this.playbarLayoutRadio = activity.findViewById(R.id.playbar_layout_radio);
        this.updateProgressBar = mUpdateProgressBar;

        this.playBarHelper = new PlayBarHelper(playBarLayout, playbarLayoutRadio, progressBar, updateProgressBar);
        initializePlayBar();
    }

    private void initializePlayBar() {

        previousButton = activity.findViewById(R.id.quick_prev);
        forwardButton = activity.findViewById(R.id.quick_next);

        playBarLayout.setOnClickListener(mOnClickListener);

        previousButton.setOnClickListener(mOnClickListener);
        forwardButton.setOnClickListener(mOnClickListener);
        previousButton.setOnLongClickListener(mOnLongClickListener);
        forwardButton.setOnLongClickListener(mOnLongClickListener);

        activity.findViewById(R.id.play_pause_toggle).setOnClickListener(mOnClickListener);
        activity.findViewById(R.id.play_pause_radio_fragment).setOnClickListener(mOnClickListener);
        activity.findViewById(R.id.add_radio).setOnClickListener(mOnClickListener);
        activity.findViewById(R.id.radio_stop).setOnClickListener(mOnClickListener);
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onClick(View v) {
            if (activity.playerService == null)
                return;

            Context context = activity.getApplicationContext();
            int haptic = HapticFeedbackConstants.VIRTUAL_KEY;

            int viewId = v.getId();

            Intent radioService = new Intent(activity.getApplicationContext(), RadioService.class);

            if (viewId == R.id.add_radio) {
                v.performHapticFeedback(haptic);
                SendBroadcast.sending(activity.getApplicationContext(), RADIO_LOAD_TUNEIN_STATE);

            } else if (viewId == R.id.play_pause_radio_fragment) {
                v.performHapticFeedback(haptic);
                if (RadioState.isPlaying()) {
                    radioService.setAction(ACTION_RADIO_PAUSE);
                    context.startService(radioService);
                } else if (PlayerService.isPlaying()) {
                    SendBroadcast.sending(context, PLAYER_SERVICE_STOP);
                } else {
                    radioService.setAction(ACTION_RADIO_RESTART);
                    context.startService(radioService);
                }
                mHandler.postDelayed(PlayBarManager.this::setButtonDrawable, 150);

            } else if (viewId == R.id.radio_stop) {
                v.performHapticFeedback(haptic);
               if (RadioState.isPlaying()){
                    radioService.setAction(ACTION_RADIO_STOP);
                    context.startService(radioService);
                    ((TextView) activity.findViewById(R.id.radio_state)).setText(R.string.radio_stop);
                } else if (PlayerService.isPlaying()) {
                    SendBroadcast.sending(context, PLAYER_SERVICE_STOP);
                }
                mHandler.postDelayed(PlayBarManager.this::setButtonDrawable, 150);

            } else if (viewId == R.id.play_pause_toggle) {
                v.performHapticFeedback(haptic);
                if (RadioState.isPlaying() || RadioState.isPaused()) {
                    radioService.setAction(ACTION_RADIO_STOP);
                    context.startService(radioService);
                    ((TextView) activity.findViewById(R.id.radio_state)).setText(R.string.radio_stop);
                } else {
                    SendBroadcast.sending(context, PLAYER_TOGGLE_PLAY);
                    SendBroadcast.sending(context, INTENT_PLAYER_STATE, "state", "play");
                }

                mHandler.postDelayed(PlayBarManager.this::setButtonDrawable, 150);

            } else if (viewId == R.id.quick_prev) {
                v.performHapticFeedback(haptic);
                if (RadioState.isPlaying() || !PlayerService.isPlaying())
                    return;

                queueManager.enableAutoScroll(true);
                SendBroadcast.sending(context, PLAYER_PREVIOUS_TRACK);
                SendBroadcast.sending(context, INTENT_PLAYER_STATE, "state", "prev");
                queueManager.notifyQueueDataChanged();

                mHandler.postDelayed(PlayBarManager.this::updateQueue, 50);

            } else if (viewId == R.id.quick_next) {
                v.performHapticFeedback(haptic);
                if (RadioState.isPlaying() || !PlayerService.isPlaying())
                    return;

                queueManager.enableAutoScroll(true);
                SendBroadcast.sending(context, PLAYER_NEXT_TRACK);
                SendBroadcast.sending(context, INTENT_PLAYER_STATE, "state", "next");
                queueManager.notifyQueueDataChanged();
                mHandler.postDelayed(PlayBarManager.this::updateQueue, 50);

            }
        }
    };

    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (activity.playerService == null)
                return false;

            int viewId = v.getId();

            if (viewId == R.id.quick_prev) {
                mHandler.postDelayed(() -> {
                    if (previousButton.isPressed()) {
                        seekBy(-7000);
                        mHandler.postDelayed(PlayBarManager.this::rewind, 250);
                    }
                }, 300);
            } else if (viewId == R.id.quick_next) {
                mHandler.postDelayed(() -> {
                    if (forwardButton.isPressed()) {
                        seekBy(7000);
                        mHandler.postDelayed(PlayBarManager.this::forward, 250);
                    }
                }, 300);
            }
            return true;
        }
    };

    private void forward() {
        if (forwardButton.isPressed()) {
            seekBy(7000);
            mHandler.postDelayed(this::forward, 250);
        }
    }

    private void rewind() {
        if (previousButton.isPressed()) {
            seekBy(-7000);
            mHandler.postDelayed(this::rewind, 250);
        }
    }

    private void seekBy(int offset) {
        int position = PlayerService.getPlayerPosition();
        position += offset;
        SendBroadcast.sending(activity.getApplicationContext(), PLAYER_SERVICE_SEEK_TO, "msec", position);
    }

    public final Runnable mUpdateProgressBar = new Runnable() {

        @Override
        public void run() {
            progressBar.setCurrentProgress(PlayerService.getPlayerPosition());
            mHandler.postDelayed(mUpdateProgressBar, 250);
        }
    };

    private void updateQueue() {
        if (activity.playerService == null)
            return;

        List<Song> queue = PlayerService.getQueuePlayList();

        if (!queue.isEmpty()) {
            if (!queue.equals(queueManager.songQueue)) {
                queueManager.songQueue = queue;
                queueManager.queueAdapter.updateQueue(queueManager.songQueue);
                // Notify only the changed items
                queueManager.queueAdapter.notifyItemRangeChanged(0, queue.size());
            }
            setQueueSelection(PlayerService.getPositionWithinQueuePlayList());
        }
    }

    private void setQueueSelection(final int position) {
        queueManager.queueAdapter.setSelectedItem(position);
        if (queueManager.autoScrollEnabled) {
            mHandler.postDelayed(() -> {
                queueManager.queueRecyclerView.smoothScrollToPosition(position);
                queueManager.autoScrollEnabled = false;
            }, 100);
        }
    }

    public void handlePlayStateChanged() {
        playBarHelper.handlePlayStateChanged();
    }

    public void showPlayBarRadio(float tailleBarre) {
        playBarHelper.showRadioLayout(tailleBarre);
    }

    public void hidePlayBarRadio(float tailleBarre) {
        playBarHelper.hideRadioLayout(tailleBarre);
    }

    public void setButtonDrawable() {
        if (PlayerService.isPlaying() || RadioState.isPlaying()) {
            setPauseDrawable();
        } else {
            setPlayDrawable();
        }
    }

    private void setPauseDrawable() {
        ImageButton quickButton = (activity).findViewById(R.id.play_pause_toggle);
        ImageButton quickButton1 = (activity).findViewById(R.id.play_pause_radio_fragment);

        quickButton.setImageResource(R.drawable.pause_circle_48px);
        quickButton1.setImageResource(R.drawable.pause_circle_48px);
    }

    private void setPlayDrawable() {
        ImageButton quickButton = (activity).findViewById(R.id.play_pause_toggle);
        ImageButton quickButton1 = (activity).findViewById(R.id.play_pause_radio_fragment);

        quickButton.setImageResource(R.drawable.play_circle_48px);
        quickButton1.setImageResource(R.drawable.play_circle_48px);
    }

    public void clearReferences() {
        mHandler.removeCallbacksAndMessages(null);
        activity = null;
    }
}
