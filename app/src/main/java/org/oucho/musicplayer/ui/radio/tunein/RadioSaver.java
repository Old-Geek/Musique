package org.oucho.musicplayer.ui.radio.tunein;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.RadioDbHelper;
import org.oucho.musicplayer.db.model.Radio;
import org.oucho.musicplayer.tools.ImageTools;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.radio.MD5Helper;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

class RadioSaver implements Keys {

    private static final ExecutorService executor = Executors.newSingleThreadExecutor();
    private static final OkHttpClient client = new OkHttpClient();

    static void addRadioStation(String item) {
        String[] parts = item.split("\" ");

        if (item.contains("type=\"audio\"")) {
            String text = parts[1];
            String name = text.replace("text=\"", "");
            String url = null;
            String imageUrl = null;

            for (String part : parts) {
                if (part.contains("URL=\"")) {
                    url = part.replace("URL=\"", "");
                }

                if (part.contains("image=\"")) {
                    imageUrl = part.replace("image=\"", "");
                }
            }

            saveRadioStationAsync(url, name, imageUrl);
        }
    }

    public static void saveRadioStationAsync(String url, String name, String imageUrl) {
        executor.submit(() -> saveRadioStation(url, name, imageUrl));
    }

    private static void saveRadioStation(String httpRequest, String radioName, String imageUrl) {
        String radioUrl = fetchRadioUrl(httpRequest);

        if (radioUrl == null)
            return;

        String imagePath = fetchAndSaveImage(imageUrl, radioUrl);
        saveRadioDetails(radioUrl, renameRadio(radioName), imagePath);
    }

    private static String fetchRadioUrl(String httpRequest) {
        String radioUrl = null;

        try {
            String userAgent = Keys.getRadioAppUserAgent();
            Request request = new Request.Builder()
                    .url(httpRequest)
                    .addHeader("User-Agent", userAgent)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                assert response.body() != null;
                try (BufferedSource source = Okio.buffer(response.body().source())) {
                    String data = source.readUtf8();
                    String[] lines = data.split("\n");

                    // Liste des formats préférés dans cet ordre
                    List<String> preferredFormats = Arrays.asList("flac", "aac", "mp3", "ogg");

                    // Parcourir les lignes pour trouver l'URL avec le format préféré
                    for (String format : preferredFormats) {
                        for (String line : lines) {
                            if (line.contains(format)) {
                                radioUrl = line;
                                break;
                            }
                        }
                        if (radioUrl != null) {
                            break;
                        }
                    }

                    // Si aucun format préféré n'est trouvé, utiliser la première URL disponible
                    if (radioUrl == null && lines.length > 0) {
                        radioUrl = lines[0];
                    }
                }
            }

        } catch (SocketTimeoutException e) {
            SendBroadcast.sending(MusicPlayerApp.getInstance(), RADIO_ERROR_STATE, "error", "TimeoutException " + e);
        } catch (Exception e) {
            Log.e("RadioSaver", "An error occurred", e);
        }

        return radioUrl;
    }

    private static String fetchAndSaveImage(String imageUrl, String radioUrl) {
        try {
            String userAgent = Keys.getRadioAppUserAgent();
            Request request = new Request.Builder()
                    .url(imageUrl)
                    .addHeader("User-Agent", userAgent)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                assert response.body() != null;
                try (BufferedSource source = Okio.buffer(response.body().source())) {
                    byte[] bytes = source.readByteArray();
                    Mat mat = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_COLOR);

                    // Enhance and resize image using OpenCV
                    ImageTools imageTools = new ImageTools();
                    mat = imageTools.enhanceAndResizeImage(mat);

                    // Utiliser MatOfByte pour encoder l'image
                    MatOfByte matOfByte = new MatOfByte();
                    Imgcodecs.imencode(".jpg", mat, matOfByte);
                    byte[] jpegBytes = matOfByte.toArray();

                    String fileName = MD5Helper.hash(radioUrl) + ".jpg";
                    File file = new File(MusicPlayerApp.getInstance().getFilesDir(), fileName);
                    try (BufferedSink sink = Okio.buffer(Okio.sink(file))) {
                        sink.write(jpegBytes);
                    }

                    mat.release();
                    return file.getAbsolutePath();
                }
            }

        } catch (SocketTimeoutException e) {
            SendBroadcast.sending(MusicPlayerApp.getInstance(), RADIO_ERROR_STATE, "error", "TimeoutException " + e);
        } catch (Exception e) {
            Log.e("RadioSaver", "An error occurred", e);
        }

        return null;
    }

    private static void saveRadioDetails(String url, String name, String imagePath) {
        Context context = MusicPlayerApp.getInstance();

        if (url == null) {
            String text = context.getResources().getString(R.string.addRadio_error_url);
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            return;
        }

        Radio newRadio = new Radio(url, name, imagePath);
        RadioDbHelper.addNewRadio(context, newRadio);
    }

    private static String renameRadio(String radioname) {
        if (radioname == null) return "";

        // Supprimer le texte entre parenthèses à la fin
        String title = radioname.replaceAll("\\s*\\([^)]*\\)$", "");

        // Supprimer les chiffres de la fréquence
        title = title.replaceAll("\\b\\d+\\.\\d+\\b", "").trim();

        // Remplacer les mots spécifiques si le titre commence par "CHERIE" ou "RIRE"
        if (title.toUpperCase().startsWith("CHERIE") || title.toUpperCase().startsWith("RIRE")) {
            title = title.replaceAll("(?i)CHERIE", "Chérie")
                    .replaceAll("(?i)NOUVEAUTES", "Nouveautés")
                    .replaceAll("(?i)ANNEES", "Années")
                    .replaceAll("(?i)RIRE", "Rire");

            // Capitaliser la première lettre de chaque mot sauf le premier et garder "et" en minuscules
            String[] words = title.split(" ");
            for (int i = 1; i < words.length; i++) {
                if (!words[i].equalsIgnoreCase("ET")) {
                    words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
                } else {
                    words[i] = "et";
                }
            }

            title = String.join(" ", words);
        }

        return title;
    }
}
