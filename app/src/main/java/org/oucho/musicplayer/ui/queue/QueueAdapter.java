package org.oucho.musicplayer.ui.queue;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.helpers.ItemTouchHelperAdapter;
import org.oucho.musicplayer.ui.helpers.OnStartDragListener;

import java.util.Collections;
import java.util.List;

/** @noinspection ClassEscapesDefinedScope */
public class QueueAdapter extends BaseAdapter<QueueAdapter.QueueItemViewHolder> implements Keys, ItemTouchHelperAdapter {

    private final Context context;
    private final OnStartDragListener dragStartListener;
    private List<Song> songQueue;
    private int selectedItemPosition = -1;

    public QueueAdapter(Context context, OnStartDragListener dragStartListener) {
        this.context = context;
        this.dragStartListener = dragStartListener;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void updateQueue(List<Song> queue) {
        songQueue = queue;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QueueItemViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fragment_queue_item, parent, false);

        return new QueueItemViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(QueueItemViewHolder viewHolder, int position) {
        position = viewHolder.getBindingAdapterPosition();

        Song song = songQueue.get(position);
        long songId = PlayerService.getCurrentSong().getSongId();

        viewHolder.itemView.setSelected(position == selectedItemPosition);
        viewHolder.titleTextView.setText(song.getSongTitle());
        viewHolder.artistTextView.setText(song.getArtistName());

        if (song.getSongId() == songId)
            viewHolder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        else
            viewHolder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {

        if (fromPosition < toPosition)
            for (int i = fromPosition; i < toPosition; i++)
                Collections.swap(songQueue, i, i + 1);
        else
            for (int i = fromPosition; i > toPosition; i--)
                Collections.swap(songQueue, i, i - 1);

        notifyItemMoved(fromPosition, toPosition);
    }

    public void setSelectedItem(int position) {
        int oldSelection = selectedItemPosition;
        selectedItemPosition = position;

        if (oldSelection >= 0 && oldSelection < songQueue.size())
            notifyItemChanged(oldSelection);

        if (selectedItemPosition >= 0 && selectedItemPosition < songQueue.size())
            notifyItemChanged(selectedItemPosition);
    }


    @Override
    public int getItemCount() {
        if (songQueue == null)
            return 0;

        return songQueue.size();
    }


    @Override
    public void onItemDismiss(int position) {
        songQueue.remove(position);
        notifyItemRemoved(position);
    }


    class QueueItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final TextView titleTextView;
        final TextView artistTextView;
        final ImageButton reorderButton;
        final View itemView;

        @SuppressLint("ClickableViewAccessibility")
        QueueItemViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title);
            artistTextView = itemView.findViewById(R.id.artist);

            reorderButton = itemView.findViewById(R.id.reorder_button);
            reorderButton.setOnTouchListener((v, event) -> {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN)
                    dragStartListener.onStartDrag(QueueAdapter.QueueItemViewHolder.this);

                return false;
            });

            itemView.findViewById(R.id.song_info).setOnClickListener(this);
            itemView.setOnClickListener(this);
            this.itemView = itemView;
        }


        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onClick(View v) {
            final int position = getBindingAdapterPosition();

            triggerOnItemClickListener(position, v);

            if (v.getId() == R.id.song_info) {

                if (RadioState.isPlaying() || RadioState.isPaused()) {
                    Intent player = new Intent(context, RadioService.class);
                    player.setAction(ACTION_RADIO_STOP);
                    context.startService(player);

                    View rootView = getRootView(itemView);
                    TextView radioStateTextView = rootView.findViewById(R.id.radio_state);
                    radioStateTextView.setText(R.string.radio_stop);
                }

                Intent intent = new Intent(context, PlayerService.class);
                intent.setAction(PlayerService.PLAYER_SET_TRACK_POSITION);
                intent.putExtra("position", position);
                intent.putExtra("play", true);
                context.startService(intent);

                // Petit délai pour attendre la lecture de la musique avant de rafraichir la vue
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    notifyDataSetChanged();
                    // ce n'est pas la radio mais ça met à jour le boutton player
                    SendBroadcast.sending(context, RADIO_PLAYER_STATE);
                }, 150); //150ms de délai
            }
        }

        private View getRootView(View view) {
            ViewParent parent = view.getParent();
            while (parent instanceof View) {
                view = (View) parent;
                parent = view.getParent();
            }
            return view;
        }
    }

}
