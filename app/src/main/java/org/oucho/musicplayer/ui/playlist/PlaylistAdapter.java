package org.oucho.musicplayer.ui.playlist;


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;

import java.util.Collections;
import java.util.List;

/** @noinspection ClassEscapesDefinedScope */
public class PlaylistAdapter extends BaseAdapter<PlaylistAdapter.PlaylistViewHolder> {

    private List<String> playlistNames = Collections.emptyList();

    public PlaylistAdapter() {}


    @SuppressLint("NotifyDataSetChanged")
    public void updateData(List<String> data) {
        playlistNames = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_playlist_item, parent, false);
        return new PlaylistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder viewHolder, int position) {
        String playlistName = playlistNames.get(position);
        viewHolder.playlistNameTextView.setText(playlistName);
    }

    @Override
    public int getItemCount() {
        return playlistNames.size();
    }

    public String getPlaylistName(int position) {
        return playlistNames.get(position);
    }

    class PlaylistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        final TextView playlistNameTextView;

        private PlaylistViewHolder(View itemView) {
            super(itemView);
            playlistNameTextView = itemView.findViewById(R.id.playlist_name);
            RelativeLayout layout = itemView.findViewById(R.id.playlist_item);
            layout.setOnLongClickListener(this);
            layout.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int position = getBindingAdapterPosition();
            triggerOnItemClickListener(position, view);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getBindingAdapterPosition();
            triggerOnItemLongClickListener(position, view);

            return true;
        }
    }
}
