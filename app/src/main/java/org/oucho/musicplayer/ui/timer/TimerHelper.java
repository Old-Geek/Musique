package org.oucho.musicplayer.ui.timer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.oucho.musicplayer.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TimerHelper {

    private final ScheduledExecutorService scheduler;
    private ScheduledFuture<?> scheduledTask;
    private boolean isTimerRunning;
    private final TextView timerDisplay;
    private final WeakReference<Context> contextRef;
    private final VolumeTimer volumeTimer;
    private CountDownTimer countdownTimer;
    private final LayoutInflater layoutInflater;
    private final List<TimerListener> listeners = new ArrayList<>();

    final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;

    public TimerHelper(TextView timerDisplay, Context context, LayoutInflater layoutInflater) {
        this.timerDisplay = timerDisplay;
        this.contextRef = new WeakReference<>(context);
        this.layoutInflater = layoutInflater;
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.volumeTimer = new VolumeTimer();
    }

    private Context getContext() {
        return contextRef.get();
    }

    public void addListener(TimerListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TimerListener listener) {
        listeners.remove(listener);
    }

    public void showTimePicker() {
        final String start = getContext().getString(R.string.start);
        final String cancel = getContext().getString(R.string.cancel);

        View view = layoutInflater.inflate(R.layout.dialog_timer_picker, null);

        final SeekArc mSeekArc = view.findViewById(R.id.seekArc);
        final TextView mSeekArcProgress = view.findViewById(R.id.seekArcProgress);
        mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {


            @Override
            public void onStopTrackingTouch() {
                view.performHapticFeedback(haptic);
            }

            @Override
            public void onStartTrackingTouch() {
                final int hapticRelease = HapticFeedbackConstants.VIRTUAL_KEY_RELEASE;
                view.performHapticFeedback(hapticRelease);
            }

            @Override
            public void onProgressChanged(int progress) {
                String minute = (progress <= 1) ? "minute" : "minutes";
                String textProgress = progress + " " + minute;
                mSeekArcProgress.setText(textProgress);
            }
        });

        new AlertDialog.Builder(getContext())
                .setPositiveButton(start, (dialog, which) -> {
                    int mins = mSeekArc.getProgress();
                    startTimer(mins);
                    view.performHapticFeedback(haptic);
                })
                .setNegativeButton(cancel, (dialog, which) -> view.performHapticFeedback(haptic))
                .setView(view)
                .create()
                .show();
    }

    public void showTimerInfo() {
        final String continuer = getContext().getString(R.string.continuer);
        final String cancelTimer = getContext().getString(R.string.cancel_timer);
        final String stopTimer = getContext().getString(R.string.stop_timer);

        if (scheduledTask.getDelay(TimeUnit.MILLISECONDS) < 0) {
            cancelTimer();
            return;
        }

        View view = layoutInflater.inflate(R.layout.dialog_timer_info, null);
        final TextView timeLeft = view.findViewById(R.id.time_left);

        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setPositiveButton(continuer, (dialog12, which) -> dialog12.dismiss())
                .setNegativeButton(cancelTimer, (dialog1, which) -> {
                    cancelTimer();
                    Toast.makeText(getContext(), stopTimer, Toast.LENGTH_LONG).show();
                })
                .setView(view)
                .create();

        new CountDownTimer(scheduledTask.getDelay(TimeUnit.MILLISECONDS), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                timeLeft.setText(formatTime(seconds));
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
            }
        }.start();

        dialog.show();
    }

    public void startTimer(int minutes) {
        final String impossible = getContext().getString(R.string.impossible);
        final String arret = getContext().getString(R.string.arret);
        final String minuteTxt = minutes == 1 ? getContext().getString(R.string.minute_singulier) : getContext().getString(R.string.minute_pluriel);

        final int delay = minutes * 60 * 1000;

        if (delay == 0) {
            Toast.makeText(getContext(), impossible, Toast.LENGTH_LONG).show();
            return;
        }

        scheduledTask = scheduler.schedule(new StopTimerTask(), delay, TimeUnit.MILLISECONDS);
        Toast.makeText(getContext(), arret + " " + minutes + " " + minuteTxt, Toast.LENGTH_LONG).show();

        isTimerRunning = true;
        displayTimer();
        volumeTimer.startVolumeTimer(getContext(), scheduledTask, delay);
    }

    public void cancelTimer() {
        if (isTimerRunning) {
            if (scheduledTask != null) {
                scheduledTask.cancel(true);
            }
            if (countdownTimer != null) {
                countdownTimer.cancel();
                countdownTimer = null;
            }
            volumeTimer.getVolumeTimer().cancel();
            volumeTimer.setVolume(1.0f);
        }

        isTimerRunning = false;
        updateTimerVisibility(View.GONE);
        notifyListenersFinish();
    }

    public boolean isTimerRunning() {
        return isTimerRunning;
    }

    private void displayTimer() {
        updateTimerVisibility(View.VISIBLE);

        countdownTimer = new CountDownTimer(scheduledTask.getDelay(TimeUnit.MILLISECONDS), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                String textTemps = "zZz " + formatTime(seconds);
                updateTimerText(textTemps);
                notifyListenersUpdate(textTemps);
            }

            @Override
            public void onFinish() {
                updateTimerVisibility(View.GONE);
                notifyListenersFinish();
            }
        }.start();
    }

    private void updateTimerVisibility(int visibility) {
        timerDisplay.setVisibility(visibility);
    }

    private void updateTimerText(String text) {
        timerDisplay.setText(text);
    }

    private void notifyListenersUpdate(String text) {
        for (TimerListener listener : listeners) {
            listener.onTimerUpdate(text);
        }
    }

    private void notifyListenersFinish() {
        for (TimerListener listener : listeners) {
            listener.onTimerFinish();
        }
    }

    @SuppressLint("DefaultLocale")
    private String formatTime(long seconds) {
        long minutes = seconds / 60;
        long remainingSeconds = seconds % 60;
        return String.format("%02d:%02d", minutes, remainingSeconds);
    }

    class StopTimerTask implements Runnable {
        public void run() {
            if (isTimerRunning) {
                scheduledTask.cancel(true);
                isTimerRunning = false;
            }
        }
    }
}