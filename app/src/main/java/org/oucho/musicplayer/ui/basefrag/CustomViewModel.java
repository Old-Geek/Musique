package org.oucho.musicplayer.ui.basefrag;

import androidx.lifecycle.ViewModel;

public class CustomViewModel extends ViewModel {
    private boolean isAlbumFragmentVisible = false;
    private boolean isQueueLayoutVisible = false;

    /** @noinspection BooleanMethodIsAlwaysInverted*/
    public boolean isAlbumFragmentVisible() {
        return isAlbumFragmentVisible;
    }

    public void setAlbumFragmentVisible(boolean visible) {
        isAlbumFragmentVisible = visible;
    }

    public boolean isQueueLayoutVisible() {
        return isQueueLayoutVisible;
    }

    public void setQueueLayoutVisible(boolean visible) {
        isQueueLayoutVisible = visible;
    }
}
