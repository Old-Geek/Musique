package org.oucho.musicplayer.ui.queue;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.ui.helpers.ItemTouchHelperCallback;
import org.oucho.musicplayer.ui.helpers.OnStartDragListener;
import org.oucho.musicplayer.ui.queue.blurview.BlurView;
import org.oucho.musicplayer.ui.view.CustomLayoutManager;

import java.util.List;


public class QueueManager implements OnStartDragListener {

    private final Context context;
    public final View queueLayout;
    public final RecyclerView queueRecyclerView;
    public QueueAdapter queueAdapter;
    private final BlurView blurView;
    private ItemTouchHelper itemTouchHelper;
    public List<Song> songQueue;
    public boolean autoScrollEnabled = false;
    private final View rootView;

    public QueueManager(Context context, View rootView) {
        this.context = context;
        this.rootView = rootView;
        this.queueLayout = rootView.findViewById(R.id.queue_layout);
        this.queueRecyclerView = rootView.findViewById(R.id.queue_view);
        this.blurView = rootView.findViewById(R.id.queueView);

        setupQueueView();
        setupBlurView();
    }

    private void setupQueueView() {
        queueRecyclerView.setLayoutManager(new CustomLayoutManager(context));
        queueAdapter = new QueueAdapter(context, this);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(queueAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(queueRecyclerView);

        queueRecyclerView.setAdapter(queueAdapter);
    }

    private void setupBlurView() {
        float radius = 10f;
        ViewGroup rootView = ((MainActivity) context).findViewById(R.id.activ_main);
        View decorView = ((MainActivity) context).getWindow().getDecorView();
        Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurRadius(radius)
                .setOverlayColor(Color.parseColor("#80FFFFFF"));
    }

    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
        rootView.performHapticFeedback(haptic);
        itemTouchHelper.startDrag(viewHolder);
    }

    public void toggleQueueVisibility() {
        if (queueLayout.getVisibility() != View.VISIBLE) {
            queueLayout.setVisibility(View.VISIBLE);
            blurView.setVisibility(View.VISIBLE);
        } else {
            queueLayout.setVisibility(View.GONE);
            blurView.setVisibility(View.GONE);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void refreshQueue() {
        if (PlayerService.getQueuePlayList() == null) return;

        List<Song> queue = PlayerService.getQueuePlayList();

        if (!queue.isEmpty()) {
            if (!queue.equals(songQueue)) {
                songQueue = queue;
                queueAdapter.updateQueue(songQueue);
            }

            queueAdapter.notifyDataSetChanged();
            setSelectedQueueItem(PlayerService.getPositionWithinQueuePlayList());
        }
    }

    private void setSelectedQueueItem(final int position) {
        queueAdapter.setSelectedItem(position);

        if (autoScrollEnabled) {
            queueRecyclerView.postDelayed(() -> {
                queueRecyclerView.smoothScrollToPosition(position);
                autoScrollEnabled = false;
            }, 100);
        }
    }

    public void enableAutoScroll(boolean autoScrollEnabled) {
        this.autoScrollEnabled = autoScrollEnabled;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyQueueDataChanged() {
        queueAdapter.notifyDataSetChanged();
    }

}