package org.oucho.musicplayer.ui.radio.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;


public class Connectivity extends BroadcastReceiver implements Keys {

   private static ConnectivityManager connectivity = null;
   private final Context context;
   private final RadioService radioService;
   private static final int TYPE_NONE = -1;
   private static int previous_type = TYPE_NONE;
   private Handler handler;
   private NetworkRequest networkRequest;
   private NetworkCallback networkCallback;

   public Connectivity(Context a_context, RadioService a_player) {

      context = a_context;
      radioService = a_player;

      initConnectivity(context);
      initNetworkLock();

      ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
   }

   private void initNetworkLock() {
      networkRequest = new NetworkRequest.Builder()
              .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
              .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
              .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
              .build();

      networkCallback = new ConnectivityManager.NetworkCallback() {
         @Override
         public void onAvailable(@NonNull Network network) {
            Log.i("Connectivity", "La connectivité réseau est disponible");
         }

         @Override
         public void onLost(@NonNull Network network) {
            Log.i("Connectivity", "La connectivité réseau est perdue");
         }
      };
   }


   static private void initConnectivity(Context context) {

      if ( connectivity == null )
         connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      if ( connectivity != null )
         setPreviousType(getType(context));
   }

   public void lockNetwork() {
      // Verrouille la connectivité aux réseaux Wi-Fi et cellulaires (4G/5G)
      if (connectivity != null && networkRequest != null && networkCallback != null)
         connectivity.requestNetwork(networkRequest, networkCallback);
   }

   public void unlockNetwork() {
      // Libère le verrouillage de la connectivité réseau
      if (connectivity != null && networkCallback != null)
         connectivity.unregisterNetworkCallback(networkCallback);
   }

   public void destroy() {
      context.unregisterReceiver(this);
   }

   static private int getType(Context context) {
      return getType(context, null);
   }

   static private int getType(Context context, Intent intent) {

      if (connectivity == null)
         return TYPE_NONE;

      if (intent != null && intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false))
         return TYPE_NONE;

      Network network = connectivity.getActiveNetwork();
      if (network != null) {
         ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
         NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
         if (networkCapabilities != null && (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                 || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                 || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))) {int type = TYPE_NONE;

            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
               type = NetworkCapabilities.TRANSPORT_WIFI;
            else if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
               type = NetworkCapabilities.TRANSPORT_CELLULAR;
            else if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
               type = NetworkCapabilities.TRANSPORT_ETHERNET;

            return type;
         }
      }

      return TYPE_NONE;
   }


   static public boolean isConnected(Context context) {
      initConnectivity(context);
      return (getType(context) != TYPE_NONE);
   }


   @Override
   public void onReceive(Context context, Intent intent) {

      int type = getType(context, intent);
      int then = 0;

      boolean want_network_playing = RadioState.isWantPlaying() && radioService.isNetworkUrl();

      if ( type == TYPE_NONE && getPreviousType() != TYPE_NONE && want_network_playing )
         droppedConnection();

      if ( getPreviousType() == TYPE_NONE && type != getPreviousType() && Counter.still(then) )
         restart();

      if ( getPreviousType() != TYPE_NONE && type != TYPE_NONE && type != getPreviousType() && want_network_playing )
         restart();

      setPreviousType(type);
   }


   public void droppedConnection() {

      RadioState.setState(context, RadioState.STATE_DISCONNECTED, true);

      handler = new Handler(Looper.getMainLooper());
      handler.postDelayed(() -> {

         if (RadioState.isOnline(context)) {

            Intent player = new Intent(context, RadioService.class);
            player.setAction(ACTION_RADIO_STOP);
            context.startService(player);

            Intent player2 = new Intent(context, RadioService.class);
            player2.setAction(ACTION_RADIO_PLAY);
            context.startService(player2);

            reconnect();

         } else {

            Intent player3 = new Intent(context, RadioService.class);
            player3.setAction(ACTION_RADIO_STOP);
            context.startService(player3);
         }

      }, 2000);

   }

   private void reconnect() {

      handler = new Handler(Looper.getMainLooper());
      handler.postDelayed(() -> {

         if (!RadioState.isPlaying()) {

            Intent player2 = new Intent(context, RadioService.class);
            player2.setAction(ACTION_RADIO_PLAY);
            context.startService(player2);
         }
      }, 5000);

   }


   private void restart() {

      Intent playerS = new Intent(context, RadioService.class);
      playerS.setAction(ACTION_RADIO_STOP);
      context.startService(playerS);

      Intent playerP = new Intent(context, RadioService.class);
      playerP.setAction(ACTION_RADIO_PLAY);
      context.startService(playerP);

   }

   private static int getPreviousType() {
      return previous_type;
   }

   private static void setPreviousType(int value) {
      previous_type = value;
   }
}

