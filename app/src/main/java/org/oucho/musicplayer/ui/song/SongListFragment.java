package org.oucho.musicplayer.ui.song;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuProvider;
import androidx.lifecycle.Lifecycle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.basefrag.SortOrder;
import org.oucho.musicplayer.ui.helpers.TitleSet;
import org.oucho.musicplayer.ui.playlist.CreatePlaylistDialog;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.tags.SongTagHelper;
import org.oucho.musicplayer.ui.view.bubulle.BubulleRecyclerView;

import java.util.ArrayList;
import java.util.List;


public class SongListFragment extends BaseFragment implements Keys {

    private SongListAdapter musicAdapter;
    private String title;
    private String sortCriteria;
    private List<Song> songList;
    private Menu menu;
    private OnBackInvokedCallback backCallback;

    public static SongListFragment newInstance() {
        return new SongListFragment();
    }

    private final LoaderManager.LoaderCallbacks<List<Song>> loaderCallbacks = new LoaderManager.LoaderCallbacks<>() {

        @NonNull
        @Override
        public Loader<List<Song>> onCreateLoader(int id, Bundle args) {
            SongLoader loader = new SongLoader(getActivity());

            loader.setSortOrder(Preferences.getSongSort());
            return loader;
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<Song>> loader, List<Song> songList) {
            updateAdapter(songList);
            SongListFragment.this.songList = songList;
        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<Song>> loader) {
            //  Auto-generated method stub

        }
    };


    private void updateAdapter(List<Song> songList) {
        musicAdapter.updateData(songList);
    }

    private final BaseAdapter.OnItemClickListener itemClickListener = (position, view) -> {

        final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
        view.performHapticFeedback(haptic);

        int viewId = view.getId();

        if (viewId == R.id.item_view)
            selectSong(position);
        else if (viewId == R.id.menu_button)
            showMenu(position, view);
    };


    /* *********************************************************************************************
     * Menu titre
     * ********************************************************************************************/

    private void showMenu(final int position, View v) {
        final Song song = musicAdapter.getItem(position);

        PopupMenu popup = new PopupMenu(requireActivity(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.song_item, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();

            if (itemId == R.id.action_add_to_queue)
                ((MainActivity) requireActivity()).addToQueue(song);
            else if (itemId == R.id.action_add_to_playlist)
                showPlaylistDialog(position);
            else if (itemId == R.id.action_show_tag) {
                Log.d("TAG", "showTags:");
                SongTagHelper songTagHelper = new SongTagHelper(this, song, position, songList);
                songTagHelper.showTags();
            }

            return true; // Retourne true pour indiquer que le clic a été géré
        });

        popup.show();
    }

    private void showPlaylistDialog(final int position) {
        final Song song = musicAdapter.getItem(position);
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(requireActivity())) {
            List<String> playlists = dbHelper.getAllPlaylists();

            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setTitle(R.string.add_to_playlist);

            CharSequence[] playlistNames = playlists.toArray(new CharSequence[0]);

            builder.setItems(playlistNames, (dialog, which) -> {
                String playlistName = playlists.get(which);
                dbHelper.addSongToPlaylist(playlistName, song);
            });

            builder.setPositiveButton(R.string.create_playlist, (dialog, which) -> showCreatePlaylistDialog(song));

            builder.setNegativeButton(android.R.string.cancel, null);
            builder.show();
        }
    }

    private void showCreatePlaylistDialog(final Song song) {
        CreatePlaylistDialog dialog = CreatePlaylistDialog.newInstance();
        dialog.setOnPlaylistCreatedListener(playlistName -> {
            try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(requireActivity())) {
                dbHelper.addSongToPlaylist(playlistName, song);
            }
        });
        dialog.show(getChildFragmentManager(), "create_playlist");
    }

    private void selectSong(int position) {

        MainActivity activity = (MainActivity) requireActivity();
        activity.onSongSelected(musicAdapter.getSongList(), position);

    }

    @Override
    public void load() {
        LoaderManager.getInstance(this).restartLoader(0, null, getLoaderCallbacks());
    }



    /* *********************************************************************************************
     * Création de l'activité
     * ********************************************************************************************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        title = requireContext().getString(R.string.titles);

        setSortCriteria();
    }


    /* *********************************************************************************************
     * Création de la vue
     * ********************************************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_song, container, false);

        BubulleRecyclerView mRecyclerView = rootView.findViewById(R.id.recycler_view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        musicAdapter = new SongListAdapter(getActivity());
        musicAdapter.setOnItemClickListener(itemClickListener);

        mRecyclerView.setAdapter(musicAdapter);

        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoaderManager.getInstance(this).initLoader(0, null, getLoaderCallbacks());
        setOptionsMenu();
    }

    private void setOptionsMenu() {
        requireActivity().addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menuInflater.inflate(R.menu.song_sort_by, menu);
                SongListFragment.this.menu = menu; // Stocker la référence au menu
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {

                int itemId = menuItem.getItemId();

                if (itemId == R.id.menu_sort_by_az) {
                    Preferences.edit(SONG_SORT_CRITERIA, SortOrder.SongSortOrder.SONG_TITLE_A_Z);
                    load();
                    sortCriteria = "a-z";
                } else if (itemId == R.id.menu_sort_by_album) {
                    Preferences.edit(SONG_SORT_CRITERIA, SortOrder.SongSortOrder.SONG_ALBUM);
                    load();
                    sortCriteria = requireContext().getString(R.string.title_sort_album);
                } else if (itemId == R.id.menu_sort_by_artist) {
                    Preferences.edit(SONG_SORT_CRITERIA, SortOrder.SongSortOrder.SONG_ARTIST_IGNORE_THE);
                    load();
                    sortCriteria = requireContext().getString(R.string.title_sort_artist);
                } else if (itemId == R.id.menu_sort_by_year) {
                    Preferences.edit(SONG_SORT_CRITERIA, SortOrder.SongSortOrder.SONG_YEAR_DESC);
                    load();
                    sortCriteria = requireContext().getString(R.string.title_sort_year);
                } else if (itemId == R.id.action_search) {
                    return performSearch();
                }

                TitleSet.setTitle((AppCompatActivity) getActivity(), title, sortCriteria);

                return true;
            }
        }, getViewLifecycleOwner(), Lifecycle.State.RESUMED);
    }


    @Override
    public void onPause() {
        super.onPause();

        requireActivity().getOnBackInvokedDispatcher().
                unregisterOnBackInvokedCallback(backCallback);
    }


    @Override
    public void onResume() {
        super.onResume();

        MainActivity.setCurrentViewID(R.id.fragment_song_layout);
        TitleSet.setTitle((AppCompatActivity) getActivity(), title, sortCriteria);

        setBackInvokedCallback();
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;

            if (mQueueView.getVisibility() == View.VISIBLE)
                queueManager.toggleQueueVisibility();
            else
                SendBroadcast.sending(requireContext(), BACK_VIEWPAGER, true);
        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }


    private LoaderManager.LoaderCallbacks<List<Song>> getLoaderCallbacks() {
        return loaderCallbacks;
    }


    /** @noinspection SameReturnValue*/
    private boolean performSearch() {

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            View actionView = searchItem.getActionView();
            if (actionView instanceof SearchView) {
                searchView = (SearchView) actionView;
            }
        }

        final String getTri = Preferences.getSongSort();

        assert searchView != null;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterSongs(newText);
                return true;
            }


            @SuppressLint("NotifyDataSetChanged")
            private void filterSongs(String query) {
                List<Song> filteredSongs = filterSongsByQuery(query);

                musicAdapter.updateData(filteredSongs);
                musicAdapter.notifyDataSetChanged();
            }

            private List<Song> filterSongsByQuery(String query) {

                List<Song> filteredSongs = new ArrayList<>();

                for (Song song : songList) { // listeTitre est votre liste originale d'albums

                    if (getTri.equals("REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>')")) {

                        if (song.getArtistName().toLowerCase().contains(query.toLowerCase()))
                            filteredSongs.add(song);

                    } else if (getTri.equals("album")) {

                        if (song.getAlbumTitle().toLowerCase().contains(query.toLowerCase()))
                            filteredSongs.add(song);
                    } else {

                        if (song.getSongTitle().toLowerCase().contains(query.toLowerCase()))
                            filteredSongs.add(song);
                    }
                }
                return filteredSongs;
            }
        });

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(@NonNull MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(@NonNull MenuItem item) {
                return true;
            }
        });

        return true;
    }

    /* *********************************************************************************************
     * Titre
     * ********************************************************************************************/

    private void setSortCriteria() {
        String sortCriteria = Preferences.getSongSort();

        switch (sortCriteria) {
            case "year DESC":
                this.sortCriteria = requireContext().getString(R.string.title_sort_year);
                break;
            case "REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC":
                this.sortCriteria = requireContext().getString(R.string.title_sort_artist);
                break;
            case "album":
                this.sortCriteria = requireContext().getString(R.string.title_sort_album);
                break;
            default:
                this.sortCriteria = "a-z";
                break;
        }
    }

}