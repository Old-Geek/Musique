package org.oucho.musicplayer.ui.view.bubulle;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.ColorInt;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.ui.view.bubulle.FastScrollBubble.SectionIndexer;

@SuppressWarnings("unused")
public class BubulleRecyclerView extends RecyclerView {

    private FastScrollBubble fastScrollBubble;

    public BubulleRecyclerView(Context context) {
        super(context);
        layout(context, null);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public BubulleRecyclerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BubulleRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        layout(context, attrs);
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);

        if (adapter instanceof SectionIndexer)
            setSectionIndexer((SectionIndexer) adapter);
        else if (adapter == null)
            setSectionIndexer(null);
    }


    private void setSectionIndexer(SectionIndexer sectionIndexer) {
        fastScrollBubble.setSectionIndexer(sectionIndexer);
    }

    public void setFastScrollEnabled(boolean enabled) {
        fastScrollBubble.setEnabled(enabled);
    }

    public void setHideScrollbar(boolean hideScrollbar) {
        fastScrollBubble.setHideScrollbar(hideScrollbar);
    }

    public void setHandleColor(@ColorInt int color) {
        fastScrollBubble.setHandleColor(color);
    }

    public void setBubbleColor(@ColorInt int color) {
        fastScrollBubble.setBubbleColor(color);
    }

    public void setBubbleTextColor(@ColorInt int color) {
        fastScrollBubble.setBubbleTextColor(color);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        fastScrollBubble.attachRecyclerView(this);

        ViewParent parent = getParent();

        if (parent instanceof ViewGroup viewGroup) {
            if (fastScrollBubble.getParent() == null) {
                viewGroup.addView(fastScrollBubble);
                fastScrollBubble.setLayoutParams(viewGroup);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        fastScrollBubble.detachRecyclerView();
        super.onDetachedFromWindow();
    }

    private void layout(Context context, AttributeSet attrs) {
        fastScrollBubble = new FastScrollBubble(context, attrs);
        fastScrollBubble.setId(R.id.fastscroller);
    }
}
