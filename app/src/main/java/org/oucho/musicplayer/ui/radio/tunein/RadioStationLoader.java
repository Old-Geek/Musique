package org.oucho.musicplayer.ui.radio.tunein;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseLoader;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSource;
import okio.Okio;

public class RadioStationLoader extends BaseLoader<List<String>> implements Keys {

    private static final String TAG = "RadioStationLoader";
    private final String radioUrl;
    private static final OkHttpClient client = new OkHttpClient();

    public RadioStationLoader(Context context, String url) {
        super(context);
        this.radioUrl = url;
    }

    @Override
    public List<String> loadInBackground() {

        List<String> stationList = null;

        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();

        try {

            String userAgent = Keys.getRadioAppUserAgent();
            Request request = new Request.Builder()
                    .url(radioUrl)
                    .addHeader("User-Agent", userAgent)
                    .addHeader("Accept-Language", language + "-" + country)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                assert response.body() != null;
                try (BufferedSource source = Okio.buffer(response.body().source())) {
                    String data = source.readUtf8();
                    stationList = parseResponse(data);
                }
            }

        } catch (SocketTimeoutException e) {
            SendBroadcast.sending(getContext(), RADIO_ERROR_STATE, "error", "TimeoutException " + e);
        } catch (Exception e) {
            Log.e(TAG, "Une erreur est survenue", e);
        }

        return stationList;
    }

    private List<String> parseResponse(String string) {

        // Parse header
        if (!string.contains("<opml version=\"1\">"))
            return null;

        String header = string;
        header = header.substring(header.indexOf("<head>") + 6, header.indexOf("</head>"));

        String status = header.substring(header.indexOf("<status>") + 8, header.indexOf("</status>"));

        if (status.equals("400")) {
            Log.e(TAG, "Error: " + header.substring(header.indexOf("<fault>") + 7, header.indexOf("</fault>")));
            Log.e(TAG, "Error code: " + header.substring(header.indexOf("<fault_code>") + 12, header.indexOf("</fault_code>")));
            Toast.makeText(getContext(), "Error code: 400", Toast.LENGTH_LONG).show();
            return null;
        }

        if (status.equals("200") && header.contains("<title>")) {
            String title = header.substring(header.indexOf("<title>") + 7, header.indexOf("</title>"));
            SendBroadcast.sending(getContext(), RADIO_PLAYER_STATE, "titre", title);
        }

        List<String> stationList = new ArrayList<>();

        String body = string;
        body = body.substring(body.indexOf("<body>") + 6, body.indexOf("</body>"));
        body = body.substring(2, body.length() - 1);

        final String[] lines = body.split("\r\n|\r|\n");

        for (String line : lines) {
            String type = line.replace("<outline ", "").replace("/>", "").replace("\n", "");
            stationList.add(Html.fromHtml(type, Html.FROM_HTML_MODE_LEGACY).toString());
        }

        return stationList;
    }
}
