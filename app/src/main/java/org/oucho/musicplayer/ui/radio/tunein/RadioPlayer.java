package org.oucho.musicplayer.ui.radio.tunein;

import android.content.Intent;
import android.util.Log;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.SendBroadcast;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSource;
import okio.Okio;

public class RadioPlayer implements Keys {

    private static final ExecutorService executor = Executors.newSingleThreadExecutor();
    private static final OkHttpClient client = new OkHttpClient();

    public static void playRadioItem(String url, String name) {
        if (url == null || url.isEmpty() || name == null || name.isEmpty()) {
            Log.e("RadioPlayer", "Invalid input parameters");
            return;
        }

        Log.d("RadioPlayer", "playRadioItem: " + url + " " + name);
        executor.execute(() -> play(url, name));
    }

    private static void play(String url, String name) {
        String finalURL = fetchFinalURL(url);

        if (finalURL == null)
            return;

        if (!(RadioState.isPlaying() && finalURL.equals(RadioService.getUrl()))) {
            sendRadioServiceCommand(ACTION_RADIO_STOP);
            sendRadioServiceCommand(ACTION_RADIO_PLAY, finalURL, name);
        }
    }

    public static void sendRadioServiceCommand(String action, String url, String name) {
        Intent player = new Intent(MusicPlayerApp.getInstance(), RadioService.class);
        player.setAction(action);
        player.putExtra("url", url);
        player.putExtra("name", name);
        MusicPlayerApp.getInstance().startService(player);
    }

    public static void sendRadioServiceCommand(String action) {
        Intent player = new Intent(MusicPlayerApp.getInstance(), RadioService.class);
        player.setAction(action);
        MusicPlayerApp.getInstance().startService(player);
    }

    private static String fetchFinalURL(String url) {

        url = url.replace(" ", "%20");

        String[] finalURL = null;

        try {

            String userAgent = Keys.getRadioAppUserAgent();
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("User-Agent", userAgent)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                assert response.body() != null;
                try (BufferedSource source = Okio.buffer(response.body().source())) {
                    String data = source.readUtf8();
                    finalURL = data.split("\n"); // a tendance à doubler l'url
                }
            }

        } catch (SocketTimeoutException e) {
            SendBroadcast.sending(MusicPlayerApp.getInstance(), RADIO_ERROR_STATE, "error", "TimeoutException " + e);
        } catch (Exception e) {
            Log.e("RadioPlayer", "playRadioItem error: " + e);
        }

        return finalURL != null ? finalURL[0] : null;
    }

    public static void shutdownExecutor() {
        if (executor != null)
            executor.shutdown(); // Arrête l'executor
    }
}
