package org.oucho.musicplayer.ui.album;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.db.SongDbHelper;
import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.db.model.Artist;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter.OnItemClickListener;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter.OnItemLongClickListener;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.basefrag.CustomViewModel;
import org.oucho.musicplayer.ui.basefrag.SortOrder;
import org.oucho.musicplayer.ui.helpers.TitleSet;
import org.oucho.musicplayer.ui.playlist.CreatePlaylistDialog;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.song.SongLoader;
import org.oucho.musicplayer.ui.tags.AlbumTagFragment;
import org.oucho.musicplayer.ui.view.CustomGridLayoutManager;
import org.oucho.musicplayer.ui.view.bubulle.BubulleRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AlbumListFragment extends BaseFragment implements SensorEventListener {

    private Menu optionsMenu;
    private AlbumListAdapter albumAdapter;
    private ReloadView reloadReceiver;
    private String title;
    private String sortOrder;
    private boolean isReceiverRegistered = false;
    private BubulleRecyclerView recyclerView;
    private Artist selectedArtist;
    private List<Album> albumList;
    private CustomViewModel viewModel;
    private OnBackInvokedCallback backCallback;

    private long lastShakeTime;



    public static AlbumListFragment newInstance() {
        return new AlbumListFragment();
    }

    private final LoaderManager.LoaderCallbacks<List<Album>> albumLoaderCallbacks = new LoaderManager.LoaderCallbacks<>() {

        @NonNull
        @Override
        public Loader<List<Album>> onCreateLoader(int id, Bundle args) {
            Log.d("AlbumListFragment", "Creating loader for albums...");
            AlbumLoader loader;
            if (selectedArtist != null) {
                loader = new AlbumLoader(requireContext(), selectedArtist.getName());
                loader.setSortOrder(Preferences.getAlbumSort());
            } else {
                loader = new AlbumLoader(requireContext());
                loader.setSortOrder(Preferences.getAlbumSort());
            }

            return loader;
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<Album>> loader, List<Album> albums) {
            Log.d("AlbumListFragment", "Albums loaded: " + albums.size());
            albumAdapter.setData(albums);
            albumList = albums;
        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<Album>> loader) {
            Log.d("AlbumListFragment", "Loader reset.");
        }
    };


    /* *********************************************************************************************
     * Création de l'activité
     * ********************************************************************************************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null)
            selectedArtist = bundle.getParcelable("artist", Artist.class);

        title = requireContext().getString(R.string.albums);

        setSortOrder();

        reloadReceiver = new ReloadView();
        IntentFilter filter = new IntentFilter();
        filter.addAction("reload");
        filter.addAction(SET_TITLE);

        requireContext().registerReceiver(reloadReceiver, filter, Context.RECEIVER_EXPORTED);
        isReceiverRegistered = true;

        load();
    }

    /* *********************************************************************************************
     * Création de la vue
     * ********************************************************************************************/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int layout;

        if (selectedArtist != null)
            layout = R.layout.fragment_search_liste_album;
        else
            layout = R.layout.fragment_liste_album;

        View rootView = inflater.inflate(layout, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);

        // nombre de colonnes
        recyclerView.setLayoutManager(new CustomGridLayoutManager(requireContext(), 3));

        WindowMetrics windowMetrics = WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(requireActivity());
        final int width = windowMetrics.getBounds().width();
        int artworkSize = (width / 3);

        albumAdapter = new AlbumListAdapter(requireContext(), artworkSize);
        albumAdapter.setOnItemClickListener(albumItemClickListener);
        albumAdapter.setOnItemLongClickListener(albumItemLongClickListener);

        recyclerView.setAdapter(albumAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(CustomViewModel.class);

        setOptionsMenu();
    }


    private void setOptionsMenu() {
        requireActivity().addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menuInflater.inflate(R.menu.albumlist_sort_by, menu);
                optionsMenu = menu; // Stocker la référence au menu
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {

                int itemId = menuItem.getItemId();

                if (itemId == R.id.menu_sort_by_az) {
                    Preferences.edit(ALBUM_SORT_CRITERIA, SortOrder.AlbumSortOrder.ALBUM_TITLE_A_Z);
                    load();
                    sortOrder = "a-z";
                } else if (itemId == R.id.menu_sort_by_artist) {
                    Preferences.edit(ALBUM_SORT_CRITERIA, SortOrder.AlbumSortOrder.ALBUM_ARTIST_IGNORE_THE);
                    load();
                    sortOrder = requireContext().getString(R.string.title_sort_artist);
                } else if (itemId == R.id.menu_sort_by_year) {
                    Preferences.edit(ALBUM_SORT_CRITERIA, SortOrder.AlbumSortOrder.ALBUM_YEAR_DESC);
                    load();
                    sortOrder = requireContext().getString(R.string.title_sort_year);
                } else if (itemId == R.id.action_search) {

                    return handleSearchView();
                }
                setTitle();

                return true;
            }
        }, getViewLifecycleOwner(), Lifecycle.State.RESUMED);
    }


    @Override
    public void load() {
        Log.d("AlbumListFragment", "Loading albums...");
        LoaderManager.getInstance(this).restartLoader(0, null, albumLoaderCallbacks);
    }



    /* *********************************************************************************************
     * Menu des albums
     * ********************************************************************************************/

    private void showAlbumMenu(final int position, View v) {

        PopupMenu popup = new PopupMenu(requireContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.song_item, popup.getMenu()); // Assurez-vous d'avoir le bon menu
        final Album album = albumAdapter.getItem(position);

        popup.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();

            if (itemId == R.id.action_add_to_queue) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("album", album);
                LoaderManager.getInstance(this).restartLoader(1, bundle, loaderSong);
            } else if (itemId == R.id.action_add_to_playlist) {
                showPlaylistDialog(album);
            } else if (itemId == R.id.action_show_tag) {
                Fragment fragment = AlbumTagFragment.newInstance(album);
                FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom, R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                ft.replace(R.id.fragment_album_list_layout, fragment);
                ft.addToBackStack("AlbumTagFragment");
                ft.commit();
            }

            return true; // Retourne true pour indiquer que le clic a été géré
        });
        popup.show();
    }

    private void showPlaylistDialog(final Album album) {
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(requireActivity())) {
            List<String> playlists = dbHelper.getAllPlaylists();

            AlertDialog.Builder builder =new AlertDialog.Builder(requireActivity());
            builder.setTitle(R.string.add_to_playlist);

            CharSequence[] playlistNames = playlists.toArray(new CharSequence[0]);

            builder.setItems(playlistNames, (dialog, which) -> {
                String playlistName = playlists.get(which);
                addAlbumToPlaylist(playlistName, album);
            });

            builder.setPositiveButton(R.string.create_playlist, (dialog, which) -> showCreatePlaylistDialog(album));

            builder.setNegativeButton(android.R.string.cancel, null);builder.show();
        }
    }


    private void showCreatePlaylistDialog(final Album album) {
        CreatePlaylistDialog dialog = CreatePlaylistDialog.newInstance();
        dialog.setOnPlaylistCreatedListener(playlistName -> {
            addAlbumToPlaylist(playlistName, album);
            dialog.dismiss(); // Fermer explicitement la boîte de dialogue
        });
        dialog.show(getChildFragmentManager(), "create_playlist");
    }

    private void addAlbumToPlaylist(String playlistName, Album album) {
        SongDbHelper.addSongsToPlaylist(requireContext(), album, playlistName);
    }

    private final LoaderManager.LoaderCallbacks<List<Song>> loaderSong = new LoaderManager.LoaderCallbacks<>() {

        @NonNull
        @Override
        public Loader<List<Song>> onCreateLoader(int id, Bundle args) {
            assert args != null;
            Album album = args.getParcelable("album", Album.class);
            SongLoader loader = new SongLoader(getActivity());
            assert album != null;
            loader.setSelection(MediaStore.Audio.Media.ALBUM_ID + " = ?", new String[]{String.valueOf(album.getAlbumId())});
            loader.setSortOrder(MediaStore.Audio.Media.TRACK);
            return loader;
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<Song>> loader, List<Song> songList) {
            for (int i = 0; i < songList.size(); i++)
                ((MainActivity) requireActivity()).addToQueue(songList.get(i));
        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<Song>> loader) {}
    };


    private final OnItemClickListener albumItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position, View view) {
            Album album = albumAdapter.getItem(position);

            int viewId = view.getId();
            final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;

            view.performHapticFeedback(haptic);

            if (viewId == R.id.album_artwork || viewId == R.id.album_info) {
                Fragment fragment = AlbumContentFragment.newInstance(album);
                FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom, R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                ft.replace(R.id.fragment_album_list_layout, fragment);
                ft.addToBackStack("AlbumContentFragment");
                ft.commit();

                new Handler(Looper.getMainLooper()).postDelayed(() -> showOverflowMenu(false), 300);

            } else if (viewId == R.id.menu_button) {
                showAlbumMenu(position, view);
            }
        }
    };


    private final OnItemLongClickListener albumItemLongClickListener = new OnItemLongClickListener() {

        @Override
        public void onItemLongClick(int position, View view) {

            Song song = PlayerService.getCurrentSong();

            for (int i = 0; i < albumList.size(); i++) {
                if (albumList.get(i).getAlbumId() == song.getAlbumIdentifier())
                    recyclerView.smoothScrollToPosition( i );
            }
        }
    };


    private void showOverflowMenu(boolean showMenu){
        if(optionsMenu == null)
            return;

        optionsMenu.setGroupVisible(R.id.main_menu_group, showMenu);
    }

    /** @noinspection SameReturnValue*/
    private boolean handleSearchView() {

        MenuItem searchItem = optionsMenu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            View actionView = searchItem.getActionView();

            if (actionView instanceof SearchView)
                searchView = (SearchView) actionView;
        }

        assert searchView != null;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            final String getTri = Preferences.getAlbumSort();

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                effectuerRecherche(newText);
                return true;
            }


            @SuppressLint("NotifyDataSetChanged")
            private void effectuerRecherche(String query) {
                // Ici, vous devez filtrer votre liste d'albums en fonction de la requête 'query'
                List<Album> albumsFiltres = filtrerAlbums(query);

                // Mettez à jour l'adaptateur de votre RecyclerView avec la liste filtrée
                albumAdapter.setData(albumsFiltres);
                albumAdapter.notifyDataSetChanged(); // Notifiez l'adaptateur des changements
            }

            private List<Album> filtrerAlbums(String query) {

                List<Album> albumsFiltres = new ArrayList<>();

                for (Album album : albumList) { // listeTitre est votre liste originale d'albums

                    if (getTri.equals("REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC")) {

                        if (album.getArtist().toLowerCase().contains(query.toLowerCase()))
                            albumsFiltres.add(album);

                    } else {

                        if (album.getTitle().toLowerCase().contains(query.toLowerCase()))
                            albumsFiltres.add(album);
                    }
                }
                return albumsFiltres;
            }
        });

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(@NonNull MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(@NonNull MenuItem item) {
                return true;
            }
        });

        return true;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (isReceiverRegistered) {
            requireContext().unregisterReceiver(reloadReceiver);
            isReceiverRegistered = false;
        }

        SensorManager sensorManager = (SensorManager) requireContext().getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(this);

        AlbumLoader.clearArtistFilter();
        requireActivity().getOnBackInvokedDispatcher().
                unregisterOnBackInvokedCallback(backCallback);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Désenregistrez les écouteurs ou annulez les références ici
        if (reloadReceiver != null && isReceiverRegistered) {
            requireContext().unregisterReceiver(reloadReceiver);
            isReceiverRegistered = false;
        }
        // Annulez d'autres références similaires
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Annulez les références restantes ici
        if (backCallback != null) {
            requireActivity().getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback(backCallback);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (!isReceiverRegistered) {
            IntentFilter filter = new IntentFilter();
            filter.addAction("reload");
            filter.addAction(SET_TITLE);

            requireContext().registerReceiver(reloadReceiver, filter, Context.RECEIVER_EXPORTED);
            isReceiverRegistered = true;
        }

        load();

        MainActivity.setCurrentViewID(R.id.fragment_album_list_layout);
        setTitle();
        setBackInvokedCallback();

        SensorManager sensorManager= (SensorManager) requireContext().getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null)
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;

            if (mQueueView.getVisibility() == View.VISIBLE)
                queueManager.toggleQueueVisibility();
            else
                SendBroadcast.sending(requireContext(), BACK_VIEWPAGER, true);
        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }

    /* *********************************************************************************************
     * Titre
     * ********************************************************************************************/

    private void setSortOrder() {

        String getTri = Preferences.getAlbumSort();

        switch (getTri) {
            case "minyear DESC":
                sortOrder = requireContext().getString(R.string.title_sort_year);
                break;
            case "REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC":
                sortOrder = requireContext().getString(R.string.title_sort_artist);
                break;
            default:
                sortOrder = "a-z";
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            // Calcul de la force g
            float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if (gForce > SHAKE_SENSITIVITY) {
                long now = System.currentTimeMillis();
                if (lastShakeTime + SHAKE_DELAY_MS > now)
                    return; // Ignorer le shake si trop rapproché du précédent
                lastShakeTime = now;

                shuffleAlbumList();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @SuppressLint("NotifyDataSetChanged")
    private void shuffleAlbumList() {
        Collections.shuffle(albumList);
        albumAdapter.notifyDataSetChanged();

        // Animation de tremblement
        Animation shake = AnimationUtils.loadAnimation(requireContext(), R.anim.shake);
        recyclerView.startAnimation(shake);
    }


    private class ReloadView extends BroadcastReceiver {

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onReceive(final Context context, Intent intent) {

            String receiveIntent = intent.getAction();

            if ("reload".equals(receiveIntent)) {

                if (!viewModel.isAlbumFragmentVisible())
                    showOverflowMenu(true);

                load();
                Log.d("ReloadView", "ReloadView received");

                albumAdapter.notifyDataSetChanged();

            } else if (SET_TITLE.equals(receiveIntent)) {

                setTitle();
            }
        }
    }

    private void setTitle() {

        if (!viewModel.isAlbumFragmentVisible())
            TitleSet.setTitle((AppCompatActivity) getActivity(), title, sortOrder);
    }
}
