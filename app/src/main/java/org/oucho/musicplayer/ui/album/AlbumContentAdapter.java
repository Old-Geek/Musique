package org.oucho.musicplayer.ui.album;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.ui.basefrag.Adapter;

import java.util.Collections;
import java.util.List;
import java.util.Locale;


public class AlbumContentAdapter extends Adapter<AlbumContentAdapter.SongViewHolder> {


    private Context context;

    private List<Song> songList = Collections.emptyList();

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Song> data) {
        songList = data;
        notifyDataSetChanged();
    }

    public List<Song> getSongList() {
        return songList;
    }

    @Override
    public SongViewHolder onCreateViewHolderImpl(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_album_content_item, parent, false);
        return new SongViewHolder(itemView);
    }


    @Override
    public void onBindViewHolderImpl(SongViewHolder holder, int position) {
        Song song = getItem(position);

        holder.trackNumberTextView.setText(String.valueOf(song.getTrackPosition()));

        long secondes = song.getSongDuration() / 1000;
        String duration = String.format(Locale.getDefault(), "%02d:%02d", (secondes % 3600) / 60, (secondes % 60));
        holder.durationTextView.setText(duration);
        holder.titleTextView.setText(song.getSongTitle());

        long songId = PlayerService.getCurrentSong().getSongId();

        if (song.getSongId() == songId) {

            holder.durationTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.durationTextView.setTextSize(15);

            if (PlayerService.isPlaying()) {

                holder.playIconView.setImageResource(R.drawable.play_arrow_24px);
                holder.trackNumberTextView.setVisibility(View.INVISIBLE);
                holder.playIconView.setVisibility(View.VISIBLE);

            } else {

                holder.trackNumberTextView.setVisibility(View.INVISIBLE);
                holder.playIconView.setImageResource(R.drawable.pause_24px);
                holder.playIconView.setVisibility(View.VISIBLE);
            }

        } else {

            holder.durationTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
            holder.playIconView.setVisibility(View.INVISIBLE);
            holder.trackNumberTextView.setVisibility(View.VISIBLE);
        }

    }

    public Song getItem(int position) {
        return songList.get(position);
    }

    @Override
    public int getTotalItemCount() {
        return songList.size();
    }

    @Override
    public int getViewType() {
        return 0;
    }

    public class SongViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView durationTextView;
        private final TextView titleTextView;
        private final TextView trackNumberTextView;
        private final ImageView playIconView;

        SongViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            durationTextView = itemView.findViewById(R.id.time);
            titleTextView = itemView.findViewById(R.id.title);
            trackNumberTextView = itemView.findViewById(R.id.track_number);
            ImageButton menuButton = itemView.findViewById(R.id.buttonMenu);
            menuButton.setOnClickListener(this);
            playIconView = itemView.findViewById(R.id.play);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getBindingAdapterPosition();
            triggerOnItemClickListener(position, v);
        }
    }
}
