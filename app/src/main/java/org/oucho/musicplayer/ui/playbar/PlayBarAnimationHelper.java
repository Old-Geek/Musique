package org.oucho.musicplayer.ui.playbar;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;

public class PlayBarAnimationHelper {

    public static void animateMovement(View view, float startX, float endX, float startY, float endY) {
        TranslateAnimation translateAnimation = new TranslateAnimation(startX, endX, startY, endY);
        translateAnimation.setDuration(300);
        translateAnimation.setFillAfter(true);
        view.startAnimation(translateAnimation);
    }

    public static void animateOpacity(View view, float startAlpha, float endAlpha) {
        ObjectAnimator fadeAnimator = ObjectAnimator.ofFloat(view, "alpha", startAlpha, endAlpha);
        fadeAnimator.setDuration(300);
        fadeAnimator.setInterpolator(new AccelerateInterpolator());
        fadeAnimator.start();
    }
}

