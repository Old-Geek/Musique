package org.oucho.musicplayer.ui.basefrag;


import androidx.fragment.app.Fragment;

import org.oucho.musicplayer.Keys;

abstract public class BaseFragment extends Fragment implements Keys {

    abstract public void load();
}
