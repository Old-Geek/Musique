package org.oucho.musicplayer.ui.playlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.helpers.ItemTouchHelperCallback;
import org.oucho.musicplayer.ui.helpers.OnStartDragListener;
import org.oucho.musicplayer.ui.helpers.TitleSet;
import org.oucho.musicplayer.ui.queue.QueueManager;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class PlaylistContentFragment extends BaseFragment implements OnStartDragListener, SensorEventListener {

    private static final String ARG_PLAYLIST_NAME = "playlist_name";
    private static String playlistName;
    private PlaylistDbHelper dbHelper;
    private PlaylistContentAdapter playlistAdapter;
    private ItemTouchHelper itemTouchHelper;
    private RecyclerView recyclerView;
    FloatingActionButton buttonDelete;

    private long lastShakeTime;

    private OnBackInvokedCallback backCallback;

    public static PlaylistContentFragment newInstance(String playlistName) {
        PlaylistContentFragment fragment = new PlaylistContentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PLAYLIST_NAME, playlistName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            playlistName = getArguments().getString(ARG_PLAYLIST_NAME);

        dbHelper = new PlaylistDbHelper(getContext());
    }

    static String getPlaylistName() {
        return playlistName;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_playlist_content, container, false);
        recyclerView = rootView.findViewById(R.id.playlist_content_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        playlistAdapter = new PlaylistContentAdapter(getContext(), this); // Pass this as OnStartDragListener
        playlistAdapter.setOnItemClickListener(itemClickListener);
        recyclerView.setAdapter(playlistAdapter);

        ItemTouchHelperCallback callback = new ItemTouchHelperCallback(playlistAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        buttonDelete = rootView.findViewById(R.id.delete_playlist_item);
        buttonDelete.setOnClickListener(view -> {
            final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
            view.performHapticFeedback(haptic);
            if (callback.isItemViewSwipeEnabled()) {
                callback.setActiveDelete(false);
                buttonDelete.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.grey_600));
                buttonDelete.setImageResource(R.drawable.delete_24px);
            } else {
                callback.setActiveDelete(true);
                buttonDelete.setBackgroundTintList(ContextCompat.getColorStateList(requireContext(), R.color.blue_900_trans));
                buttonDelete.setImageResource(R.drawable.delete_forever_24px);
            }
        });

        loadPlaylistContent();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, false);
        loadPlaylistContent();

        TitleSet.setPlaylistContentTitle((AppCompatActivity) requireActivity(), "Playlist", playlistName);

        SensorManager sensorManager= (SensorManager) Objects.requireNonNull(requireContext()).getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null)
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

        setBackInvokedCallback();
    }

    @Override
    public void onPause() {
        super.onPause();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        SensorManager sensorManager = (SensorManager) requireContext().getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(this);

        TitleSet.setPlaylistTitle((AppCompatActivity) requireActivity(), "Playlist");

        ViewGroup parentLayout = (ViewGroup) requireView().getParent();
        FloatingActionButton button = parentLayout.findViewById(R.id.fab);
        button.setVisibility(View.VISIBLE);

        requireActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        requireActivity().getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback(backCallback);
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {// Gérer le geste de retour ici

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;

            if (mQueueView.getVisibility() == View.VISIBLE) {
                queueManager.toggleQueueVisibility();

            } else {
                requireActivity().getSupportFragmentManager()
                        .popBackStack("PlaylistContentFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }

    private void loadPlaylistContent() {
        List<Song> songs = dbHelper.getSongsFromPlaylist(getContext(), playlistName);
        playlistAdapter.updateData(songs);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
    }

    @Override
    public void load() {}


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            // Calcul de la force g
            float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if (gForce > SHAKE_SENSITIVITY) {
                long now = System.currentTimeMillis();
                if (lastShakeTime + SHAKE_DELAY_MS > now) {
                    return; // Ignorer le shake si trop rapproché du précédent
                }
                lastShakeTime = now;

                shufflePlaylist();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @SuppressLint("NotifyDataSetChanged")
    private void shufflePlaylist() {
        Collections.shuffle(playlistAdapter.getSongList());
        playlistAdapter.notifyDataSetChanged();

        // Animation de tremblement
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        recyclerView.startAnimation(shake);

        // Mettre à jour la base de données avec l'ordre aléatoire
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(getContext())) {
            dbHelper.updatePlaylistOrder(playlistName, playlistAdapter.getSongList());
        }
    }



    private final BaseAdapter.OnItemClickListener itemClickListener = new BaseAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(int position, View view) {
            MainActivity mainActivity = (MainActivity) requireActivity();
            mainActivity.onSongSelected(playlistAdapter.getSongList(), position);
            mainActivity.updateTrackInfo();
        }
    };


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

}