package org.oucho.musicplayer.ui.song;


import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.ui.basefrag.Adapter;
import org.oucho.musicplayer.ui.view.bubulle.FastScrollBubble;
import org.oucho.picasso.Picasso;

import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


/** @noinspection ClassEscapesDefinedScope */
public class SongListAdapter extends Adapter<SongListAdapter.SongViewHolder> implements FastScrollBubble.SectionIndexer, Keys {

    private final Context context;
    private String sortingCriteria;
    private List<Song> songList = Collections.emptyList();

    private final int artSize;

    public SongListAdapter(Context context) {
        this.context = context;
        this.artSize = this.context.getResources().getDimensionPixelSize(R.dimen.fragment_song_list_item_art_size);
    }


    @SuppressLint("NotifyDataSetChanged")
    public void updateData(List<Song> data) {
        songList = data;
        notifyDataSetChanged();
    }

    public List<Song> getSongList() {
        return songList;
    }

    @Override
    public SongViewHolder onCreateViewHolderImpl(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fragment_song_item, parent, false);

        return new SongViewHolder(itemView);
    }

    @Override
    public String getSectionText(int position) {
        Song song = songList.get(position);

        sortingCriteria = Preferences.getSongSort();


        return switch (sortingCriteria) {
            case "REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC" -> {
                String toto = String.valueOf(song.getArtistName()).replaceFirst("The ", "");
                yield stripAccents(String.valueOf(toto.toUpperCase().charAt(0)));
            }
            case "year DESC" -> String.valueOf(song.getReleaseYear());
            case "album" -> {
                String toto = String.valueOf(song.getAlbumTitle());
                yield stripAccents(String.valueOf(toto.toUpperCase().charAt(0)));
            }
            default -> {
                String toto = String.valueOf(song.getSongTitle());
                yield stripAccents(String.valueOf(toto.toUpperCase().charAt(0)));
            }
        };
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    @Override
    public void onBindViewHolderImpl(SongViewHolder holder, int position) {
        Song song = getItem(position);

        sortingCriteria = Preferences.getSongSort();

        holder.titleView.setText(song.getSongTitle());
        holder.titleView.setTextColor(ContextCompat.getColor(context, R.color.grey_900));
        holder.titleView.setTextSize(15);
        holder.titleView.setTypeface(null, Typeface.NORMAL);

        holder.artistView.setText(song.getArtistName());
        holder.artistView.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
        holder.artistView.setTextSize(15);
        holder.artistView.setTypeface(null, Typeface.NORMAL);

        String duration = "(" + msToText(song.getSongDuration()) + ")";

        holder.durationView.setText(duration);
        holder.durationView.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
        holder.durationView.setVisibility(View.VISIBLE);

        holder.yearView.setVisibility(View.GONE);
        holder.albumView.setVisibility(View.GONE);


        if ("REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC".equals(sortingCriteria)) {

            holder.artistView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.artistView.setTextSize(16);
            holder.artistView.setTypeface(null, Typeface.BOLD);

        } else if ("year DESC".equals(sortingCriteria)) {

            holder.durationView.setVisibility(View.GONE);

            holder.yearView.setText(String.valueOf(song.getReleaseYear()));
            holder.yearView.setVisibility(View.VISIBLE);

        } else if ("album".equals(sortingCriteria)){

            holder.albumView.setText(song.getAlbumTitle());
            holder.albumView.setVisibility(View.VISIBLE);

            holder.durationView.setVisibility(View.GONE);

        } else {

            holder.titleView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.titleView.setTextSize(16);
            holder.titleView.setTypeface(null, Typeface.BOLD);
        }

        Uri uri = ContentUris.withAppendedId(ALBUM_ARTWORK_URI, song.getAlbumIdentifier());
        Picasso.get()
                .load(uri)
                .resize(artSize, artSize)
                .centerCrop()
                .into(holder.artworkView);
    }

    public Song getItem(int position) {
        return songList.get(position);
    }

    @Override
    public int getTotalItemCount() {
        return songList.size();
    }

    @Override
    public int getViewType() {
        return 0;
    }


    class SongViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView titleView;
        private final TextView artistView;
        private final TextView durationView;
        private final TextView albumView;
        private final TextView yearView;


        private final ImageView artworkView;

        SongViewHolder(View itemView) {
            super(itemView);
            titleView = itemView.findViewById(R.id.title);
            artistView = itemView.findViewById(R.id.artist);
            durationView = itemView.findViewById(R.id.duration);
            albumView = itemView.findViewById(R.id.album);
            yearView = itemView.findViewById(R.id.year);


            artworkView = itemView.findViewById(R.id.artwork);
            itemView.setOnClickListener(this);

            ImageButton menuButton = itemView.findViewById(R.id.menu_button);
            menuButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getBindingAdapterPosition();
            triggerOnItemClickListener(position, v);
        }
    }

    private String msToText(int msec) {
        return String.format(Locale.getDefault(), "%d:%02d", msec / 60000, (msec % 60000) / 1000);
    }
}
