package org.oucho.musicplayer.ui.timer;

public interface TimerListener {
    void onTimerUpdate(String time);
    void onTimerFinish();
}