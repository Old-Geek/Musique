package org.oucho.musicplayer.ui.song;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.MediaStore;

import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.ui.basefrag.BaseLoader;

import java.util.ArrayList;
import java.util.List;


public class SongLoader extends BaseLoader<List<Song>> {


    private static final String[] SONG_PROJECTION = {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.ALBUM_ID,
            MediaStore.Audio.Media.ARTIST_ID,
            MediaStore.Audio.Media.TRACK,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.YEAR,
            MediaStore.Audio.Media.DATA};

    public SongLoader(Context context) {
        super(context);
    }

    @Override
    public List<Song> loadInBackground() {
        List<Song> songList = new ArrayList<>();

        Cursor songCursor = fetchSongCursor();

        if (songCursor != null && songCursor.moveToFirst()) {
            int idColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int artistColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int albumIdColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int trackColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
            int durationColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int yearColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
            int pathColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA);

            do {
                long id = songCursor.getLong(idColumn);
                String title = songCursor.getString(titleColumn);
                String artist = songCursor.getString(artistColumn);
                String album = songCursor.getString(albumColumn);

                long albumId = songCursor.getLong(albumIdColumn);
                int trackNumber = songCursor.getInt(trackColumn);
                int duration = songCursor.getInt(durationColumn);
                int year = songCursor.getInt(yearColumn);

                String path = songCursor.getString(pathColumn);

                String trackNumberStr = String.valueOf(trackNumber);

                if (trackNumber >= 1000 && trackNumber < 10000) {
                    trackNumber = Integer.parseInt(trackNumberStr.substring(1)); // remove first char
                } else if (trackNumber >= 10000 && trackNumber < 100000) {
                    trackNumber = Integer.parseInt(trackNumberStr.substring(2)); // remove first 2 chars
                }

                // Avoid integrating genre search, too slow to load
                songList.add(new Song(id, title, artist, album, albumId, trackNumber, duration, year, path));
            } while (songCursor.moveToNext());
        }

        if (songCursor != null) {
            songCursor.close();
        }

        return songList;
    }

    private Cursor fetchSongCursor() {

        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String selection = getSelectionClause();
        String[] selectionArgs = getSelectionArguments();

        selection = DatabaseUtils.concatenateWhere(selection, MediaStore.Audio.Media.IS_MUSIC + " = 1");

        return fetchCursor(musicUri, SONG_PROJECTION, selection, selectionArgs);
    }

    public static Song fetchFirstSong(ContentResolver contentResolver) {
        Song song = null;
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        try (Cursor cursor = contentResolver.query(uri, SONG_PROJECTION, selection, null, sortOrder)) {
            if (cursor != null && cursor.moveToFirst()) {
                long id = cursor.getLong(0);
                String title = cursor.getString(1);
                String artist = cursor.getString(2);
                String album = cursor.getString(3);
                long albumId = cursor.getLong(4);
                int trackNumber = cursor.getInt(5);
                int duration = cursor.getInt(6);
                int year = cursor.getInt(7);
                String path = cursor.getString(9);

                song = new Song(id, title, artist, album, albumId, trackNumber, duration, year, path);
            }
        }

        return song;
    }
}