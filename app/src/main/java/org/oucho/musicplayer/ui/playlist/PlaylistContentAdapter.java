package org.oucho.musicplayer.ui.playlist;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.helpers.ItemTouchHelperAdapter;
import org.oucho.musicplayer.ui.helpers.OnStartDragListener;
import org.oucho.picasso.Picasso;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

/** @noinspection ClassEscapesDefinedScope */
public class PlaylistContentAdapter extends BaseAdapter<PlaylistContentAdapter.SongViewHolder> implements Keys, ItemTouchHelperAdapter {
    private final Context context;
    private List<Song> songList = Collections.emptyList();
    private final OnStartDragListener dragStartListener;

    public PlaylistContentAdapter(Context context, OnStartDragListener dragStartListener) {
        this.context = context;
        this.dragStartListener = dragStartListener;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void updateData(List<Song> data) {
        songList = data;
        notifyDataSetChanged();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition)
            for (int i = fromPosition; i < toPosition; i++)
                Collections.swap(songList, i, i + 1);
        else
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(songList, i, i - 1);}

        notifyItemMoved(fromPosition, toPosition);

        // Mettre à jour la base de données après le déplacement
        updatePlaylistInDatabase();
    }

    @Override
    public void onItemDismiss(int position) {
        Song song = songList.remove(position);
        notifyItemRemoved(position);

        // Supprimer le titre de la base de données
        String playlistName = PlaylistContentFragment.getPlaylistName();
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(context)) {
            dbHelper.removeSongFromPlaylist(playlistName, song.getSongId());
        }
    }

    private void updatePlaylistInDatabase() {
        String playlistName = PlaylistContentFragment.getPlaylistName();
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(context)) {
            dbHelper.updatePlaylistOrder(playlistName, songList);
        }
    }

    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_playlist_content_item, parent, false);
        return new SongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder viewHolder, int position) {
        Song song = songList.get(position);
        viewHolder.titleTextView.setText(song.getSongTitle());
        viewHolder.artistTextView.setText(song.getArtistName());

        String time = convertMillisecondsToMS(song.getSongDuration());
        viewHolder.timeTextView.setText(time);

        int artSize = context.getResources().getDimensionPixelSize(R.dimen.fragment_song_list_item_art_size);
        Uri uri = ContentUris.withAppendedId(ALBUM_ARTWORK_URI, song.getAlbumIdentifier());
        Picasso.get()
                .load(uri)
                .resize(artSize, artSize)
                .centerCrop()
                .into(viewHolder.artworkImageView);
    }

    public static String convertMillisecondsToMS(long totalMilliseconds) {
        long totalSeconds = totalMilliseconds / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.getDefault(), "%d:%02d", minutes, seconds);
    }


    @Override
    public int getItemCount() {
        return songList.size();
    }

    public List<Song> getSongList() {
        return songList;
    }


    class SongViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ImageView artworkImageView;
        final TextView titleTextView;
        final TextView artistTextView;
        final TextView timeTextView;

        @SuppressLint("ClickableViewAccessibility")
        SongViewHolder(View itemView) {
            super(itemView);
            artworkImageView = itemView.findViewById(R.id.sartwork);
            titleTextView = itemView.findViewById(R.id.stitle);
            artistTextView = itemView.findViewById(R.id.sartist);
            timeTextView = itemView.findViewById(R.id.playlist_content_time);

            LinearLayout layout = itemView.findViewById(R.id.playlist_content_item);
            layout.setOnClickListener(this);

            ImageView dragHandle = itemView.findViewById(R.id.drag_handle); // Add a drag handle view in your item layout
            dragHandle.setOnTouchListener((v, event) -> {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN)
                    dragStartListener.onStartDrag(SongViewHolder.this);

                return false;
            });
        }

        @Override
        public void onClick(View view) {
            int position = getBindingAdapterPosition();
            triggerOnItemClickListener(position, view);
        }

    }
}
