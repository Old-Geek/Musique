package org.oucho.musicplayer.ui.basefrag;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

abstract public class BaseLoader<D> extends AsyncTaskLoader<D> {

    private D loadedData;

    private String selectionClause;
    private String[] selectionArguments;
    private String sortOrder = null;

    protected BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if (loadedData != null) {
            deliverResult(loadedData);
        }

        if (takeContentChanged() || loadedData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        loadedData = null;
        onStopLoading();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    public void deliverResult(D data) {
        if (!isReset()) {
            super.deliverResult(data);
        }
    }

    public void setSelection(String selectionClause, String[] selectionArguments) {
        this.selectionClause = selectionClause;
        setSelectionArguments(selectionArguments);
    }

    private void setSelectionArguments(String[] arguments) {
        selectionArguments = arguments;
    }

    protected String getSelectionClause() {
        return selectionClause;
    }

    protected String[] getSelectionArguments() {
        return selectionArguments;
    }

    @Nullable
    private Cursor fetchCursor(Uri contentUri, String[] projection, String selection, String[] selectionArgs, String orderBy) {
        return getContext().getContentResolver().query(contentUri, projection, selection, selectionArgs, orderBy);
    }

    @Nullable
    protected Cursor fetchCursor(Uri contentUri, String[] projection, String selection, String[] selectionArgs) {
        return fetchCursor(contentUri, projection, selection, selectionArgs, sortOrder);
    }

    public void setSortOrder(String orderBy) {
        sortOrder = orderBy;
    }
}
