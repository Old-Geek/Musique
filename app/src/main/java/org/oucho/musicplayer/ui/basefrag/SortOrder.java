package org.oucho.musicplayer.ui.basefrag;

import android.provider.MediaStore;

public final class SortOrder {

    public interface AlbumSortOrder {
        // Titre
        String ALBUM_TITLE_A_Z = MediaStore.Audio.Albums.DEFAULT_SORT_ORDER;

        // Ne pas prendre en compte "The" lors du tri des albums par artiste
        String ALBUM_ARTIST_IGNORE_THE = String.format(
                "REPLACE ('<BEGIN>' || %s, '<BEGIN>The ', '<BEGIN>') ASC, %s ASC",
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.FIRST_YEAR
        );

        String ALBUM_YEAR_DESC = MediaStore.Audio.Albums.FIRST_YEAR + " DESC";
    }

    public interface SongSortOrder {
        // Titre
        String SONG_TITLE_A_Z = MediaStore.Audio.Media.TITLE;

        String SONG_ARTIST_IGNORE_THE = String.format(
                "REPLACE ('<BEGIN>' || %s, '<BEGIN>The ', '<BEGIN>')",
                MediaStore.Audio.Media.ARTIST
        );

        String SONG_ALBUM = MediaStore.Audio.Media.ALBUM;
        String SONG_YEAR_DESC = MediaStore.Audio.Media.YEAR + " DESC";
    }
}

