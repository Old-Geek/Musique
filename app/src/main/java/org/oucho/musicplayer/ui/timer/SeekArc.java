package org.oucho.musicplayer.ui.timer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import org.oucho.musicplayer.R;


public class SeekArc extends View {

	private static final int INVALID_PROGRESS_VALUE = -1;

    private Drawable thumbDrawable;

	private int maxProgress = 120;
	private int currentProgress = 0;
	private int progressWidth = 4;
	private int arcWidth = 2;
	private int startAngle = 0;
	private int sweepAngle = 360;
	private int rotation = 0;

	private boolean roundedEdges = false;
	private boolean touchInside = true;
	private boolean clockwise = true;
	private boolean enabled = true;

	private int arcRadius = 0;
	private float progressSweep = 0;
	private final RectF arcRect = new RectF();
	private Paint arcPaint;
	private Paint progressPaint;
	private int translateX;
	private int translateY;
	private int thumbXPos;
	private int thumbYPos;
    private float touchIgnoreRadius;
	private OnSeekArcChangeListener onSeekArcChangeListener;

	public interface OnSeekArcChangeListener {

		void onProgressChanged(int progress);

		@SuppressWarnings("EmptyMethod")
		void onStartTrackingTouch();

		@SuppressWarnings("EmptyMethod")
		void onStopTrackingTouch();
	}

	public SeekArc(Context context) {
		super(context);
		init(context, null, 0);
	}

	public SeekArc(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, R.attr.seekArcStyle);
	}

	public SeekArc(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {

		float density = context.getResources().getDisplayMetrics().density;

		int arcColor = ContextCompat.getColor(context, R.color.grey_300);
		int progressColor0 = ContextCompat.getColor(context, R.color.colorAccent);
		int thumbHalfheight;
		int thumbHalfWidth;
		thumbDrawable = ContextCompat.getDrawable(context, R.drawable.seek_arc_control_selector);

		progressWidth = (int) (progressWidth * density);


		if (attrs != null) {
			try (TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekArc, defStyle, 0)) {
				Drawable thumb = a.getDrawable(R.styleable.SeekArc_thumb);
				if (thumb != null)
					thumbDrawable = thumb;

				thumbHalfheight = thumbDrawable.getIntrinsicHeight() / 2;
				thumbHalfWidth = thumbDrawable.getIntrinsicWidth() / 2;
				thumbDrawable.setBounds(-thumbHalfWidth, -thumbHalfheight, thumbHalfWidth, thumbHalfheight);

				maxProgress = a.getInteger(R.styleable.SeekArc_max, maxProgress);
				currentProgress = a.getInteger(R.styleable.SeekArc_progress, currentProgress);
				progressWidth = (int) a.getDimension(R.styleable.SeekArc_progressWidth, progressWidth);
				arcWidth = (int) a.getDimension(R.styleable.SeekArc_arcWidth, arcWidth);
				startAngle = a.getInt(R.styleable.SeekArc_startAngle, startAngle);
				sweepAngle = a.getInt(R.styleable.SeekArc_sweepAngle, sweepAngle);
				rotation = a.getInt(R.styleable.SeekArc_rotation, rotation);
				roundedEdges = a.getBoolean(R.styleable.SeekArc_roundEdges, roundedEdges);
				touchInside = a.getBoolean(R.styleable.SeekArc_touchInside, touchInside);
				clockwise = a.getBoolean(R.styleable.SeekArc_clockwise, clockwise);
				enabled = a.getBoolean(R.styleable.SeekArc_enabled, enabled);

				arcColor = a.getColor(R.styleable.SeekArc_arcColor, arcColor);
				progressColor0 = a.getColor(R.styleable.SeekArc_progressColor0, progressColor0);
			}
		}

		currentProgress = Math.min(currentProgress, maxProgress);
		currentProgress = Math.max(currentProgress, 0);

		sweepAngle = Math.min(sweepAngle, 360);
		sweepAngle = Math.max(sweepAngle, 0);

		progressSweep = (float) currentProgress / maxProgress * sweepAngle;

		startAngle = (startAngle > 360) ? 0 : startAngle;
		startAngle = Math.max(startAngle, 0);

		arcPaint = new Paint();
		arcPaint.setColor(arcColor);
		arcPaint.setAntiAlias(true);
		arcPaint.setStyle(Paint.Style.STROKE);
		arcPaint.setStrokeWidth(arcWidth);

		progressPaint = new Paint();
		progressPaint.setColor(progressColor0);
		progressPaint.setAntiAlias(true);
		progressPaint.setStyle(Paint.Style.STROKE);
		progressPaint.setStrokeWidth(progressWidth);

		if (roundedEdges) {
			arcPaint.setStrokeCap(Paint.Cap.ROUND);
			progressPaint.setStrokeCap(Paint.Cap.ROUND);
		}
	}

	@Override
	protected void onDraw(@NonNull Canvas canvas) {
		if(!clockwise)
			canvas.scale(-1, 1, arcRect.centerX(), arcRect.centerY() );

        int mAngleOffset = -90;
        final int arcStart = startAngle + mAngleOffset + rotation;
		final int arcSweep = sweepAngle;
		canvas.drawArc(arcRect, arcStart, arcSweep, false, arcPaint);
		canvas.drawArc(arcRect, arcStart, progressSweep, false, progressPaint);

		if (enabled) {
			canvas.translate(translateX - thumbXPos, translateY - thumbYPos);
			thumbDrawable.draw(canvas);
		}
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
		final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		final int min = Math.min(width, height);
		float top;
		float left;
		int arcDiameter;

		translateX = (int) (width * 0.5f);
		translateY = (int) (height * 0.5f);
		
		arcDiameter = min - getPaddingLeft();
		arcRadius = arcDiameter / 2;
		top = (float) height / 2 - ((float) arcDiameter / 2);
		left = (float) width / 2 - ((float) arcDiameter / 2);
		arcRect.set(left, top, left + arcDiameter, top + arcDiameter);
	
		int arcStart = (int) progressSweep + startAngle + rotation + 90;
		thumbXPos = (int) (arcRadius * Math.cos(Math.toRadians(arcStart)));
		thumbYPos = (int) (arcRadius * Math.sin(Math.toRadians(arcStart)));
		
		setTouchInSide(touchInside);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@SuppressLint("ClickableViewAccessibility")
    @Override
	public boolean onTouchEvent(MotionEvent event) {
		if (enabled) {
			this.getParent().requestDisallowInterceptTouchEvent(true);

			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					onStartTrackingTouch();
					updateOnTouch(event);
					break;

				case MotionEvent.ACTION_MOVE:
					updateOnTouch(event);
					break;

				case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    onStopTrackingTouch();
					setPressed(false);
					this.getParent().requestDisallowInterceptTouchEvent(false);
					break;

                default:
					break;
			}
			return true;
		}
		return false;
	}

	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();
		if (thumbDrawable != null && thumbDrawable.isStateful()) {
			int[] state = getDrawableState();
			thumbDrawable.setState(state);
		}
		invalidate();
	}

	private void onStartTrackingTouch() {
		if (onSeekArcChangeListener != null)
			onSeekArcChangeListener.onStartTrackingTouch();
	}

	private void onStopTrackingTouch() {
		if (onSeekArcChangeListener != null)
			onSeekArcChangeListener.onStopTrackingTouch();
	}

	private void updateOnTouch(MotionEvent event) {
		boolean ignoreTouch = ignoreTouch(event.getX(), event.getY());
		if (ignoreTouch)
			return;
		setPressed(true);
        double mTouchAngle = getTouchDegrees(event.getX(), event.getY());
		int progress = getProgressForAngle(mTouchAngle);
		onProgressRefresh(progress);
	}

	private boolean ignoreTouch(float xPos, float yPos) {
		boolean ignore = false;
		float x = xPos - translateX;
		float y = yPos - translateY;

		float touchRadius = (float) Math.sqrt(((x * x) + (y * y)));
		if (touchRadius < touchIgnoreRadius)
			ignore = true;

		return ignore;
	}

	private double getTouchDegrees(float xPos, float yPos) {
		float x = xPos - translateX;
		float y = yPos - translateY;
		//invert the x-coord if we are rotating anti-clockwise
		x= (clockwise) ? x:-x;
		// convert to arc Angle
		double angle = Math.toDegrees(Math.atan2(y, x) + (Math.PI / 2)
				- Math.toRadians(rotation));
		if (angle < 0)
			angle = 360 + angle;

		angle -= startAngle;
		return angle;
	}

	private int getProgressForAngle(double angle) {
		int touchProgress = (int) Math.round(valuePerDegree() * angle);

		touchProgress = (touchProgress < 0) ? INVALID_PROGRESS_VALUE : touchProgress;
		touchProgress = (touchProgress > maxProgress) ? INVALID_PROGRESS_VALUE : touchProgress;
		return touchProgress;
	}

	private float valuePerDegree() {
		return (float) maxProgress / sweepAngle;
	}

	private void onProgressRefresh(int progress) {
		updateProgress(progress);
	}

	private void updateThumbPosition() {
		int thumbAngle = (int) (startAngle + progressSweep + rotation + 90);
		thumbXPos = (int) (arcRadius * Math.cos(Math.toRadians(thumbAngle)));
		thumbYPos = (int) (arcRadius * Math.sin(Math.toRadians(thumbAngle)));
	}
	
	private void updateProgress(int progress) {

		if (progress == INVALID_PROGRESS_VALUE)
			return;

		progress = Math.min(progress, maxProgress);
		progress = Math.max(progress, 0);
		currentProgress = progress;

		if (onSeekArcChangeListener != null)
			onSeekArcChangeListener.onProgressChanged(progress);

		progressSweep = (float) progress / maxProgress * sweepAngle;

		updateThumbPosition();

		invalidate();
	}

	public void setOnSeekArcChangeListener(OnSeekArcChangeListener l) {
		onSeekArcChangeListener = l;
	}

    public int getProgress() {
		return currentProgress;
	}

    private void setTouchInSide(boolean isEnabled) {

		int thumbHalfheight = thumbDrawable.getIntrinsicHeight() / 2;
		int thumbHalfWidth = thumbDrawable.getIntrinsicWidth() / 2;

		touchInside = isEnabled;

		if (touchInside)
			touchIgnoreRadius = (float) arcRadius / 4;
		else
			touchIgnoreRadius = arcRadius - Math.min(thumbHalfWidth, thumbHalfheight);
	}

    public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
