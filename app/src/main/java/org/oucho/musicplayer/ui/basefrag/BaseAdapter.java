package org.oucho.musicplayer.ui.basefrag;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


public abstract class BaseAdapter<V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {

    private OnItemClickListener itemClickListener;
    private OnItemLongClickListener itemLongClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        itemClickListener = listener;
    }

    protected void triggerOnItemClickListener(int position, View view) {
        if(itemClickListener != null)
            itemClickListener.onItemClick(position, view);
    }

    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }


    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        itemLongClickListener = listener;
    }

    protected void triggerOnItemLongClickListener(int position, View view) {
        if(itemLongClickListener != null)
            itemLongClickListener.onItemLongClick(position, view);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(int position, View view);
    }
}
