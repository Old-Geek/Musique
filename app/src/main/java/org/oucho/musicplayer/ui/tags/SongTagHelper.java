package org.oucho.musicplayer.ui.tags;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SongTagHelper {

    private final BaseFragment fragment;
    private final Context context;
    private Song currentSong;
    private int currentPosition;
    private final List<Song> songList;

    private final Map<FieldKey, EditText> fieldMap = new HashMap<>();
    private final Map<FieldKey, String> tagValues = new HashMap<>();

    private Button button_previous;
    private Button button_next;
    private AlertDialog currentDialog; // Référence au dialogue actuel

    @SuppressLint("InflateParams")
    public SongTagHelper(BaseFragment fragment, Song song, int position, List<Song> songs) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        this.currentSong = song;
        this.currentPosition = position;
        this.songList = songs;
        showTagsDialog(); // Initialiser le dialogue dès la création
    }

    private void initializeFieldMap(View dialogView) {
        fieldMap.put(FieldKey.TITLE, dialogView.findViewById(R.id.editSongTextTitle));
        fieldMap.put(FieldKey.ARTIST, dialogView.findViewById(R.id.editSongTextArtist));
        fieldMap.put(FieldKey.ALBUM_ARTIST, dialogView.findViewById(R.id.editSongTextArtistAlbum));
        fieldMap.put(FieldKey.ALBUM, dialogView.findViewById(R.id.editSongTextAlbum));
        fieldMap.put(FieldKey.YEAR, dialogView.findViewById(R.id.editSongTextYear));
        fieldMap.put(FieldKey.TRACK, dialogView.findViewById(R.id.editSongTextTrack));
        fieldMap.put(FieldKey.TRACK_TOTAL, dialogView.findViewById(R.id.editSongTextTrackOf));
        fieldMap.put(FieldKey.DISC_NO, dialogView.findViewById(R.id.editSongTextDisc));
        fieldMap.put(FieldKey.DISC_TOTAL, dialogView.findViewById(R.id.editSongTextDiscOf));
        fieldMap.put(FieldKey.GENRE, dialogView.findViewById(R.id.editSongTextGenre));
        fieldMap.put(FieldKey.COMMENT, dialogView.findViewById(R.id.editSongTextComment));
    }

    private void initializeButtons(View dialogView) {
        button_previous = dialogView.findViewById(R.id.tag_song_previous);
        button_next = dialogView.findViewById(R.id.tag_song_next);
        Button save = dialogView.findViewById(R.id.tag_song_save);

        button_previous.setOnClickListener(v -> {
            if (currentPosition > 0) {
                saveTags();
                currentPosition--;
                currentSong = songList.get(currentPosition); // Mettre à jour la chanson actuelle
                updateButtonState();
                updateTags();
            }
        });

        button_next.setOnClickListener(v -> {
            if (currentPosition < songList.size() - 1) {
                saveTags();
                currentPosition++;
                currentSong = songList.get(currentPosition); // Mettre à jour la chanson actuelle
                updateButtonState();
                updateTags();
            }
        });

        save.setOnClickListener(v -> {
            saveTags();
            currentDialog.dismiss();
        });

        updateButtonState();
    }

    private void updateButtonState() {
        if (button_previous != null && button_next != null) {
            button_previous.setEnabled(currentPosition > 0);
            button_next.setEnabled(currentPosition < songList.size() - 1);
        }
    }

    public void showTags() {
        updateTags(); // Mettre à jour les tags sans recréer le dialogue
    }

    private void showTagsDialog() {
        if (currentSong.getFilePath() == null) {
            return;
        }

        // Recréer dialogView pour éviter le conflit de parent
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_tag_song, null);
        initializeFieldMap(dialogView);
        initializeButtons(dialogView);

        // Mettre à jour les valeurs des champs après l'initialisation
        updateTags();

        if (currentDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setView(dialogView);
            currentDialog = builder.create();
            currentDialog.show();
        }
    }

    private void updateTags() {
        try {
            AudioFile audioFile = AudioFileIO.read(new File(currentSong.getFilePath()));
            Tag tag = audioFile.getTag();
            if (tag != null) {
                updateTagValues(tag);
            }
        } catch (Exception e) {
            Log.e("Erreur lecture fichier audio", Objects.requireNonNull(e.getMessage()));
        }

        // Mettre à jour les valeurs des champs
        setDefaultValues();
    }

    private void updateTagValues(Tag tag) {
        tagValues.put(FieldKey.TITLE, tag.getFirst(FieldKey.TITLE));
        tagValues.put(FieldKey.ARTIST, tag.getFirst(FieldKey.ARTIST));
        tagValues.put(FieldKey.ALBUM_ARTIST, tag.getFirst(FieldKey.ALBUM_ARTIST));
        tagValues.put(FieldKey.ALBUM, tag.getFirst(FieldKey.ALBUM));
        tagValues.put(FieldKey.YEAR, tag.getFirst(FieldKey.YEAR));
        tagValues.put(FieldKey.TRACK, tag.getFirst(FieldKey.TRACK));
        tagValues.put(FieldKey.TRACK_TOTAL, tag.getFirst(FieldKey.TRACK_TOTAL));
        tagValues.put(FieldKey.DISC_NO, tag.getFirst(FieldKey.DISC_NO));
        tagValues.put(FieldKey.DISC_TOTAL, tag.getFirst(FieldKey.DISC_TOTAL));
        tagValues.put(FieldKey.GENRE, tag.getFirst(FieldKey.GENRE));
        tagValues.put(FieldKey.COMMENT, tag.getFirst(FieldKey.COMMENT));
    }

    private void setDefaultValues() {
        for (Map.Entry<FieldKey, EditText> entry : fieldMap.entrySet()) {
            entry.getValue().setText(tagValues.getOrDefault(entry.getKey(), ""));
        }
    }

    private void saveTags() {
        try {
            AudioFile audioFile = AudioFileIO.read(new File(currentSong.getFilePath()));
            Tag tag = audioFile.getTagOrCreateAndSetDefault();

            for (Map.Entry<FieldKey, EditText> entry : fieldMap.entrySet()) {
                String newValue = entry.getValue().getText().toString();
                if (newValue.isEmpty()) {
                    tag.deleteField(entry.getKey());
                } else if (!Objects.equals(tagValues.get(entry.getKey()), newValue)) {
                    tag.setField(entry.getKey(), newValue);
                }
            }

            audioFile.commit();
            scanFile(currentSong.getFilePath());

        } catch (Exception e) {
            Log.e("Jaudiotagger", "Erreur sauvegarde tags: " + e.getMessage());
        }

        new Handler(Looper.getMainLooper()).postDelayed(fragment::load, 500);
    }

    private void scanFile(String path) {
        MediaScannerConnection.scanFile(context, new String[]{path}, null,
                (path1, uri) -> {
                    Log.i("MediaScanner", "Scanned " + path1 + ":");
                    Log.i("MediaScanner", "-> uri=" + uri);
                });
    }
}
