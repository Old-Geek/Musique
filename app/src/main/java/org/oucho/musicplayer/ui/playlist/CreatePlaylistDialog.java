package org.oucho.musicplayer.ui.playlist;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;


public class CreatePlaylistDialog extends DialogFragment {

    private OnPlaylistCreatedListener listener;

    public static CreatePlaylistDialog newInstance() {
        return new CreatePlaylistDialog();
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View dialogLayout = getLayoutInflater().inflate(R.layout.dialog_playlist,
                new LinearLayout(getActivity()), false);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity())
                .setTitle(R.string.create_playlist)
                .setView(dialogLayout)
                .setPositiveButton(android.R.string.ok,
                        (dialog, which) -> {
                            EditText playlistNameEditText = dialogLayout.findViewById(R.id.playlist_name);
                            playlistNameEditText.requestFocus();

                            InputMethodManager inputMethodManager = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                            if (inputMethodManager != null)
                                inputMethodManager.showSoftInput(playlistNameEditText, InputMethodManager.SHOW_IMPLICIT);

                            try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(requireContext())) {
                                String playlistName = playlistNameEditText.getText().toString();
                                dbHelper.addPlaylist(playlistName);

                                if (listener != null)
                                    listener.onPlaylistCreated(playlistName);
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        (dialog, which) -> {/* This constructor is intentionally empty.*/});

        return builder.create();
    }

    public void setOnPlaylistCreatedListener(OnPlaylistCreatedListener listener) {
        this.listener = listener;
    }

    public interface OnPlaylistCreatedListener {
        void onPlaylistCreated(String playlistName);
    }
}
