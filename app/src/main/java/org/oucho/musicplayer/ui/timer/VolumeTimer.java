package org.oucho.musicplayer.ui.timer;


import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.SendBroadcast;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class VolumeTimer implements Keys {

    private CountDownTimer volumeTimer;

    private final Context context = MusicPlayerApp.getInstance();

    public void startVolumeTimer(final Context context, final ScheduledFuture<?> task, final int delay) {

        // définir si le delay est supérieur ou inférieur à 10mn
        final short minutes = (short) ( ( (delay / 1000) % 3600) / 60);
        final boolean isLongTimer = minutes > 10;
        int cycle;

        if (isLongTimer)
            cycle = 60000;
        else
            cycle = 1000;


        volumeTimer = new CountDownTimer(delay, cycle) {
            @Override
            public void onTick(long mseconds) {

                long temps1 = ((task.getDelay(TimeUnit.MILLISECONDS) / 1000) % 3600) / 60 ;
                long temps2 = task.getDelay(TimeUnit.MILLISECONDS) / 1000;

                if (isLongTimer) {

                    if (temps1 < 1)
                        setVolume(0.1f);
                    else if (temps1 < 2)
                        setVolume(0.2f);
                    else if (temps1 < 3)
                        setVolume(0.3f);
                    else if (temps1 < 4)
                        setVolume(0.4f);
                    else if (temps1 < 5)
                        setVolume(0.5f);
                    else if (temps1 < 6)
                        setVolume(0.6f);
                    else if (temps1 < 7)
                        setVolume(0.7f);
                    else if (temps1 < 8)
                        setVolume(0.8f);
                    else if (temps1 < 9)
                        setVolume(0.9f);
                    else if (temps1 < 10)
                        setVolume(1.0f);

                } else {

                    if (temps2 < 6)
                        setVolume(0.1f);
                    else if (temps2 < 12)
                        setVolume(0.2f);
                    else if (temps2 < 18)
                        setVolume(0.3f);
                    else if (temps2 < 24)
                        setVolume(0.4f);
                    else if (temps2 < 30)
                        setVolume(0.5f);
                    else if (temps2 < 36)
                        setVolume(0.6f);
                    else if (temps2 < 42)
                        setVolume(0.7f);
                    else if (temps2 < 48)
                        setVolume(0.8f);
                    else if (temps2 < 54)
                        setVolume(0.9f);
                    else if (temps2 < 60)
                        setVolume(1.0f);
                }
            }

            @Override
            public void onFinish() {
                Intent player = new Intent(context, RadioService.class);
                player.setAction(ACTION_RADIO_STOP);
                context.startService(player);

                SendBroadcast.sending(context, HALT_AND_EXIT);
            }
        }.start();
    }


    public void setVolume(float volume) {
        if (PlayerService.isPlaying() || PlayerService.isPaused())
            SendBroadcast.sending(context, PLAYER_SERVICE_SET_VOLUME, "volume", volume);

        if (RadioState.isPlaying() || RadioState.isPaused()) {
            Intent niveau = new Intent(context, RadioService.class);
            niveau.putExtra("voldown", volume);
            context.startService(niveau);
        }
    }

    public CountDownTimer getVolumeTimer() {
        return volumeTimer;
    }
}

