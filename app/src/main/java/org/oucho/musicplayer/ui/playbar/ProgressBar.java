package org.oucho.musicplayer.ui.playbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;

import org.oucho.musicplayer.R;


public class ProgressBar extends View {

    private int maxValue;
    private int currentProgress;
    private int progressColor;
    private Paint paint;


    public ProgressBar(Context context) {
        super(context);
        init(context, null);
    }

    public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public ProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null)
            try (TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ProgressBar, 0, 0)) {
                progressColor = a.getColor(R.styleable.ProgressBar_progressColor, 0);
            }

        paint = new Paint();
        paint.setColor(progressColor);
    }

    private int getMaxValue() {
        return Math.max(1, maxValue);
    }

    public void setMaxValue(int max) {
        maxValue = max;
    }

    private int getCurrentProgress() {
        return currentProgress;
    }

    public void setCurrentProgress(int progress) {
        currentProgress = progress;
        invalidate();
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, getWidth() * (getCurrentProgress() / (float) getMaxValue()), getHeight(), paint);
    }
}
