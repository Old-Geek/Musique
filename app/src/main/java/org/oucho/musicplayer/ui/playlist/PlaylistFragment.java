package org.oucho.musicplayer.ui.playlist;

import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.helpers.TitleSet;
import org.oucho.musicplayer.ui.queue.QueueManager;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaylistFragment extends BaseFragment {

    private PlaylistDbHelper dbHelper;
    private PlaylistAdapter mAdapter;

    private OnBackInvokedCallback backCallback;

    FloatingActionButton buttonAdd;

    public static PlaylistFragment newInstance() {
        return new PlaylistFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new PlaylistDbHelper(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_playlist, container, false);
        RecyclerView mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new PlaylistAdapter();
        mAdapter.setOnItemClickListener(mOnItemClickListener);
        mAdapter.setOnItemLongClickListener(mOnItemLongClickListener);
        mRecyclerView.setAdapter(mAdapter);

        buttonAdd = rootView.findViewById(R.id.fab);
        buttonAdd.setOnClickListener(view -> {// Afficher le dialogue pour créer une nouvelle playlist
            final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
            view.performHapticFeedback(haptic);

            CreatePlaylistDialog dialog = CreatePlaylistDialog.newInstance();
            dialog.setOnPlaylistCreatedListener(playlistName -> loadPlaylists());
            dialog.show(getChildFragmentManager(), "créer une playlist");
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();

        requireActivity().getOnBackInvokedDispatcher().
                unregisterOnBackInvokedCallback(backCallback);
    }

    @Override
    public void onResume() {
        super.onResume();
        buttonAdd.setVisibility(View.VISIBLE);
        loadPlaylists();

        MainActivity.setCurrentViewID(R.id.fragment_playlist_layout);
        if (getActivity() instanceof MainActivity)
            TitleSet.setPlaylistTitle((AppCompatActivity) getActivity(), "Playlist");

        setBackInvokedCallback();
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;

            if (mQueueView.getVisibility() == View.VISIBLE)
                queueManager.toggleQueueVisibility();
            else
                SendBroadcast.sending(requireContext(), BACK_VIEWPAGER, true);
        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }


    private void loadPlaylists() {
        List<String> playlists = dbHelper.getAllPlaylists();

        // Tri personnalisé
        playlists.sort(new Comparator<>() {
            @Override
            public int compare(String s1, String s2) {
                // Extraire les nombres des chaînes
                int num1 = extractNumber(s1);
                int num2 = extractNumber(s2);

                if (num1 != -1 && num2 != -1) {
                    // Comparer les nombres si les deux chaînes contiennent des nombres
                    return Integer.compare(num1, num2);
                } else if (num1 != -1) {
                    // Si seulement s1 contient un nombre, il est considéré comme plus petit
                    return -1;
                } else if (num2 != -1) {
                    // Si seulement s2 contient un nombre, il est considéré comme plus petit
                    return 1;
                } else {
                    // Sinon, comparer en tant que chaînes de caractères
                    return s1.compareTo(s2);
                }
            }

            private int extractNumber(String s) {
                // Utiliser une expression régulière pour extraire le premier nombre entier de la chaîne
                Pattern pattern = Pattern.compile("\\d+");
                Matcher matcher = pattern.matcher(s);
                if (matcher.find()) {
                    try {
                        return Integer.parseInt(matcher.group());
                    } catch (NumberFormatException e) {
                        // Ignorer et retourner -1 si le nombre ne peut pas être analysé
                    }
                }
                return -1;
            }
        });

        mAdapter.updateData(playlists);
    }


    @Override
    public void load() {}

    private final BaseAdapter.OnItemClickListener mOnItemClickListener = new BaseAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(int position, View view) {
            String playlistName = mAdapter.getPlaylistName(position);

            Fragment fragment = PlaylistContentFragment.newInstance(playlistName);
            FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom, R.anim.slide_in_bottom, R.anim.slide_out_bottom);
            ft.replace(R.id.fragment_playlist_layout, fragment);
            ft.addToBackStack("PlaylistContentFragment");
            ft.commit();
            buttonAdd.setVisibility(View.GONE);
        }
    };


    private final BaseAdapter.OnItemLongClickListener mOnItemLongClickListener = this::showContextMenu;

    private void showContextMenu(int position, View view) {
        PopupMenu popup = new PopupMenu(requireContext(), view);
        popup.getMenuInflater().inflate(R.menu.playlist_context_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {

            if (item.getItemId() == R.id.action_delete_playlist)
                showConfirmDeleteDialog(position);
            else if (item.getItemId() == R.id.action_rename_playlist)
                showRenameDialog(position);

            return true;
        });

        popup.show();
    }

    private void showConfirmDeleteDialog(int position) {
        String playlistName = mAdapter.getPlaylistName(position);

        new AlertDialog.Builder(requireContext())
                .setTitle(R.string.delete_playlist)
                .setMessage(getString(R.string.confirm_delete_playlist, playlistName))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> deletePlaylist(position))
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void deletePlaylist(int position) {
        String playlistName = mAdapter.getPlaylistName(position);
        dbHelper.removePlaylist(playlistName);
        loadPlaylists();
    }

    private void showRenameDialog(int position) {
        String oldPlaylistName = mAdapter.getPlaylistName(position);

        AlertDialog.Builder builder= new AlertDialog.Builder(requireContext());
        builder.setTitle(R.string.rename_playlist);

        View viewInflated = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_rename_playlist, (ViewGroup) getView(), false);
        final EditText input = viewInflated.findViewById(R.id.input);
        input.setText(oldPlaylistName);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            String newPlaylistName = input.getText().toString().trim();
            if (!newPlaylistName.isEmpty() && !newPlaylistName.equals(oldPlaylistName)) {
                dbHelper.renamePlaylist(oldPlaylistName, newPlaylistName);
                loadPlaylists();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private void setOptionsMenu() {
        requireActivity().addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menu.findItem(R.id.action_search).setVisible(false);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {return false;}

        }, getViewLifecycleOwner(), Lifecycle.State.RESUMED);
    }
}