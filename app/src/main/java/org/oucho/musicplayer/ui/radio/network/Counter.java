package org.oucho.musicplayer.ui.radio.network;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter {

    private static final AtomicInteger counter = new AtomicInteger(1);

    public static int now() {
        return counter.get();
    }

    public static boolean still(int then) {
       return then == now();
   }
}

