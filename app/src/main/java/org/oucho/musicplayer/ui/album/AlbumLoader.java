package org.oucho.musicplayer.ui.album;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.ui.basefrag.BaseLoader;

import java.util.ArrayList;
import java.util.List;

public class AlbumLoader extends BaseLoader<List<Album>> {

    private static final String[] ALBUM_PROJECTION = {
            BaseColumns._ID,
            MediaStore.Audio.AlbumColumns.ALBUM,
            MediaStore.Audio.AlbumColumns.ARTIST,
            MediaStore.Audio.AlbumColumns.FIRST_YEAR,
    };

    private static String artistFilter = null;

    public AlbumLoader(Context context) {
        super(context);
    }

    public AlbumLoader(Context context, String artist) {
        super(context);
        setArtistFilter(artist);
    }

    private static void setArtistFilter(String artist) {
        artistFilter = artist;
    }

    public static void clearArtistFilter() {
        artistFilter = null;
    }

    @Override
    public List<Album> loadInBackground() {
        List<Album> albumList = new ArrayList<>();
        Cursor cursor = fetchAlbumCursor(artistFilter);

        if (cursor != null && cursor.moveToFirst()) {
            int idColumn = cursor.getColumnIndex(BaseColumns._ID);
            int albumNameColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM);
            int artistColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ARTIST);
            int yearColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.FIRST_YEAR);

            do {
                long id = cursor.getLong(idColumn);
                String name = cursor.getString(albumNameColumn);
                if (name == null || name.equals(MediaStore.UNKNOWN_STRING)) {
                    name = getContext().getString(R.string.unknown_album);
                    id = -1;
                }

                String artist = cursor.getString(artistColumn);
                int year = cursor.getInt(yearColumn);

                albumList.add(new Album(id, name, artist, year));

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return albumList;
    }

    private Cursor fetchAlbumCursor(String artist) {
        Uri albumUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String selection = getSelectionClause();
        String[] selectionArgs = getSelectionArguments();

        if (artist != null) {
            selection = DatabaseUtils.concatenateWhere(selection, MediaStore.Audio.Albums.ARTIST + " = ?");
            selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{artist});
        }

        return fetchCursor(albumUri, ALBUM_PROJECTION, selection, selectionArgs);
    }
}
