package org.oucho.musicplayer.ui.radio.network;

import android.util.Log;

import org.oucho.musicplayer.ui.radio.RadioPlaylist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSource;
import okio.Okio;

public class HttpGetter {

    private static final OkHttpClient client = new OkHttpClient();

    public static List<String> httpGet(String urlString) {
        List<String> lines = new ArrayList<>();

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                if (RadioPlaylist.isPlaylistMimeType(response.header("Content-Type"))) {
                    assert response.body() != null;
                    try (BufferedSource source = Okio.buffer(response.body().source())) {
                        readStream(source, lines);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("HttpGetter", "Erreur lors de la récupération des données HTTP", e);
        }

        return lines;
    }

    private static void readStream(BufferedSource source, List<String> lines) throws IOException {
        String line;
        while ((line = source.readUtf8Line()) != null) {
            lines.add(line);
        }
    }
}
