package org.oucho.musicplayer.ui.view.bubulle;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.tools.Preferences;

import java.util.Objects;

public class FastScrollBubble extends LinearLayout {

    public interface SectionIndexer {

        String getSectionText(int position);
    }

    private static final int BUBBLE_ANIM_DURATION = 100;
    private static final int SCROLLBAR_HIDE_DELAY = 1000;
    private static final int SCROLLBAR_ANIM_DURATION = 300;

    @ColorInt
    private int bubbleColor;
    @ColorInt private int handleColor;

    private int height;
    private View scrollbar;
    private TextView bubbleTextView;
    private ImageView handleImageView;
    private Drawable bubbleDrawable;
    private Drawable handleDrawable;
    private boolean hideScrollbar;
    private RecyclerView recyclerView;
    private SectionIndexer sectionIndexer;
    private ViewPropertyAnimator scrollbarAnimator;
    private ViewPropertyAnimator bubbleAnimator;

    private final Runnable scrollbarHider = this::hideScrollbar;

    private final RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(@androidx.annotation.NonNull RecyclerView recyclerView, int dx, int dy) {
            if (!handleImageView.isSelected() && isEnabled()) {
                setViewPositions(getScrollProportion(recyclerView));
            }
        }

        @Override
        public void onScrollStateChanged(@androidx.annotation.NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (isEnabled()) {
                switch (newState) {
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    getHandler().removeCallbacks(scrollbarHider);
                    cancelAnimation(scrollbarAnimator);

                    if (!isViewVisible(scrollbar)) {
                        showScrollbar();
                    }

                    break;

                case RecyclerView.SCROLL_STATE_IDLE:
                    if (hideScrollbar && !handleImageView.isSelected()) {
                        getHandler().postDelayed(scrollbarHider, SCROLLBAR_HIDE_DELAY);
                    }

                    break;
                default:
                    break;
                }
            }
        }
    };

    public FastScrollBubble(Context context) {
        super(context);
        initializeLayout(context, null);
        setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
    }

    public FastScrollBubble(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FastScrollBubble(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeLayout(context, attrs);
        setLayoutParams(generateLayoutParams(attrs));
    }

    @Override
    public void setLayoutParams(@NonNull ViewGroup.LayoutParams params) {
        params.width = LayoutParams.WRAP_CONTENT;
        super.setLayoutParams(params);
    }

    public void setLayoutParams(@NonNull ViewGroup viewGroup) {
        @IdRes int recyclerViewId = recyclerView.getId();

        if (recyclerViewId == NO_ID) {
            throw new IllegalArgumentException("RecyclerView must have a view ID");
        }

        if (viewGroup instanceof ConstraintLayout) {
            ConstraintSet constraintSet = new ConstraintSet();
            @IdRes int layoutId = getId();

            constraintSet.connect(layoutId, ConstraintSet.TOP, recyclerViewId, ConstraintSet.TOP);
            constraintSet.connect(layoutId, ConstraintSet.BOTTOM, recyclerViewId, ConstraintSet.BOTTOM);
            constraintSet.connect(layoutId, ConstraintSet.END, recyclerViewId, ConstraintSet.END);
            constraintSet.applyTo((ConstraintLayout) viewGroup);

        } else if (viewGroup instanceof CoordinatorLayout) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) getLayoutParams();

            layoutParams.setAnchorId(recyclerViewId);
            layoutParams.anchorGravity = GravityCompat.END;
            setLayoutParams(layoutParams);

        } else if (viewGroup instanceof FrameLayout) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();

            layoutParams.gravity = GravityCompat.END;
            setLayoutParams(layoutParams);

        } else if (viewGroup instanceof RelativeLayout) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
            int endRule = RelativeLayout.ALIGN_END;

            layoutParams.addRule(RelativeLayout.ALIGN_TOP, recyclerViewId);
            layoutParams.addRule(RelativeLayout.ALIGN_BOTTOM, recyclerViewId);
            layoutParams.addRule(endRule, recyclerViewId);
            setLayoutParams(layoutParams);

        } else {
            throw new IllegalArgumentException("Parent ViewGroup must be a ConstraintLayout, CoordinatorLayout, FrameLayout, or RelativeLayout");
        }
    }

    public void setSectionIndexer(SectionIndexer sectionIndexer) {
        this.sectionIndexer = sectionIndexer;
    }

    public void attachRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;

        if (this.recyclerView != null) {
            this.recyclerView.addOnScrollListener(scrollListener);
        }
    }

    public void detachRecyclerView() {
        if (recyclerView != null) {
            recyclerView.removeOnScrollListener(scrollListener);
            recyclerView = null;
        }
    }


    public void setHideScrollbar(boolean hideScrollbar) {
        this.hideScrollbar = hideScrollbar;
        scrollbar.setVisibility(hideScrollbar ? GONE : VISIBLE);
    }


    public void setHandleColor(@ColorInt int color) {
        handleColor = color;

        if (handleDrawable == null) {
            handleDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(getContext(), R.drawable.fastscroll_handle)));
            handleDrawable.mutate();
        }

        DrawableCompat.setTint(handleDrawable, handleColor);
        handleImageView.setImageDrawable(handleDrawable);
    }


    public void setBubbleColor(@ColorInt int color) {
        bubbleColor = color;

        if (bubbleDrawable == null) {
            bubbleDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(getContext(), R.drawable.fastscroll_bubble)));
            bubbleDrawable.mutate();
        }

        DrawableCompat.setTint(bubbleDrawable, bubbleColor);
        bubbleTextView.setBackground(bubbleDrawable);
    }


    public void setBubbleTextColor(@ColorInt int color) {
        bubbleTextView.setTextColor(color);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setVisibility(enabled ? VISIBLE : GONE);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {


        boolean voirBubulle = isBubbleVisible();


        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:

            if (event.getX() < handleImageView.getX() - handleImageView.getPaddingStart())
                return false;

            setHandleSelected(true);

            getHandler().removeCallbacks(scrollbarHider);
            cancelAnimation(scrollbarAnimator);
            cancelAnimation(bubbleAnimator);

            if (!isViewVisible(scrollbar))
                showScrollbar();

            if (sectionIndexer != null && !isViewVisible(bubbleTextView) && voirBubulle )
                showBubble();

            return true;
        case MotionEvent.ACTION_MOVE:
            final float y = event.getY();
            setViewPositions(y);
            setRecyclerViewPosition(y);
            return true;
        case MotionEvent.ACTION_UP:
        case MotionEvent.ACTION_CANCEL:
            setHandleSelected(false);

            if (hideScrollbar)
                getHandler().postDelayed(scrollbarHider, SCROLLBAR_HIDE_DELAY);

            if (isViewVisible(bubbleTextView))
                hideBubble();

            return true;
        }

        return super.onTouchEvent(event);
    }

    private static boolean isBubbleVisible() {
        String getTriSong = Preferences.getSongSort();
        String getTriAlbum = Preferences.getAlbumSort();
        int currentView = MainActivity.getCurrentViewID();
        boolean songByID = currentView == R.id.fragment_song_layout;
        boolean albumByID = currentView == R.id.fragment_album_list_layout;

        boolean voirBubulle = !albumByID || (!"_id DESC".equals(getTriAlbum));

        if (songByID && ("_id DESC".equals(getTriSong)))
            voirBubulle = false;

        return voirBubulle;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        height = h;
    }

    private void setRecyclerViewPosition(float y) {
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            int itemCount = recyclerView.getAdapter().getItemCount();
            float proportion;

            if (handleImageView.getY() == 0) {
                proportion = 0f;
            } else if (handleImageView.getY() + handleImageView.getHeight() >= height) {
                proportion = 1f;
            } else {
                proportion = y / (float) height;
            }

            int targetPos = getValueInRange(itemCount - 1, (int) (proportion * (float) itemCount));
            Objects.requireNonNull(recyclerView.getLayoutManager()).scrollToPosition(targetPos);

            if (sectionIndexer != null) {
                bubbleTextView.setText(sectionIndexer.getSectionText(targetPos));
            }
        }
    }

    private float getScrollProportion(RecyclerView recyclerView) {
        final int verticalScrollOffset = recyclerView.computeVerticalScrollOffset();
        final int verticalScrollRange = recyclerView.computeVerticalScrollRange();
        float proportion = (float) verticalScrollOffset / ((float) verticalScrollRange - height);
        return height * proportion;
    }

    private int getValueInRange(int max, int value) {
        int minimum = Math.max(0, value);
        return Math.min(minimum, max);
    }

    private void setViewPositions(float y) {
        int bubbleHeight = bubbleTextView.getHeight();
        int handleHeight = handleImageView.getHeight();

        bubbleTextView.setY(getValueInRange(height - bubbleHeight - handleHeight / 2, (int) (y - bubbleHeight)));
        handleImageView.setY(getValueInRange(height - handleHeight, (int) (y - (float) handleHeight / 2)));
    }

    private boolean isViewVisible(View view) {
        return view != null && view.getVisibility() == VISIBLE;
    }

    private void cancelAnimation(ViewPropertyAnimator animator) {
        if (animator != null) {
            animator.cancel();
        }
    }

    private void showBubble() {
        bubbleTextView.setVisibility(VISIBLE);
        bubbleAnimator = bubbleTextView.animate().alpha(1f)
                .setDuration(BUBBLE_ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                });
    }

    private void hideBubble() {
        bubbleAnimator = bubbleTextView.animate().alpha(0f)
                .setDuration(BUBBLE_ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        bubbleTextView.setVisibility(GONE);
                        bubbleAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        bubbleTextView.setVisibility(GONE);
                        bubbleAnimator = null;
                    }
                });
    }

    private void showScrollbar() {
        if (recyclerView.computeVerticalScrollRange() - height > 0) {
            float transX = getResources().getDimensionPixelSize(R.dimen.fastscroll_scrollbar_padding);

            scrollbar.setTranslationX(transX);
            scrollbar.setVisibility(VISIBLE);
            scrollbarAnimator = scrollbar.animate().translationX(0f).alpha(1f)
                    .setDuration(SCROLLBAR_ANIM_DURATION)
                    .setListener(new AnimatorListenerAdapter() {
                        // adapter required for new alpha value to stick
                    });
        }
    }

    private void hideScrollbar() {
        float transX = getResources().getDimensionPixelSize(R.dimen.fastscroll_scrollbar_padding);

        scrollbarAnimator = scrollbar.animate().translationX(transX).alpha(0f)
                .setDuration(SCROLLBAR_ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        scrollbar.setVisibility(GONE);
                        scrollbarAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        scrollbar.setVisibility(GONE);
                        scrollbarAnimator = null;
                    }
                });
    }

    private void setHandleSelected(boolean selected) {
        handleImageView.setSelected(selected);
        DrawableCompat.setTint(handleDrawable, selected ? bubbleColor : handleColor);
    }

    private void initializeLayout(Context context, AttributeSet attrs) {
        inflate(context, R.layout.fastscroller, this);

        setClipChildren(false);
        setOrientation(HORIZONTAL);
        bubbleTextView = findViewById(R.id.fastscroll_bubble);
        handleImageView = findViewById(R.id.fastscroll_handle);
        scrollbar = findViewById(R.id.fastscroll_scrollbar);

        @ColorInt int bubbleColor = Color.GRAY;
        @ColorInt int handleColor = Color.DKGRAY;
        @ColorInt int textColor = Color.WHITE;

        boolean hideScrollbar = true;

        if (attrs != null) {
            try (TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FastScrollBubble, 0, 0)) {
                bubbleColor = typedArray.getColor(R.styleable.FastScrollBubble_bubbleColor, bubbleColor);
                handleColor = typedArray.getColor(R.styleable.FastScrollBubble_handleColor, handleColor);
                textColor = typedArray.getColor(R.styleable.FastScrollBubble_bubbleTextColor, textColor);
                hideScrollbar = typedArray.getBoolean(R.styleable.FastScrollBubble_hideScrollbar, true);
            }
        }

        setHandleColor(handleColor);
        setBubbleColor(bubbleColor);
        setBubbleTextColor(textColor);
        setHideScrollbar(hideScrollbar);
    }
}
