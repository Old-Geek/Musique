package org.oucho.musicplayer.ui.radio;


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Radio;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.picasso.Picasso;

import java.util.ArrayList;

public class RadioAdapter extends BaseAdapter<RadioAdapter.RadioViewHolder> {

    private ArrayList<Radio> radioList = new ArrayList<>();

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Radio> data) {
        radioList = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RadioViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_radio_item, parent, false);
        return new RadioViewHolder(itemView);
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(RadioViewHolder viewHolder, int position) {

        Radio radio = radioList.get(position);
        String title = radio.stationName();
        viewHolder.text.setText(title);

        String logoPath = "file://" + radio.logoImage();
        Picasso.get().load(logoPath).into(viewHolder.logoRadio);
    }



    @Override
    public int getItemCount() {
        return radioList.size();
    }

    public Radio getItem(int position) {
        return radioList.get(position);
    }

    public class RadioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private final TextView text;
        private final ImageView logoRadio;

        RadioViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.textViewRadio);
            logoRadio = itemView.findViewById(R.id.logoViewRadio);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            triggerOnItemClickListener(position, v);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getLayoutPosition();
            triggerOnItemLongClickListener(position, view);
            return false;
        }
    }
}
