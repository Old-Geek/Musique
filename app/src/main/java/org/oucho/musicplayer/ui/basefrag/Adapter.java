package org.oucho.musicplayer.ui.basefrag;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


@SuppressWarnings("SameReturnValue")
public abstract class Adapter<V extends RecyclerView.ViewHolder> extends BaseAdapter<V> {

    @NonNull
    @Override
    public V onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return onCreateViewHolderImpl(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
            onBindViewHolderImpl(holder, position);
    }

    @Override
    public int getItemCount() {
        return getTotalItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return getViewType();
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    protected void triggerOnItemClickListener(int position, View view) {
        super.triggerOnItemClickListener(position, view);
    }

    protected abstract V onCreateViewHolderImpl(ViewGroup parent);

    protected abstract void onBindViewHolderImpl(V holder, int position);

    protected abstract int getTotalItemCount();

    protected abstract int getViewType();

}
