package org.oucho.musicplayer.ui.playbar;

import android.os.Handler;
import android.os.Looper;
import android.view.View;

import org.oucho.musicplayer.services.PlayerService;

public class PlayBarHelper {

    private final View mainLayout;
    private final View radioLayout;
    private final ProgressBar progressBar;
    private final Runnable updateProgressTask;

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    private boolean isPlaybarRadioVisible = true; // premier lancement = radio


    public PlayBarHelper(View mainLayout, View radioLayout, ProgressBar progressBar, Runnable updateProgressBarRunnable) {
        this.mainLayout = mainLayout;
        this.radioLayout = radioLayout;
        this.progressBar = progressBar;
        this.updateProgressTask = updateProgressBarRunnable;
    }

    public void handlePlayStateChanged() {
        if (PlayerService.isPlaying())
            mainHandler.post(updateProgressTask); // Assurez-vous que mUpdateProgressBar est accessible
        else
            mainHandler.removeCallbacks(updateProgressTask);
    }

    public void showRadioLayout(float tailleBarre) {

        if (isPlaybarRadioVisible) // premier lancement = radio
            return;

        PlayBarAnimationHelper.animateMovement(radioLayout, 0, 0, tailleBarre, 0);
        radioLayout.setVisibility(View.VISIBLE);

        PlayBarAnimationHelper.animateMovement(mainLayout, 0, 0, 0, -tailleBarre);
        mainLayout.setVisibility(View.GONE);

        PlayBarAnimationHelper.animateOpacity(progressBar, 1, 0);
        progressBar.setVisibility(View.GONE);
        isPlaybarRadioVisible = true;

    }

    public void hideRadioLayout(float tailleBarre) {
        PlayBarAnimationHelper.animateMovement(mainLayout, 0, 0, -tailleBarre, 0);
        mainLayout.setVisibility(View.VISIBLE);

        PlayBarAnimationHelper.animateMovement(radioLayout, 0, 0, 0, tailleBarre);
        mainHandler.postDelayed(() -> {
            radioLayout.clearAnimation();
            radioLayout.setVisibility(View.GONE);
        }, 300);

        if (isPlaybarRadioVisible) {
            PlayBarAnimationHelper.animateOpacity(progressBar, 0, 1);
            progressBar.setVisibility(View.VISIBLE);
            isPlaybarRadioVisible = false;
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}