package org.oucho.musicplayer.ui.album;


import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.Keys;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.view.bubulle.FastScrollBubble;
import org.oucho.picasso.Picasso;

import java.text.Normalizer;
import java.util.Collections;
import java.util.List;


public class AlbumListAdapter extends BaseAdapter<AlbumListAdapter.AlbumViewHolder> implements FastScrollBubble.SectionIndexer, Keys {

    private final Context context;
    private List<Album> mAlbumList = Collections.emptyList();

    private final int artSize;

    public AlbumListAdapter(Context context, int artSize) {
        this.context = context;
        this.artSize = artSize;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Album> data) {
        mAlbumList = data;
        notifyDataSetChanged();
    }


    /** @noinspection ClassEscapesDefinedScope*/
    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        int mLayoutId = R.layout.fragment_liste_album_item;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);

        return new AlbumViewHolder(itemView);
    }

    @Override
    public String getSectionText(int position) {

        Album album = mAlbumList.get(position);
        String getTri = Preferences.getAlbumSort();
        String toto;
        return switch (getTri) {
            case "REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC" -> {
                toto = String.valueOf(album.getArtist()).replaceFirst("The ", "");
                yield stripAccents(String.valueOf(toto.toUpperCase().charAt(0)));
            }
            case "minyear DESC" -> String.valueOf(album.getReleaseYear());
            case "album_key" -> {
                toto = String.valueOf(album.getTitle()).replaceFirst("The ", "");
                yield stripAccents(String.valueOf(toto.toUpperCase().charAt(0))); // TODO pourquoi album key ?
            }
            default -> null;
        };
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }


    /** @noinspection ClassEscapesDefinedScope*/
    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder viewHolder, int position) {
        Album album = mAlbumList.get(position);
        String getTri = Preferences.getAlbumSort();
        Song song = PlayerService.getCurrentSong();

        if (song == null)
            return;

        if (album.getAlbumId() == song.getAlbumIdentifier()) {

            viewHolder.vPlayerStatus.setVisibility(View.VISIBLE);
            viewHolder.vPlayerStatusFond.setVisibility(View.VISIBLE);

        } else {

            viewHolder.vPlayerStatus.setVisibility(View.INVISIBLE);
            viewHolder.vPlayerStatusFond.setVisibility(View.INVISIBLE);
        }

        switch (getTri) {
            case "REPLACE ('<BEGIN>' || artist, '<BEGIN>The ', '<BEGIN>') ASC, minyear ASC":

                viewHolder.vName.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
                viewHolder.vName.setTextSize(13);
                viewHolder.vName.setTypeface(null, Typeface.NORMAL);

                viewHolder.vArtist.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                viewHolder.vArtist.setTextSize(14);
                viewHolder.vArtist.setTypeface(null, Typeface.BOLD);

                viewHolder.vYear.setVisibility(View.INVISIBLE);
                viewHolder.vBackgroundYear.setVisibility(View.INVISIBLE);

                break;
            case "minyear DESC":
                viewHolder.vName.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
                viewHolder.vName.setTextSize(13);
                viewHolder.vName.setTypeface(null, Typeface.NORMAL);

                viewHolder.vArtist.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
                viewHolder.vArtist.setTextSize(13);
                viewHolder.vArtist.setTypeface(null, Typeface.NORMAL);

                viewHolder.vYear.setText(String.valueOf(album.getReleaseYear()));
                viewHolder.vYear.setVisibility(View.VISIBLE);

                viewHolder.vBackgroundYear.setVisibility(View.VISIBLE);

                break;
            case "album_key":
                viewHolder.vName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                viewHolder.vName.setTextSize(14);
                viewHolder.vName.setTypeface(null, Typeface.BOLD);

                viewHolder.vArtist.setTextColor(ContextCompat.getColor(context, R.color.grey_600));
                viewHolder.vArtist.setTextSize(13);
                viewHolder.vArtist.setTypeface(null, Typeface.NORMAL);

                viewHolder.vYear.setVisibility(View.INVISIBLE);
                viewHolder.vBackgroundYear.setVisibility(View.INVISIBLE);
                break;
        }

        viewHolder.vName.setText(album.getTitle());
        viewHolder.vArtist.setText(album.getArtist());

        if (album.getAlbumId() == -1)
            return;

        Uri uri = ContentUris.withAppendedId(ALBUM_ARTWORK_URI, album.getAlbumId());
        Picasso.get()
                .load(uri)
                .resize(artSize, artSize)
                .centerCrop()
                .into(viewHolder.vArtwork);
    }

    @Override
    public int getItemCount() {
        return mAlbumList.size();
    }


    public Album getItem(int position) {
        return mAlbumList.get(position);
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        final ImageView vArtwork;
        final TextView vName;
        final TextView vYear;
        final ImageView vBackgroundYear;

        private final ImageView vPlayerStatus;
        private final ImageView  vPlayerStatusFond;

        private final TextView vArtist;

        private AlbumViewHolder(View itemView) {
            super(itemView);

            vArtwork = itemView.findViewById(R.id.album_artwork);
            vName = itemView.findViewById(R.id.album_name);
            vYear = itemView.findViewById(R.id.year);
            vBackgroundYear = itemView.findViewById(R.id.background_year);
            vPlayerStatus = itemView.findViewById(R.id.album_play);
            vPlayerStatusFond = itemView.findViewById(R.id.album_play_fond);

            LinearLayout vAlbumInfo = itemView.findViewById(R.id.album_info);

            vArtwork.setOnClickListener(this);

            vArtist = itemView.findViewById(R.id.artist_name);
            itemView.findViewById(R.id.album_info).setOnClickListener(this);

            vArtwork.setOnLongClickListener(this);
            vAlbumInfo.setOnLongClickListener(this);

            ImageButton menuButton = itemView.findViewById(R.id.menu_button);
            menuButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getBindingAdapterPosition();
            triggerOnItemClickListener(position, v);
        }

        @Override
        public boolean onLongClick(View v) {
            int position = getBindingAdapterPosition();
            triggerOnItemLongClickListener(position, v);
            return true;
        }
    }

}
