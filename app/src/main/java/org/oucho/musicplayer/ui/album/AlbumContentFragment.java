package org.oucho.musicplayer.ui.album;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.PlaylistDbHelper;
import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.services.PlayerService;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.basefrag.CustomViewModel;
import org.oucho.musicplayer.ui.playlist.CreatePlaylistDialog;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.song.SongLoader;
import org.oucho.musicplayer.ui.tags.SongTagHelper;
import org.oucho.musicplayer.ui.view.CustomLayoutManager;
import org.oucho.musicplayer.ui.view.bubulle.BubulleRecyclerView;
import org.oucho.picasso.Picasso;

import java.util.List;
import java.util.Objects;


public class AlbumContentFragment extends BaseFragment {


    private Album album;
    private AlbumContentAdapter adapter;
    private BubulleRecyclerView recyclerView;
    private Receiver broadcastReceiver;
    private boolean isReceiverRegistered = false;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private String albumName = "";
    private List<Song> songList;
    private CustomViewModel viewModel;

    private OnBackInvokedCallback backCallback;



    public static AlbumContentFragment newInstance(Album album) {
        AlbumContentFragment fragment = new AlbumContentFragment();

        Bundle args = new Bundle();
        args.putParcelable("album", album);
        fragment.setArguments(args);

        return fragment;
    }


    private final LoaderManager.LoaderCallbacks<List<Song>> songLoaderCallbacks = new LoaderManager.LoaderCallbacks<>() {

        @NonNull
        @Override
        public Loader<List<Song>> onCreateLoader(int id, Bundle args) {
            SongLoader loader = new SongLoader(getActivity());

            loader.setSelection(MediaStore.Audio.Media.ALBUM_ID + " = ?", new String[]{String.valueOf(album.getAlbumId())});
            loader.setSortOrder(MediaStore.Audio.Media.TRACK);
            return loader;
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<Song>> loader, List<Song> songs) {
            adapter.setData(songs);

            songList = songs;

            int dureeTotal = 0;

            long songId = PlayerService.getCurrentSong().getSongId();

            for (int i = 0; i < songList.size(); i++) {

                if (songList.get(i).getSongId() == songId)
                    recyclerView.smoothScrollToPosition(i);

                dureeTotal = dureeTotal + songList.get(i).getSongDuration();
            }

        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<Song>> loader) {
            //  Auto-generated method stub
        }
    };


    /* *********************************************************************************************
     * Création du fragment
     * ********************************************************************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        broadcastReceiver = new Receiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_PLAYER_STATE);
        filter.addAction(SET_TITLE);

        requireContext().registerReceiver(broadcastReceiver, filter, Context.RECEIVER_EXPORTED);
        isReceiverRegistered = true;

        if (bundle != null) {

            album = bundle.getParcelable("album", Album.class);

            assert album != null;
            albumName = album.getTitle();
        }

        handler.postDelayed(() -> {
            requireActivity().setTitle(albumName);
            SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", false);
            }, 300);
    }


    /* *********************************************************************************************
     * Création du visuel
     * ********************************************************************************************/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_album_content, container, false);

        CoordinatorLayout presentation = rootView.findViewById(R.id.presentation);
        presentation.setOnClickListener(null);

        ImageView artworkView = rootView.findViewById(R.id.album_artwork);

        WindowMetrics windowMetrics = WindowMetricsCalculator .getOrCreate().computeCurrentWindowMetrics(requireActivity());
        final int size = windowMetrics.getBounds().width();

        Uri uri = ContentUris.withAppendedId(ALBUM_ARTWORK_URI, album.getAlbumId());

        Picasso.get()
                .load(uri)
                .resize(size, size)
                .centerCrop()
                .into(artworkView);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new CustomLayoutManager(getActivity()));
        adapter = new AlbumContentAdapter();
        adapter.setOnItemClickListener(songItemClickListener);

        recyclerView.setAdapter(adapter);

        return rootView;
    }


    private final BaseAdapter.OnItemClickListener songItemClickListener = new BaseAdapter.OnItemClickListener() {

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onItemClick(int position, View view) {
            final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
            view.performHapticFeedback(haptic);

            handler.postDelayed(() -> adapter.notifyDataSetChanged(), 100);

            int viewId = view.getId();

            if (viewId == R.id.item_view)
                selectSong(position);
            else if (viewId == R.id.buttonMenu)
                showMenu(position, view);
        }
    };


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(CustomViewModel.class);
        LoaderManager.getInstance(this).initLoader(0, null, songLoaderCallbacks);
    }


    @Override
    public void load() {
        LoaderManager.getInstance(this).restartLoader(0, null, songLoaderCallbacks);
    }

    private void selectSong(int position) {
        MainActivity mainActivity = (MainActivity) requireContext();
        mainActivity.onSongSelected(adapter.getSongList(), position);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (isReceiverRegistered) {
            requireContext().unregisterReceiver(broadcastReceiver);
            isReceiverRegistered = false;
        }

        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        viewModel.setAlbumFragmentVisible(false);

        requireActivity().getSupportFragmentManager()
                .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        requireActivity().getOnBackInvokedDispatcher()
                .unregisterOnBackInvokedCallback(backCallback);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        if (broadcastReceiver != null && isReceiverRegistered) {
            requireContext().unregisterReceiver(broadcastReceiver);
            isReceiverRegistered = false;
        }
        if (backCallback != null) {
            requireActivity().getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback(backCallback);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        handler.removeCallbacksAndMessages(null);
        if (backCallback != null) {
            requireActivity().getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback(backCallback);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, false);

        viewModel.setAlbumFragmentVisible(true);

        if (!isReceiverRegistered) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(INTENT_PLAYER_STATE);
            filter.addAction(SET_TITLE);

            requireContext().registerReceiver(broadcastReceiver, filter, Context.RECEIVER_EXPORTED);
            isReceiverRegistered = true;
        }

        setBackInvokedCallback();
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {// Gérer le geste de retour ici

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;

            if (mQueueView.getVisibility() == View.VISIBLE) {
                queueManager.toggleQueueVisibility();

            } else {

                viewModel.setAlbumFragmentVisible(false);

                requireActivity().getSupportFragmentManager()
                        .popBackStack("AlbumContentFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                SendBroadcast.sending(requireContext(), "reload");
                SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", true);
                SendBroadcast.sending(requireContext(), SET_TITLE);
            }

        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }


    private class Receiver extends BroadcastReceiver {

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onReceive(Context context, Intent intent) {

            String receiveIntent = intent.getAction();

            if (INTENT_PLAYER_STATE.equals(receiveIntent)) {

                if (Objects.equals(intent.getStringExtra("state"), "prev") ||
                        Objects.equals(intent.getStringExtra("state"), "next") ||
                        Objects.equals(intent.getStringExtra("state"), "play")) {

                    long songId = PlayerService.getCurrentSong().getSongId();

                    for (int i = 0; i < songList.size(); i++)
                        if (songList.get(i).getSongId() == songId)
                            recyclerView.smoothScrollToPosition(i);

                    if (PlayerService.isPlaying())
                        adapter.notifyDataSetChanged();
                    else
                        handler.postDelayed(() -> adapter.notifyDataSetChanged(), 100);
                }
            }

            if (SET_TITLE.equals(receiveIntent))
                    requireActivity().setTitle(albumName);
        }
    }

    /* *********************************************************************************************
     * Menu titre
     * ********************************************************************************************/

    private void showMenu(final int position, View v) {

        final Song song = adapter.getItem(position);

        PopupMenu popup = new PopupMenu(requireActivity(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.song_item, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();

            if (itemId == R.id.action_add_to_queue)
                ((MainActivity) requireActivity()).addToQueue(song);
            else if (itemId == R.id.action_add_to_playlist)
                showPlaylistDialog(position);
            else if (itemId == R.id.action_show_tag) {
                Log.d("TAG", "showTags:");
                SongTagHelper songTagHelper = new SongTagHelper(this, song, position, songList);
                songTagHelper.showTags();
            }

            return true; // Retourne true pour indiquer que le clic a été géré
        });

        popup.show();
    }

    private void showPlaylistDialog(final int position) {
        final Song song = adapter.getItem(position);
        try (PlaylistDbHelper dbHelper = new PlaylistDbHelper(requireActivity())) {
            List<String> playlists = dbHelper.getAllPlaylists();

            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setTitle(R.string.add_to_playlist);

            CharSequence[] playlistNames = playlists.toArray(new CharSequence[0]);

            builder.setItems(playlistNames, (dialog, which) -> {
                String playlistName = playlists.get(which);
                dbHelper.addSongToPlaylist(playlistName, song);
            });

            builder.setPositiveButton(R.string.create_playlist, (dialog, which) -> showCreatePlaylistDialog(song));
            builder.setNegativeButton(android.R.string.cancel, null);
            builder.show();
        }
    }

    private void showCreatePlaylistDialog(final Song song) {
        CreatePlaylistDialog dialog = CreatePlaylistDialog.newInstance();
        dialog.setOnPlaylistCreatedListener(playlistName -> {
            try (PlaylistDbHelper dbHelper =new PlaylistDbHelper(requireActivity())) {
                dbHelper.addSongToPlaylist(playlistName, song);
            }
        });
        dialog.show(getChildFragmentManager(), "create_playlist");
    }
}
