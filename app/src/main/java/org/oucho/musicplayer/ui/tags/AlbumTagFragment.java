package org.oucho.musicplayer.ui.tags;

import static android.app.Activity.RESULT_OK;
import static org.oucho.musicplayer.Keys.SET_TITLE;
import static org.oucho.musicplayer.Keys.SWIPE_ENABLED;
import static org.oucho.musicplayer.Keys.TOOLBAR_SHADOW;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.images.Artwork;
import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.model.Album;
import org.oucho.musicplayer.db.model.Song;
import org.oucho.musicplayer.tools.ImageTools;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.CustomViewModel;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.song.SongLoader;
import org.oucho.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AlbumTagFragment extends Fragment {

    private static final String ARG_ALBUM = "album";
    private static final int HAPTIC_FEEDBACK = HapticFeedbackConstants.VIRTUAL_KEY;

    private Album currentAlbum;
    private String artiste = "", albumArtiste = "", album = "", annee = "", genre = "", comment = "";
    private EditText editTextArtist, editTextAlbumArtiste, editTextAlbum, editTextYear, editTextGenre, editTextComment;
    private ImageButton imageButton;
    private ActivityResultLauncher<Intent> filePickerLauncher;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private OnBackInvokedCallback backCallback;
    private CustomViewModel viewModel;
    private boolean newCover = false;
    private Uri newBitmapUri;
    private Bitmap newBitmap;
    private List<Song> songList;
    private Context context;
    private View rootView;

    public AlbumTagFragment() {
        // Required empty public constructor
    }

    public static AlbumTagFragment newInstance(Album album) {
        AlbumTagFragment fragment = new AlbumTagFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ALBUM, album);
        fragment.setArguments(args);
        return fragment;
    }

    private final LoaderManager.LoaderCallbacks<List<Song>> songLoaderCallbacks = new LoaderManager.LoaderCallbacks<>() {
        @NonNull
        @Override
        public Loader<List<Song>> onCreateLoader(int id, Bundle args) {
            SongLoader loader = new SongLoader(requireContext());
            loader.setSelection(MediaStore.Audio.Media.ALBUM_ID + " = ?", new String[]{String.valueOf(currentAlbum.getAlbumId())});
            loader.setSortOrder(MediaStore.Audio.Media.TRACK);
            return loader;
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<Song>> loader, List<Song> songs) {
            songList = songs;
            if (savedState != null) {
                onViewStateRestored(savedState);
            } else {
                showTags(songs);
            }
        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<Song>> loader) {}
    };

    private Bundle savedState = null;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("artist", editTextArtist.getText().toString());
        outState.putString("albumArtist", editTextAlbumArtiste.getText().toString());
        outState.putString("album", editTextAlbum.getText().toString());
        outState.putString("year", editTextYear.getText().toString());
        outState.putString("genre", editTextGenre.getText().toString());
        outState.putString("comment", editTextComment.getText().toString());
        savedState = outState;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            editTextArtist.setText(savedInstanceState.getString("artist"));
            editTextAlbumArtiste.setText(savedInstanceState.getString("albumArtist"));
            editTextAlbum.setText(savedInstanceState.getString("album"));
            editTextYear.setText(savedInstanceState.getString("year"));
            editTextGenre.setText(savedInstanceState.getString("genre"));
            editTextComment.setText(savedInstanceState.getString("comment"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = requireContext();
        Bundle bundle = getArguments();
        if (bundle != null) {
            currentAlbum = bundle.getParcelable(ARG_ALBUM, Album.class);
            assert currentAlbum != null;
            String albumName = currentAlbum.getTitle();
            handler.postDelayed(() -> {
                requireActivity().setTitle(albumName);
                SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", false);
            }, 300);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_album_tag, container, false);
        rootView.setClickable(true);
        rootView.setFocusable(true);
        rootView.setFocusableInTouchMode(true);
        rootView.setOnTouchListener((v, event) -> true);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(CustomViewModel.class);
        editTextArtist = rootView.findViewById(R.id.editTextArtist);
        editTextAlbumArtiste = rootView.findViewById(R.id.editTextAlbumArtist);
        editTextAlbum = rootView.findViewById(R.id.editTextAlbum);
        editTextYear = rootView.findViewById(R.id.editTextYear);
        editTextGenre = rootView.findViewById(R.id.editTextGenre);
        editTextComment = rootView.findViewById(R.id.editTextComment);
        imageButton = rootView.findViewById(R.id.editAlbumArt);
        imageButton.setOnClickListener(v -> openFilePicker());
        Button buttonSave = rootView.findViewById(R.id.button_save);
        buttonSave.setOnClickListener(v -> {
            v.performHapticFeedback(HAPTIC_FEEDBACK);
            saveTagsInParallel(songList,
                    editTextArtist.getText().toString(),
                    editTextAlbumArtiste.getText().toString(),
                    editTextAlbum.getText().toString(),
                    editTextYear.getText().toString(),
                    editTextGenre.getText().toString(),
                    editTextComment.getText().toString()
            );
        });
        initializeFilePicker();
        LoaderManager.getInstance(this).initLoader(0, null, songLoaderCallbacks);
    }

    private void initializeFilePicker() {
        filePickerLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                        Uri fileUri = result.getData().getData();
                        if (fileUri != null) {
                            try {
                                int inPixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, requireContext().getResources().getDisplayMetrics());
                                InputStream inputStream = requireContext().getContentResolver().openInputStream(fileUri);
                                assert inputStream != null;
                                byte[] imageBytes = readBytes(inputStream);
                                Bitmap bitmap = ImageTools.resizeImageToBitmap(imageBytes, inPixels, inPixels);
                                new Handler(Looper.getMainLooper()).post(() -> {
                                    newCover = true;
                                    newBitmapUri = fileUri;
                                    newBitmap = bitmap;
                                    imageButton.setImageBitmap(bitmap);
                                });
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                });
    }

    private void openFilePicker() {
        rootView.performHapticFeedback(HAPTIC_FEEDBACK);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        filePickerLauncher.launch(intent);
    }

    private byte[] readBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void showTags(List<Song> songs) {
        if (songs.isEmpty()) {
            return;
        }
        Song firstSong = songs.get(0);
        try {
            AudioFile audioFile = AudioFileIO.read(new File(firstSong.getFilePath()));
            Tag tag = audioFile.getTag();
            if (tag != null) {
                artiste = tag.getFirst(FieldKey.ARTIST);
                albumArtiste = tag.getFirst(FieldKey.ALBUM_ARTIST);
                album = tag.getFirst(FieldKey.ALBUM);
                genre = tag.getFirst(FieldKey.GENRE);
                annee = tag.getFirst(FieldKey.YEAR);
                comment = tag.getFirst(FieldKey.COMMENT);
                int inPixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, requireContext().getResources().getDisplayMetrics());
                List<Artwork> artworkList = tag.getArtworkList();
                if (newCover && newBitmap != null) {
                    imageButton.setImageBitmap(newBitmap);
                } else if (!artworkList.isEmpty()) {
                    Artwork artwork = artworkList.get(0);
                    byte[] imageData = artwork.getBinaryData();
                    Bitmap bitmap = ImageTools.resizeImageToBitmap(imageData, inPixels, inPixels);
                    imageButton.setImageBitmap(bitmap);
                }
            }
        } catch (Exception e) {
            Log.e("Erreur lors de la lecture du fichier audio", Objects.requireNonNull(e.getMessage()));
        }
        setDefaultValues(songs);
    }

    private void setDefaultValues(List<Song> songs) {
        boolean artistDivers = false, albumArtisteDivers = false, albumDivers = false, yearDivers = false, genreDivers = false, commentDivers = false;
        for (Song song : songs) {
            try {
                AudioFile audioFile = AudioFileIO.read(new File(song.getFilePath()));
                Tag tag = audioFile.getTag();
                if (tag != null) {
                    if (!tag.getFirst(FieldKey.ARTIST).equals(artiste) && !artistDivers) artistDivers = true;
                    if (!tag.getFirst(FieldKey.ALBUM_ARTIST).equals(albumArtiste) && !albumArtisteDivers) albumArtisteDivers = true;
                    if (!tag.getFirst(FieldKey.ALBUM).equals(album) && !albumDivers) albumDivers = true;
                    if (!tag.getFirst(FieldKey.YEAR).equals(annee) && !yearDivers) yearDivers = true;
                    if (!tag.getFirst(FieldKey.GENRE).equals(genre) && !genreDivers) genreDivers = true;
                    if (!tag.getFirst(FieldKey.COMMENT).equals(comment) && !commentDivers) commentDivers = true;
                }
            } catch (Exception e) {
                Log.e("Erreur lors de la lecture du fichier audio", Objects.requireNonNull(e.getMessage()));
            }
        }
        int multipleColor = ContextCompat.getColor(requireContext(), R.color.blue_900_trans);
        editTextArtist.setText(artistDivers ? "Artiste multiple" : artiste);
        editTextArtist.setTextColor(artistDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
        editTextAlbumArtiste.setText(albumArtisteDivers ? "Artiste multiple" : albumArtiste);
        editTextAlbumArtiste.setTextColor(albumArtisteDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
        editTextAlbum.setText(albumDivers ? "Album multiple" : album);
        editTextAlbum.setTextColor(albumDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
        editTextYear.setText(yearDivers ? "Année multiple" : annee);
        editTextYear.setTextColor(yearDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
        editTextGenre.setText(genreDivers ? "Genre multiple" : genre);
        editTextGenre.setTextColor(genreDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
        editTextComment.setText(commentDivers ? "Commentaire multiple" : comment);
        editTextComment.setTextColor(commentDivers ? multipleColor : ContextCompat.getColor(requireContext(), android.R.color.black));
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {
            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;
            if (mQueueView.getVisibility() == View.VISIBLE) {
                queueManager.toggleQueueVisibility();
            } else {
                viewModel.setAlbumFragmentVisible(false);
                requireActivity().getSupportFragmentManager().popBackStack("AlbumTagFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                SendBroadcast.sending(requireContext(), "reload");
                SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", true);
                SendBroadcast.sending(requireContext(), SET_TITLE);
            }
        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }

    private void handleBackAction() {
        viewModel.setAlbumFragmentVisible(false);
        requireActivity().getSupportFragmentManager().popBackStack("AlbumTagFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        SendBroadcast.sending(requireContext(), "reload");
        SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", true);
        SendBroadcast.sending(requireContext(), SET_TITLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        viewModel.setAlbumFragmentVisible(true);
        handler.removeCallbacksAndMessages(null);
        if (backCallback != null) {
            requireActivity().getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback(backCallback);
            backCallback = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, false);
        viewModel.setAlbumFragmentVisible(true);
        setBackInvokedCallback();
    }

    public void saveTagsInParallel(List<Song> songs, String artistx, String albumArtistex, String albumx, String yearx, String genrex, String commentx) {
        int corePoolSize = Runtime.getRuntime().availableProcessors() - 1;
        int maximumPoolSize = corePoolSize * 2;

        ThreadFactory threadFactory = new PriorityThreadFactory(Thread.MAX_PRIORITY);
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                threadFactory
        );

        Artwork coverArtData;
        if (newBitmap != null && newBitmapUri != null)
            coverArtData = ImageTools.createArtworkForJAudioTagger(context, newBitmapUri, 1220);
        else {
            coverArtData = null;
        }

        for (Song song : songs) {
            executorService.execute(() -> {
                try {
                    AudioFile audioFile = AudioFileIO.read(new File(song.getFilePath()));
                    Tag tag = audioFile.getTag();
                    if (tag == null) {
                        tag = audioFile.createDefaultTag();
                    }
                    updateTag(tag, FieldKey.ARTIST, artistx, "Artiste multiple");
                    updateTag(tag, FieldKey.ALBUM_ARTIST, albumArtistex, "Artiste multiple");
                    updateTag(tag, FieldKey.ALBUM, albumx, "Album multiple");
                    updateTag(tag, FieldKey.YEAR, yearx, "Année multiple");
                    updateTag(tag, FieldKey.GENRE, genrex, "Genre multiple");
                    updateTag(tag, FieldKey.COMMENT, commentx, "Commentaire multiple");
                    if (newCover && coverArtData != null) {
                            tag.deleteField(FieldKey.COVER_ART);
                            tag.setField(coverArtData);
                    }
                    audioFile.commit();
                    scanFile(song.getFilePath());
                } catch (Exception e) {
                    Log.e("Jaudiotagger", "Erreur lors de la sauvegarde des tags: " + e.getMessage());
                }
            });
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        handler.postDelayed(() -> {
            SendBroadcast.sending(requireContext(), "reload");
            handleBackAction();
        }, 1000);
    }

    private void updateTag(Tag tag, FieldKey fieldKey, String value, String multipleValue) throws FieldDataInvalidException {
        if (!value.equals(multipleValue)) {
            if (value.isEmpty()) {
                tag.deleteField(fieldKey);
            } else {
                tag.setField(fieldKey, value);
            }
        }
    }

    private void scanFile(String filePath) {
        MediaScannerConnection.scanFile(context, new String[]{filePath}, null,
                (path, uri) -> {
                    Uri mediaUri = Uri.parse(uri.toString());
                    Uri albumArtUri = getAlbumArtUri(mediaUri);
                    Picasso.get().invalidate(albumArtUri);
                }
        );
    }

    public Uri getAlbumArtUri(Uri mediaUri) {
        String[] projection = {MediaStore.Audio.Media.ALBUM_ID};
        Cursor cursor = context.getContentResolver().query(mediaUri, projection, null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
                    Uri albumArtUri = Uri.parse("content://media/external/audio/albumart");
                    return ContentUris.withAppendedId(albumArtUri, albumId);
                }
            } finally {
                cursor.close();
            }
        }
        return null;
    }
}
