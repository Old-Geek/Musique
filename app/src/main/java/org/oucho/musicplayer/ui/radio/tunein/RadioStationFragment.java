package org.oucho.musicplayer.ui.radio.tunein;


import static org.oucho.musicplayer.ui.radio.tunein.RadioSaver.addRadioStation;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.view.CustomLayoutManager;
import org.oucho.musicplayer.ui.view.bubulle.BubulleRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class RadioStationFragment extends BaseFragment {

    private List<String> history = new ArrayList<>();
    private RadioStationAdapter adapter;
    private LinearLayout progressBarLayout;
    private BroadcastReceiver broadcastReceiver;
    private BubulleRecyclerView recyclerView;

    private OnBackInvokedCallback backCallback;

    private final String radioSourceUrl = "http://opml.radiotime.com";
    private final String tuneInFormats = "&formats=flac,aac,mp3,ogg";

    public RadioStationFragment() { }


    public static RadioStationFragment newInstance() {
        return new RadioStationFragment();
    }

    @Override
    public void load() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tunein, container, false);
        recyclerView = rootView.findViewById(R.id.tunein_recyclerview);
        recyclerView.setLayoutManager(new CustomLayoutManager(requireContext()));
        adapter = new RadioStationAdapter();
        adapter.setOnItemClickListener(itemClickListener);
        recyclerView.setAdapter(adapter);

        progressBarLayout = rootView.findViewById(R.id.tunein_progressbar_layout);

        Bundle args = new Bundle();
        args.putString("url", radioSourceUrl);
        load(args);

        return rootView;
    }

    private void load(Bundle args) {
        Animation fadeInAnimation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_in);
        fadeInAnimation.setDuration(200);
        progressBarLayout.setAnimation(fadeInAnimation);
        progressBarLayout.setVisibility(View.VISIBLE);

        LoaderManager.getInstance(this).restartLoader(0, args, loaderCallbacks);
    }


    private final LoaderManager.LoaderCallbacks<List<String>> loaderCallbacks = new LoaderManager.LoaderCallbacks<>() {

        @NonNull
        @Override
        public Loader<List<String>> onCreateLoader(int id, Bundle args) {

            assert args != null;
            String url = args.getString("url");
            history.add(url);
            setHomeButtonVisibility(history.size() > 1);

            return new RadioStationLoader(requireContext(), args.getString("url"));
        }

        @Override
        public void onLoadFinished(@NonNull Loader<List<String>> loader, List<String> list) {
            if (list != null)
                adapter.setData(list);

            Animation fadeOutAnimation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_out);
            fadeOutAnimation.setDuration(200);
            progressBarLayout.setAnimation(fadeOutAnimation);
            progressBarLayout.setVisibility(View.GONE);
            recyclerView.scrollToPosition(0);
        }

        @Override
        public void onLoaderReset(@NonNull Loader<List<String>> loader) {}
    };


    private final BaseAdapter.OnItemClickListener itemClickListener = new BaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(int position, View v) {

            String item = adapter.getItem(position);
            String[] parts = item.split("\" ");

            if (v.getId() == R.id.radio_ajout) {
                addRadioStation(item);
            } else {

                if (item.contains("type=\"link\"")) {
                    Bundle args = new Bundle();
                    args.putString("url", extractURL(parts) + tuneInFormats);
                    load(args);
                }

                if (item.contains("type=\"audio\""))
                    RadioPlayer.playRadioItem(extractRadioURL(parts), extractRadioName(parts));

            }
        }
    };


    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        // Gérer le geste de retour ici
        backCallback = this::handleBackAction;
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }


    private void setBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(RADIO_SEARCH_STATE);
        filter.addAction(RADIO_FOCUS_STATE);
        filter.addAction(RADIO_HOME_STATE);

        requireContext().registerReceiver(broadcastReceiver, filter, Context.RECEIVER_EXPORTED);
    }

    private void handleBackAction() {
        if (history.size() > 1) {

            String last = history.get(history.size() -2);
            Bundle args = new Bundle();
            args.putString("url", last);

            // supprime les 2 derniers.
            history.remove(history.size() - 1);
            history.remove(history.size() - 1);

            setHomeButtonVisibility(history.size() > 1);

            load(args);

        } else {
            navigateToRadioList();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, false);
        setBroadcastReceiver();
        setBackInvokedCallback();
    }

    @Override
    public void onPause() {
        super.onPause();
        requireContext().unregisterReceiver(broadcastReceiver);

        requireActivity().getOnBackInvokedDispatcher().
                unregisterOnBackInvokedCallback(backCallback);

        SendBroadcast.sending(requireContext(), SWIPE_ENABLED, true);
        navigateToRadioList();
    }

    private class BroadcastReceiver extends android.content.BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (RADIO_SEARCH_STATE.equals(action)) {

                String text = intent.getStringExtra("performSearch");
                if (text != null)
                    performSearch(text);
            }

            if (RADIO_HOME_STATE.equals(action) ) {

                try {
                    if (Objects.equals(intent.getStringExtra("go"), "home")) {
                        history = new ArrayList<>();

                        Bundle args = new Bundle();
                        args.putString("url", radioSourceUrl);
                        setHomeButtonVisibility(false);
                        load(args);
                    }

                    if (Objects.equals(intent.getStringExtra("go"), "back"))
                        navigateToRadioList();

                } catch (RuntimeException ignore) {}
            }

            if (RADIO_FOCUS_STATE.equals(action)) {
                requireView().setFocusableInTouchMode(true);
                requireView().requestFocus();
            }

        }
    }

    private void setHomeButtonVisibility(Boolean value) {
        SendBroadcast.sending(requireContext(), RADIO_HOME_STATE, "setButton", value);
    }

    private void navigateToRadioList() {
        // Supprime la barre de recherche
        SendBroadcast.sending(requireContext(), RADIO_TUNEIN_CLOSE_STATE);
        setHomeButtonVisibility(false);

        requireActivity().getSupportFragmentManager()
                      .popBackStack("RadioStationFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void performSearch(String search) {

        String query = search.replace(" ", "%20");

        Bundle args = new Bundle();
        args.putString("url", radioSourceUrl + "/Search.ashx?query=" + query + tuneInFormats);

        requireView().setFocusableInTouchMode(true);
        requireView().requestFocus();

        load(args);
    }

    static String extractURL(String[] parts) {
        String url = null;
        for (String part : parts) {
            if (part.contains("URL=\""))
                url = part.replace("URL=\"", "").replace("\"", "");
        }
        return url;
    }

    static String extractRadioURL(String[] parts) {
        String url = null;
        for (String part : parts) {
            if (part.contains("URL=\""))
                url = part.replace("URL=\"", "");
        }
        return url;
    }

    static String extractRadioName(String[] parts) {
        String text = parts[1];
        return text.replace("text=\"", "");
    }

}
