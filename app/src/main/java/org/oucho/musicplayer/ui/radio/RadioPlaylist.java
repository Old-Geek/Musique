package org.oucho.musicplayer.ui.radio;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.webkit.URLUtil;

import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.ui.radio.network.HttpGetter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RadioPlaylist {

    private static final int NONE    = 0;
    private static final int M3U     = 1;
    private static final int PLS     = 2;

    private final RadioService player;
    private final String start_url;


    public RadioPlaylist(RadioService a_player, String a_url) {
        super();
        player = a_player;
        start_url = a_url;
    }

    public void start() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            String url = fetchPlaylistUrl(start_url);
            new Handler(Looper.getMainLooper()).post(() -> {
                if (url != null && player != null)
                    player.playLaunch(url);
            });
        });
        executor.shutdown();
    }

    private String fetchPlaylistUrl(String url) {

        int ttl = 10;
        int type;

        while (ttl > 0 && url != null && !url.isEmpty() && URLUtil.isValidUrl(url)) {
            type = playlistType(url);

            if (type != NONE) { // Si c'est une playlist
                ttl--;
                url = selectUrlFromPlaylist(url, type);
            } else // Si ce n'est pas une playlist
                break; // Sortir de la boucle
        }

        if (ttl == 0) // Si on a atteint la limite de redirections
            url = null;

        return url;
    }

    private static String filter(String line, int type) {

        return switch (type) {
            case M3U -> line.indexOf('#') == 0 ? "" : line;
            case PLS -> {
                if (line.startsWith("File") && 0 < line.indexOf('='))
                    yield line;
                yield "";
            }
            default -> line;
        };
    }


    private String selectUrlFromPlaylist(String url, int type) {

        List<String> lines = HttpGetter.httpGet(url);

        for (int i=0; i<lines.size(); i+= 1) {
            String line = lines.get(i);
            line = filter(line.trim(),type);
            lines.set(i, line);
        }

        List<String> links = selectUrlsFromList(lines);
        if (links.isEmpty())
            return null;

        return links.get(0);
    }


    private static ArrayList<String> selectUrlsFromList(List<String> lines) {

        String url_regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";

        Pattern url_pattern = Pattern.compile(url_regex);

        ArrayList<String> links = new ArrayList<>();

        for (int i=0; i<lines.size(); i+=1)  {

            String line = lines.get(i);

            if (!line.isEmpty()) {
                Matcher matcher = url_pattern.matcher(line);

                if ( matcher.find() ) {
                    String link = matcher.group();

                    if (link.startsWith("(") && link.endsWith(")"))
                        link = link.substring(1, link.length() - 1);

                    links.add(link);
                }
            }
        }

        return links;
    }

    private static Uri parseUri(String url) {
        return Uri.parse(url);
    }

    private static boolean isSuffix(String text, String suffix) {
        return text != null && text.endsWith(suffix) ;
    }

    private static boolean isSomeSuffix(String url, String suffix) {
        return isSuffix(url, suffix) || isSuffix(parseUri(url).getPath(), suffix);
    }

    private static int playlistType(String url) {

        String URL = url.toLowerCase();
        if ( isSomeSuffix(URL,".m3u" ) || isSomeSuffix(URL,".m3u8" )) return M3U;
        else if ( isSomeSuffix(URL,".pls" ) ) return PLS;
        return NONE;
    }

    public static boolean isPlaylistMimeType(String mime) {

        return mime != null && ("audio/x-scpls".equals(mime)
                || "audio/scpls".equals(mime)
                || "audio/x-mpegurl".equals(mime)
                || "audio/mpegurl".equals(mime)
                || "audio/mpeg-url".equals(mime)
                || "application/vnd.apple.mpegurl".equals(mime)
                || "application/x-winamp-playlist".equals(mime)
                || mime.contains("mpegurl")
                || mime.contains("mpeg-url")
                || mime.contains("scpls")
                || mime.indexOf("text/") == 0);
    }

}
