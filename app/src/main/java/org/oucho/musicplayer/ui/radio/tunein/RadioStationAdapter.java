package org.oucho.musicplayer.ui.radio.tunein;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.R;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class RadioStationAdapter extends BaseAdapter<RadioStationAdapter.RadioStationViewHolder> {

    private List<String> categoryList = new ArrayList<>();

    private boolean isAudioType = true;

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<String> data) {

        List<String> list = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            if  (data.get(i).contains("type=\"audio\""))
                list.add(data.get(i));
        }

        for (int i = 0; i < data.size(); i++) {
            if  (data.get(i).contains("type=\"link\""))
                list.add(data.get(i));
        }

        categoryList = list;
        isAudioType = data.contains("type=\"audio\"");
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RadioStationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_tunein_list_item, parent, false);
        return new RadioStationViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull RadioStationViewHolder viewHolder, int position) {

        String item = categoryList.get(position);
        String[] parts = item.split("\" ");

        viewHolder.textView.setVisibility(View.GONE);
        viewHolder.relativeLayout.setVisibility(View.GONE);

        if  (item.contains("type=\"audio\"")) {

            String text = parts[1];
            String name = text.replace("text=\"" , "");

            viewHolder.textView.setVisibility(View.GONE);
            viewHolder.relativeLayout.setVisibility(View.VISIBLE);
            viewHolder.detailsTitle.setText(name);

            int img_size = 50;

            for (String part : parts) {

                if (part.contains("subtext=\""))
                    viewHolder.detailSubtitle.setText(part.replace("subtext=\"", ""));

                if (part.contains("image=\"")) {
                    String url_image = part.replace("image=\"", "");
                    Picasso.get()
                            .load(url_image)
                            .error(R.drawable.mic_24px)
                            .resize(img_size, img_size)
                            .centerInside()
                            .into(viewHolder.imageView);
                }
            }
        }

        if  (!isAudioType) {

            if (item.contains("type=\"link\"")) {

                String text = parts[1];
                String name = text.replace("text=\"", "");

                viewHolder.textView.setVisibility(View.VISIBLE);
                viewHolder.relativeLayout.setVisibility(View.GONE);
                viewHolder.textView.setText(name);
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public String getItem(int position) {
        return categoryList.get(position);
    }

    public class RadioStationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final RelativeLayout relativeLayout;
        private final ImageView imageView;
        private final TextView detailSubtitle;
        private final TextView detailsTitle;
        private final TextView textView;

        RadioStationViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.radio_item_title);
            detailsTitle = itemView.findViewById(R.id.radio_detail_title);
            detailSubtitle = itemView.findViewById(R.id.radio_detail_subtitle);
            imageView = itemView.findViewById(R.id.radio_detail_image);
            relativeLayout = itemView.findViewById(R.id.radio_detail_layout);
            ImageButton radioAddButton = itemView.findViewById(R.id.radio_ajout);

            radioAddButton.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getBindingAdapterPosition();

            if (position != RecyclerView.NO_POSITION)
                triggerOnItemClickListener(position, v);
        }
    }
}