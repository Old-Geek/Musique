package org.oucho.musicplayer.ui.radio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.MusicPlayerApp;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.db.RadioDbHelper;
import org.oucho.musicplayer.db.model.Radio;
import org.oucho.musicplayer.services.RadioService;
import org.oucho.musicplayer.services.RadioState;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.tools.SendBroadcast;
import org.oucho.musicplayer.ui.basefrag.BaseAdapter;
import org.oucho.musicplayer.ui.basefrag.BaseFragment;
import org.oucho.musicplayer.ui.basefrag.CustomViewModel;
import org.oucho.musicplayer.ui.helpers.TitleSet;
import org.oucho.musicplayer.ui.queue.QueueManager;
import org.oucho.musicplayer.ui.queue.blurview.BlurView;
import org.oucho.musicplayer.ui.radio.tunein.RadioStationFragment;
import org.oucho.musicplayer.ui.timer.TimerHelper;
import org.oucho.musicplayer.ui.timer.TimerListener;

import java.io.File;
import java.util.ArrayList;


public class RadioFragment extends BaseFragment implements TimerListener, View.OnClickListener {

    private boolean isFocusedSearch;
    private static boolean bHome = false;
    private boolean showBitrate = false;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Handler bitrateHandler = new Handler(Looper.getMainLooper());

    private EditText editText;
    private ImageButton home;
    TextView stationTextView;
    ImageView img_equalizer;
    TextView bitrate;

    TextView zzz;
    private TimerHelper timerHelper;

    private BroadcastReceiver playerReceiver;

    private RecyclerView mRecyclerView;
    private RadioAdapter mAdapter;
    public RelativeLayout searchLayout;

    private OnBackInvokedCallback backCallback;
    final int haptic = HapticFeedbackConstants.VIRTUAL_KEY;
    final int hapticLong = HapticFeedbackConstants.LONG_PRESS;

    public static Fragment newInstance() {
        return new RadioFragment();
    }

    @Override
    public void load() {}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        timerHelper = ((MainActivity) requireActivity()).getTimerHelper();
        playerReceiver = new playerReceiver();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_radio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new RadioAdapter();
        mRecyclerView = view.findViewById(R.id.radio_recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), 4); // 1 colonne pour simuler une liste
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mAdapter.setOnItemClickListener(mOnItemClickListener);
        mAdapter.setOnItemLongClickListener(mOnItemLongClickListener);
        mRecyclerView.setAdapter(mAdapter);

        editText = view.findViewById(R.id.search_radio);
        searchLayout = view.findViewById(R.id.radio_search_layout);
        stationTextView = view.findViewById(R.id.radio_station);
        img_equalizer = view.findViewById(R.id.radio_icon_equalizer);
        bitrate = view.findViewById(R.id.radio_bitrate);
        zzz = view.findViewById(R.id.zZzR);

        home = view.findViewById(R.id.radio_home_button);
        home.setOnClickListener(this);

        view.findViewById(R.id.radio_search_button).setOnClickListener(this);

        showBitrate = true;
        showCurrentBitRate();

        search();
        RadioState.broadcastState(requireContext());

        setOptionsMenu();
    }


    private void setOptionsMenu() {
        requireActivity().addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menu.findItem(R.id.action_search).setVisible(false);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {return false;}

        }, getViewLifecycleOwner(), Lifecycle.State.RESUMED);
    }

    private void setBackInvokedCallback() {
        OnBackInvokedDispatcher dispatcher = requireActivity().getOnBackInvokedDispatcher();
        backCallback = () -> {// Gérer le geste de retour ici

            MainActivity activity = (MainActivity) requireActivity();
            CustomViewModel customViewModel = new ViewModelProvider(requireActivity()).get(CustomViewModel.class);

            QueueManager queueManager = ((MainActivity) requireActivity()).getQueueManager();
            View mQueueView = queueManager.queueLayout;
            if (mQueueView.getVisibility() == View.VISIBLE)
                queueManager.toggleQueueVisibility();
            else if (isFocusedSearch) {
                SendBroadcast.sending(requireContext(), RADIO_FOCUS_STATE);
            } else if (customViewModel.isQueueLayoutVisible()) {

                View queueLayout = activity.findViewById(R.id.queue_layout);
                queueLayout.setVisibility(View.GONE);
                BlurView blurview = activity.findViewById(R.id.queueView);
                blurview.setVisibility(View.GONE);
                customViewModel.setQueueLayoutVisible(false);

            } else {
                if (timerHelper.isTimerRunning())
                    requireActivity().moveTaskToBack(true);
                else
                    requireActivity().finish();
            }
        };
        dispatcher.registerOnBackInvokedCallback(OnBackInvokedDispatcher.PRIORITY_DEFAULT, backCallback);
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    public void setHomeButton(Boolean value) {

        bHome = value;

        if (value)
            home.setImageDrawable(requireContext().getDrawable(R.drawable.home_24px));
        else
            home.setImageDrawable(requireContext().getDrawable(R.drawable.grid_on_24px));
    }



    /* **********************************************************************************************
     * Pause / résume / etc.
     * *********************************************************************************************/

    @Override
    public void onResume() {
        super.onResume();

        updateRadioName();

        if (getActivity() instanceof MainActivity)
            TitleSet.setRadioTitle((AppCompatActivity) getActivity(), "Radio");

        SendBroadcast.sending(requireContext(), INTENT_LAYOUT_VIEW, PLAYBAR_CONTROL, "showradio");
        SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", false);

        if (!showBitrate) {
            showBitrate = true;
            showCurrentBitRate();
        }

        updateListView();
        setBackInvokedCallback();

        IntentFilter filter = new IntentFilter();
        filter.addAction(RADIO_PLAYER_STATE);
        filter.addAction(RADIO_TUNEIN_CLOSE_STATE);
        filter.addAction(RADIO_ERROR_STATE);
        filter.addAction(RADIO_HOME_STATE);
        filter.addAction(RADIO_LOAD_TUNEIN_STATE);

        requireContext().registerReceiver(playerReceiver, filter, Context.RECEIVER_EXPORTED);
        timerHelper.addListener(this);
        updatePlayPauseIcon();
    }

    @Override
    public void onPause() {
        super.onPause();

        SendBroadcast.sending(requireContext(), INTENT_LAYOUT_VIEW, PLAYBAR_CONTROL, "hideradio");
        SendBroadcast.sending(requireContext(), TOOLBAR_SHADOW, "boolean", true);

        if (showBitrate) {
            stopBitrate();
            showBitrate = false;
        }


        requireContext().unregisterReceiver(playerReceiver);

        timerHelper.removeListener(this);

        requireActivity().getOnBackInvokedDispatcher().
                unregisterOnBackInvokedCallback(backCallback);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (showBitrate)
            stopBitrate();

        try {
            requireContext().unregisterReceiver(playerReceiver);
        } catch (IllegalArgumentException ignore) {}
    }


    @Override
    public void onTimerUpdate(String time) {
        zzz.setVisibility(View.VISIBLE);
        zzz.setText(time);
    }

    @Override
    public void onTimerFinish() {
        zzz.setText("0");
        zzz.setVisibility(View.GONE);
    }

    public void loadSearch() {

        Fragment fragment = RadioStationFragment.newInstance();
        FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom, R.anim.slide_in_bottom, R.anim.slide_out_bottom);
        ft.replace(R.id.fragment_radio_layout, fragment);
        ft.addToBackStack("RadioStationFragment");
        ft.commit();

        Animation animFadeIn = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_in);
        animFadeIn.setDuration(400);
        searchLayout.setAnimation(animFadeIn);
        searchLayout.setVisibility(View.VISIBLE);
    }

    private void search() {

        editText.setOnFocusChangeListener(focusListener);
        editText.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                setSearch();
                return true;
            }

            return false;
        });
    }


    private void setSearch() {

        String textSearch = editText.getText().toString();

        if (!textSearch.isEmpty()) {
            SendBroadcast.sending(requireContext(), RADIO_SEARCH_STATE, "performSearch", textSearch);

            View view = requireView().findFocus();

            if (view != null) {
                InputMethodManager imm = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm.isActive())
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    private final View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean hasFocus) {
            isFocusedSearch = hasFocus;
        }
    };


    /* *********************************
     * Affiche le nom de la radio active
     * *********************************/

    private void updateRadioName() {

        String title = Preferences.getRadioName();

        if (title != null) {
            Radio radio = new Radio("", title, "");
            title = radio.stationName();
            stationTextView.setText(title);
        }
    }


    /* ****************************
     * Changement d'état play/pause
     * ****************************/

    @SuppressLint("UseCompatLoadingForDrawables")
    public void updatePlayPauseIcon() {
        Drawable drawable = requireContext().getDrawable(R.drawable.equalizer_24px);

        if (drawable == null)
            return;

        drawable = DrawableCompat.wrap(drawable);

        if (RadioState.isPlaying() || RadioState.isPaused())
            DrawableCompat.setTint(drawable, ContextCompat.getColor(requireContext(), R.color.colorAccent));
        else
            DrawableCompat.setTint(drawable, ContextCompat.getColor(requireContext(), R.color.grey_300));

        img_equalizer.setBackground(drawable);
    }


    /* **********************************************************************************************
     * Gestion des clicks sur l'interface
     * *********************************************************************************************/

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        v.performHapticFeedback(haptic);

       if (v.getId() == R.id.radio_search_button) {

            setSearch();

        } else if (v.getId() == R.id.radio_home_button) {

            String value = "back";
            if (bHome)
                value = "home";

            SendBroadcast.sending(requireContext(), RADIO_HOME_STATE, "go", value);
        }
    }

    private final BaseAdapter.OnItemLongClickListener mOnItemLongClickListener = new BaseAdapter.OnItemLongClickListener() {
        @Override
        public void onItemLongClick(int position, View view) {
            view.performHapticFeedback(hapticLong);
            Radio radio = mAdapter.getItem(position);
            popupRadioItem(view, radio);
        }
    };

    private final BaseAdapter.OnItemClickListener mOnItemClickListener = new BaseAdapter.OnItemClickListener() {

        @SuppressLint("NonConstantResourceId")
        @Override
        public void onItemClick(int position, View view) {
            view.performHapticFeedback(haptic);
            Radio radio = mAdapter.getItem(position);
            if (view.getId() == R.id.fond) {
                if ( !(RadioState.isPlaying() && radio.streamUrl().equals(RadioService.getUrl())) ) {
                    Intent player = new Intent(MusicPlayerApp.getInstance(), RadioService.class);
                    player.setAction(ACTION_RADIO_STOP);
                    requireContext().startService(player);
                    play(radio);
                }
            }
        }
    };


    @SuppressLint("NonConstantResourceId")
    private void popupRadioItem(final View v, Radio radio) {

        final PopupMenu popup = new PopupMenu(requireContext(), v);
        popup.getMenuInflater().inflate(R.menu.radio_editdelete, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {

            if (item.getItemId() == R.id.menu_delete)
                deleteRadio(radio);

            return true;
        });

        popup.show();
    }

    public void updateListView() {
        ArrayList<Radio> radioList = new ArrayList<>(RadioDbHelper.getRadios(requireContext()));

        mAdapter.setData(radioList);
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(() -> {

            String url = RadioService.getUrl();

            if (RadioService.getUrl() == null)
                url = Preferences.getRadioUrl();

            for (int i = 0; i < radioList.size(); i++) {
                if (radioList.get(i).streamUrl().equals(url))
                    mRecyclerView.smoothScrollToPosition(i);
            }
        }, 250);
    }



    /* **********************************************************************************************
     * Lecture de la radio
     * *********************************************************************************************/
    private void play(Radio radio) {

        String url = radio.streamUrl();
        String name = radio.stationName();

        Intent player = new Intent(requireContext(), RadioService.class);
        player.setAction(ACTION_RADIO_PLAY);
        player.putExtra("url", url);
        player.putExtra("name", name);
        requireContext().startService(player);
    }


    /* **********************************************************************************************
     * Suppression de la radio
     * *********************************************************************************************/
    private void deleteRadio(final Radio radio) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setMessage(getResources().getString(R.string.deleteRadioConfirm) + radio.stationName());
        builder.setPositiveButton(R.string.delete, (dialog, which) -> {
            RadioDbHelper.deleteRadio(requireContext(), radio);
            deleteImage(radio.streamUrl());
            updateListView();
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    public void deleteImage(String key) {
        try {
            String fileName = MD5Helper.hash(key) + ".jpg";
            File file = new File(requireContext().getFilesDir(), fileName);
            if (file.exists()) {
                if (file.delete())
                    Log.i("SaveRadio", "Image supprimée : " + fileName);
                else
                    Log.e("SaveRadio", "Erreur lors de la suppression de l'image : " + fileName);
            }
        } catch (Exception e) {
            Log.e("SaveRadio", "Erreur lors de la suppression de l'image", e);
        }
    }


    /* **********************************************************************************************
     * Get showBitrate
     * *********************************************************************************************/
    private void showCurrentBitRate() {

        bitrateHandler.postDelayed(new Runnable() {

            public void run() {
                bitRate();
                bitrateHandler.postDelayed(this, 2000);
            }
        }, 1);
    }

    private void bitRate() {
        final int uid = Process.myUid();
        final long received = TrafficStats.getUidRxBytes(uid) / 1024;

        handler.postDelayed(() -> {
            long current = TrafficStats.getUidRxBytes(uid) / 1024;
            long total = current - received;
            long ByteToBit = total * 8;

            if (ByteToBit <= 1024 ) {
                String bitrate = ByteToBit + " Kb/s";
                this.bitrate.setText(bitrate);
            } else {
                long megaBit = ByteToBit / 1024;
                String bitrate = megaBit + " Mb/s";
                this.bitrate.setText(bitrate);
            }
        }, 1000);
    }

    private void stopBitrate() {

        if (showBitrate) {
            bitrateHandler.removeCallbacksAndMessages(null);
            showBitrate = false;
        }
    }


    private class playerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action == null)
                return;

            switch (action) {
                case RADIO_HOME_STATE:
                    handleHomeIntent(intent);
                    break;
                case RADIO_ERROR_STATE:
                    handleErrorIntent(intent);
                    break;
                case RADIO_TUNEIN_CLOSE_STATE:
                    radioTuneInClose();
                    break;
                case RADIO_PLAYER_STATE:
                    handleStateIntent();
                    break;
                case RADIO_LOAD_TUNEIN_STATE:
                    loadSearch();
                    break;
            }
        }
    }


    // Méthode pour gérer l'intent RADIO_HOME
    private void handleHomeIntent(Intent intent) {
        // Récupérer la valeur du booléen "setButton" et mettre à jour le bouton Home de l'activité
        boolean home = intent.getBooleanExtra("setButton", false);
        setHomeButton(home);
    }

    private void handleErrorIntent(Intent intent) {
        String erreur = intent.getStringExtra("error");
        assert erreur != null;
        Log.e("RadioFragment", erreur);

    }


    private void radioTuneInClose() {

        Animation animFadeOut = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_out);
        animFadeOut.setDuration(400);
        searchLayout.setAnimation(animFadeOut);
        searchLayout.setVisibility(View.GONE);

        updateListView();
    }

    // Méthode pour gérer l'intent RADIO_STATE
    private void handleStateIntent() {

        // Mettreà jour le nom de la radio, l'icône de lecture/pause et la liste des radios
        updateRadioName();
        updatePlayPauseIcon();
        updateListView();
    }
}