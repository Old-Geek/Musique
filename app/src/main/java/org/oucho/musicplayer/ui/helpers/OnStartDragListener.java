package org.oucho.musicplayer.ui.helpers;

import androidx.recyclerview.widget.RecyclerView;

public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
