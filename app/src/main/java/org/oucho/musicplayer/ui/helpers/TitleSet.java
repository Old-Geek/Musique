package org.oucho.musicplayer.ui.helpers;

import android.text.Html;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.oucho.musicplayer.R;

public class TitleSet {

    public static void setTitle(AppCompatActivity activity, String title, String subtitle) {
        setTitleWithColors(activity, title, subtitle, R.color.grey_400, -1);
    }

    public static void setPlaylistContentTitle(AppCompatActivity activity, String title, String subtitle) {
        setTitleWithColors(activity, title, subtitle, R.color.red_300, R.color.grey_400);
    }

    public static void setPlaylistTitle(AppCompatActivity activity, String title) {
        setTitleWithColors(activity, title, "", -1, -1);
    }

    public static void setRadioTitle(AppCompatActivity activity, String title) {
        setTitleWithColors(activity, title, "", R.color.blue_900_trans, -1);
    }

    private static void setTitleWithColors(AppCompatActivity activity, String title, String subtitle, int titleColorResId, int subtitleColorResId) {
        String titleColor = (titleColorResId != -1) ? String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity, titleColorResId))) : "";
        String subtitleColor = (subtitleColorResId != -1) ? String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(activity, subtitleColorResId))) : "";

        String htmlTitle = "<font color=\"" + titleColor + "\">" + title + "</font>";
        if (!subtitle.isEmpty()) {
            htmlTitle += " <small><font color='" + subtitleColor + "'>" + subtitle + "</font></small>";
        }

        activity.setTitle(Html.fromHtml(htmlTitle, Html.FROM_HTML_MODE_LEGACY));
    }
}
