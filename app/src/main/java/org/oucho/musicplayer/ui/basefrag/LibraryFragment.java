package org.oucho.musicplayer.ui.basefrag;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import org.oucho.musicplayer.MainActivity;
import org.oucho.musicplayer.R;
import org.oucho.musicplayer.tools.Preferences;
import org.oucho.musicplayer.ui.album.AlbumListFragment;
import org.oucho.musicplayer.ui.playlist.PlaylistFragment;
import org.oucho.musicplayer.ui.radio.RadioFragment;
import org.oucho.musicplayer.ui.song.SongListFragment;


public class LibraryFragment extends BaseFragment {

    private LinearLayout toolbarShadow;
    private boolean isReceiverRegistered = false;
    private ViewPager2 viewPager;
    private  boolean isSwipeEnabled = true;

    public static LibraryFragment newInstance() {
        return new LibraryFragment();
    }

    @Override
    public void load() {}

    @Override
    public void onPause() {
        super.onPause();

        if (isReceiverRegistered) {
            requireContext().unregisterReceiver(serviceListener);
            isReceiverRegistered = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isReceiverRegistered) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(TOOLBAR_SHADOW);
            filter.addAction(BACK_VIEWPAGER);
            filter.addAction(SWIPE_ENABLED);

            requireContext().registerReceiver(serviceListener, filter, Context.RECEIVER_EXPORTED);
            isReceiverRegistered = true;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        MainActivity activity = (MainActivity) requireActivity();
        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(requireActivity());
        viewPager = rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setUserInputEnabled(isSwipeEnabled);
        // charge le fragment n°x
        int page = Preferences.getViewPager2();
        if (page == 1)
            page = 0;

        viewPager.setCurrentItem(page, false);
        toolbarShadow = rootView.findViewById(R.id.toolbar_shadow);

        return rootView;
    }


    private static class SectionsPagerAdapter extends FragmentStateAdapter {

        public SectionsPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return switch (position) {
                case 0 -> RadioFragment.newInstance();
                case 1 -> AlbumListFragment.newInstance();
                case 2 -> PlaylistFragment.newInstance();
                case 3 -> SongListFragment.newInstance();
                //default: //do nothing
                default -> AlbumListFragment.newInstance();
            };
        }

        @Override
        public int getItemCount() {
            return 4;
        }
    }

    public void setSwipeEnabled(boolean enabled) {
        isSwipeEnabled = enabled;
        if (viewPager != null)
            viewPager.setUserInputEnabled(enabled);
    }

    private final BroadcastReceiver serviceListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String receiveIntent = intent.getAction();

            assert receiveIntent != null;
            switch (receiveIntent) {
                case TOOLBAR_SHADOW -> {

                    boolean value = intent.getBooleanExtra("boolean", true);

                    if (value)
                        toolbarShadow.setElevation(LibraryFragment.this.requireContext().getResources().getDimension(R.dimen.toolbar_elevation));
                    else
                        toolbarShadow.setElevation(0);
                }
                case BACK_VIEWPAGER -> {
                    boolean value = intent.getBooleanExtra("boolean", true);
                    if (value)
                        navigateToPreviousPage();
                    else
                        navigateToNextPage();
                }
                case SWIPE_ENABLED -> {
                    boolean value = intent.getBooleanExtra("boolean", true);
                    setSwipeEnabled(value);
                }
            }
        }
    };

    private void navigateToPreviousPage() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    private void navigateToNextPage() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

}
