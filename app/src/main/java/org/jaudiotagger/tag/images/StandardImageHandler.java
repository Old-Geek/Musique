package org.jaudiotagger.tag.images;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Image Handling used when running on Android
 */
public class StandardImageHandler implements ImageHandler {

    private static StandardImageHandler instance;

    public static StandardImageHandler getInstanceOf() {
        if (instance == null) {
            instance = new StandardImageHandler();
        }
        return instance;
    }

    private StandardImageHandler() {
    }

    /**
     * Resize the image until the total size required to store the image is less than maxSize.
     *
     * @param artwork the artwork containing the image.
     * @param maxSize the maximum size for the image.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void reduceQuality(Artwork artwork, int maxSize) throws IOException {
        while (artwork.getBinaryData().length > maxSize) {
            Bitmap srcImage = (Bitmap) artwork.getImage();
            int w = srcImage.getWidth();
            int newSize = w / 2;
            makeSmaller(artwork, newSize);
        }
    }

    /**
     * Resize image using Android's Bitmap.
     *
     * @param artwork the artwork containing the image.
     * @param size the new size for the image.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void makeSmaller(Artwork artwork, int size) throws IOException {
        Bitmap srcImage = (Bitmap) artwork.getImage();

        int w = srcImage.getWidth();
        int h = srcImage.getHeight();

        // Determine the scaling required to get the desired result.
        float scale = (float) size / Math.max(w, h);

        // Create a new bitmap with the desired size.
        Bitmap resizedBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);

        // Set the scale.
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);

        // Paint image.
        Canvas canvas = new Canvas(resizedBitmap);
        Paint paint = new Paint();
        canvas.drawBitmap(srcImage, matrix, paint);

        if (artwork.getMimeType() != null && isMimeTypeWritable(artwork.getMimeType())) {
            artwork.setBinaryData(writeImage(resizedBitmap, artwork.getMimeType()));
        } else {
            artwork.setBinaryData(writeImageAsPng(resizedBitmap));
        }
    }

    /**
     * Checks if the MIME type is writable.
     *
     * @param mimeType the MIME type to check.
     * @return true if the MIME type is writable, false otherwise.
     */
    @Override
    public boolean isMimeTypeWritable(String mimeType) {
        // Implementation for supported MIME types
        return mimeType.equalsIgnoreCase("image/png") || mimeType.equalsIgnoreCase("image/jpeg");
    }

    /**
     * Writes the image to a byte array in the specified MIME type format.
     *
     * @param bitmap the image to write.
     * @param mimeType the MIME type of the image.
     * @return the image as a byte array.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public byte[] writeImage(Bitmap bitmap, String mimeType) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (mimeType.equalsIgnoreCase("image/png")) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else if (mimeType.equalsIgnoreCase("image/jpeg")) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        } else {
            throw new IOException("Unsupported MIME type: " + mimeType);
        }
        return stream.toByteArray();
    }

    /**
     * Writes the image as a PNG to a byte array.
     *
     * @param bitmap the image to write.
     * @return the image as a PNG byte array.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public byte[] writeImageAsPng(Bitmap bitmap) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Shows the readable formats.
     */
    @Override
    public void showReadFormats() {
        // Implementation to show supported read formats
        System.out.println("Supported read formats: png, jpeg");
    }

    /**
     * Shows the writable formats.
     */
    @Override
    public void showWriteFormats() {
        // Implementation to show supported write formats
        System.out.println("Supported write formats: png, jpeg");
    }
}
