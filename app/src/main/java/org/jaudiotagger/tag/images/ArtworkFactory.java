package org.jaudiotagger.tag.images;

import org.jaudiotagger.audio.flac.metadatablock.MetadataBlockDataPicture;

/**
 * Get appropriate Artwork class
 */
public class ArtworkFactory {

    public static Artwork getNew() {
            return new AndroidArtwork();
    }

    /**
     * Create Artwork instance from A Flac Metadata Block
     *
     */
    public static Artwork createArtworkFromMetadataBlockDataPicture(MetadataBlockDataPicture coverArt) {
            return AndroidArtwork.createArtworkFromMetadataBlockDataPicture(coverArt);
    }
}
