package org.jaudiotagger.tag.images;

import android.graphics.Bitmap;

import java.io.IOException;

/**
 * Bitmap methods
 * <p>
 * Compatible with Android.
 */
public class Images {

    /**
     * Returns the image as a Bitmap.
     *
     * @param artwork the artwork containing the image.
     * @return the image as a Bitmap.
     * @throws IOException if an I/O error occurs.
     */
    public static Bitmap getImage(Artwork artwork) throws IOException {
        // Assuming Artwork has a method to get the image as a Bitmap
        return (Bitmap) artwork.getImage();
    }
}

