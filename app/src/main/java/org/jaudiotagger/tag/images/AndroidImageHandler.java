package org.jaudiotagger.tag.images;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Image Handling to use when running on Android
 */
public class AndroidImageHandler implements ImageHandler {

    private static AndroidImageHandler instance;

    public static AndroidImageHandler getInstanceOf() {
        if (instance == null) {
            instance = new AndroidImageHandler();
        }
        return instance;
    }

    private AndroidImageHandler() {
    }

    /**
     * Resize the image until the total size required to store the image is less than maxSize.
     *
     * @param artwork the artwork containing the image.
     * @param maxSize the maximum size for the image.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void reduceQuality(Artwork artwork, int maxSize) throws IOException {
        // Implementation using Bitmap
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Resize image using Android's Bitmap.
     *
     * @param artwork the artwork containing the image.
     * @param size the new size for the image.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void makeSmaller(Artwork artwork, int size) throws IOException {
        // Implementation using Bitmap
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Checks if the MIME type is writable.
     *
     * @param mimeType the MIME type to check.
     * @return true if the MIME type is writable, false otherwise.
     */
    @Override
    public boolean isMimeTypeWritable(String mimeType) {
        // Implementation for supported MIME types
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Writes the image to a byte array in the specified MIME type format.
     *
     * @param bitmap the image to write.
     * @param mimeType the MIME type of the image.
     * @return the image as a byte array.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public byte[] writeImage(Bitmap bitmap, String mimeType) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (mimeType.equalsIgnoreCase("image/png")) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else if (mimeType.equalsIgnoreCase("image/jpeg")) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        } else {
            throw new UnsupportedOperationException("Unsupported MIME type: " + mimeType);
        }
        return stream.toByteArray();
    }

    /**
     * Writes the image as a PNG to a byte array.
     *
     * @param bitmap the image to write.
     * @return the image as a PNG byte array.
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public byte[] writeImageAsPng(Bitmap bitmap) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Shows the readable formats.
     */
    @Override
    public void showReadFormats() {
        // Implementation to show supported read formats
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Shows the writable formats.
     */
    @Override
    public void showWriteFormats() {
        // Implementation to show supported write formats
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
