package org.jaudiotagger.tag.images;

import android.graphics.Bitmap;

import java.io.IOException;

/**
 * Image Handler
 */
public interface ImageHandler {

    /**
     * Reduces the quality of the image.
     *
     * @param artwork the artwork containing the image.
     * @param maxSize the maximum size for the image.
     * @throws IOException if an I/O error occurs.
     */
    void reduceQuality(Artwork artwork, int maxSize) throws IOException;

    /**
     * Makes the image smaller.
     *
     * @param artwork the artwork containing the image.
     * @param size the new size for the image.
     * @throws IOException if an I/O error occurs.
     */
    void makeSmaller(Artwork artwork, int size) throws IOException;

    /**
     * Checks if the MIME type is writable.
     *
     * @param mimeType the MIME type to check.
     * @return true if the MIME type is writable, false otherwise.
     */
    boolean isMimeTypeWritable(String mimeType);

    /**
     * Writes the image to a byte array.
     *
     * @param bitmap the image to write.
     * @param mimeType the MIME type of the image.
     * @return the image as a byte array.
     * @throws IOException if an I/O error occurs.
     */
    byte[] writeImage(Bitmap bitmap, String mimeType) throws IOException;

    /**
     * Writes the image as a PNG to a byte array.
     *
     * @param bitmap the image to write.
     * @return the image as a PNG byte array.
     * @throws IOException if an I/O error occurs.
     */
    byte[] writeImageAsPng(Bitmap bitmap) throws IOException;

    /**
     * Shows the readable formats.
     */
    void showReadFormats();

    /**
     * Shows the writable formats.
     */
    void showWriteFormats();
}

